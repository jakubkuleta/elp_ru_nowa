@extends('front.layouts.master', array('metaTitle' => 'Сайт не существует'))
@section('content')
<div class="container text-page certificates">
    <div class="row" style="margin-top:20px">
        <div id="content" class="col-sm-12">
            <h1>Сайт не существует</h1>
            <a href="{{ UrlHelper::home() }}" class="btn btn-primary">На главную</a>
        </div>    
    </div>

    <hr>    
</div>    
@endsection    

