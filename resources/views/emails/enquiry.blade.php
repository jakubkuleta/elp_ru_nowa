@extends('emails.layout')

@section('content')
<h1 style="margin:0;padding:0;font-size:18px">Запрос с интернет-сайта</h1><br>
<strong>{{ date("Y-m-d H:i") }}</strong> числа отправлен запрос с сайта:<br>
<a style="color:#337ab7" href="{{ Config::get('content.domain') }}">{{ Config::get('content.domain') }}</a>
<br>

@if ($product)
<br>
<strong>Запрос относится к продукту:</strong><br>
<a style="color:#337ab7" href="{{ UrlHelper::product($product) }}">{{ $product->name }}</a>
<br>
@endif

<br>
<strong>{{ trans('enquiry.labels.cam') }}:</strong>
<br>
Положение: {{ array_get(trans('enquiry.options.cam'), (int)Request::input('cam'), '-') }}
<br>
@if (!$product) Размер Y: {{ FormatHelper::trim(Request::input('cam_dimension_Y'), '-') }} мм<br>@endif
Размер X: {{ FormatHelper::trim(Request::input('cam_dimension_X'), '-') }} мм<br>
Размер B: {{ FormatHelper::trim(Request::input('cam_dimension_B'), '-') }} мм
<br><br>

@if (!$product)
<strong>{{ trans('enquiry.labels.fixing_hole') }}:</strong><br>
Размер &empty;: {{ FormatHelper::trim(Request::input('fixing_hole_size_F'), '-') }} мм<br>
Размер Z: {{ FormatHelper::trim(Request::input('fixing_hole_size_Z'), '-') }} мм
<br><br>
@endif

<strong>{{ trans('enquiry.labels.position') }}:</strong><br>
{{ array_get(trans('enquiry.options.position'), (int)Request::input('position'), '-') }}
<br><br>

<strong>{{ trans('enquiry.labels.movement') }}:</strong><br>
{{ array_get(trans('enquiry.options.movement'), (int)Request::input('movement'), '-') }}
<br><br>

<strong>{{ trans('enquiry.labels.key') }}:</strong><br>
Все замки: {{ array_get(trans('enquiry.options.key'), (int)Request::input('key'), '-') }}<br>
{{ trans('enquiry.labels.key_master') }}: {{ Request::input('masterkey') ? 'да' : 'нет'}}
<br><br>

<strong>{{ trans('enquiry.inputs.enquiry') }}:</strong><br>
{!! nl2br(e(FormatHelper::trim(Request::input('enquiry'), '-'))) !!}
<br><br>

<strong>{{ trans('enquiry.inputs.quantity') }}:</strong><br>
{{ FormatHelper::trim(Request::input('quantity'), '-') }}
<br><br>

<strong>Контактные данные:</strong><br>
{{ trans('enquiry.inputs.company_name') }}: {{ FormatHelper::trim(Request::input('company_name'), '-') }}<br>
{{ trans('enquiry.inputs.country') }}: {{ FormatHelper::trim(Request::input('country'), '-') }}<br>
{{ trans('enquiry.inputs.city') }}: {{ FormatHelper::trim(Request::input('city'), '-') }}<br>
{{ trans('enquiry.inputs.street') }}: {{ FormatHelper::trim(Request::input('street'), '-') }}<br>
{{ trans('enquiry.inputs.postcode') }}: {{ FormatHelper::trim(Request::input('postcode'), '-') }}<br>

{{ trans('enquiry.inputs.person') }}: {{ FormatHelper::trim(Request::input('person'), '-') }}<br>
{{ trans('enquiry.inputs.phone') }}: {{ FormatHelper::trim(Request::input('phone'), '-') }}<br>
{{ trans('enquiry.inputs.email') }}: {{ FormatHelper::trim(Request::input('email'), '-') }}<br>

@if ($emailToCompany)
    <br><hr>
    IP: {{ Request::ip() }}<br>
    host: {{ Request::server('HTTP_HOST') }}<br>
    wejście z: {{ Session::get('referer') }}
@endif

@endsection