<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="margin: 0; padding: 0;">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Бланк запроса</title>
</head>
<body bgcolor="#FFFFFF" style="-webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0;">
<table style="width: 100%; margin: 0; padding: 0;">
<tr style="margin: 0; padding: 0;">
<td style="margin: 0; padding: 0;"></td>
<td bgcolor="#FFFFFF" style="display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 0;">
<div style="max-width: 600px; display: block; margin: 0 auto; padding: 10px;">
<table style="border-bottom-width: 1px; border-bottom-color: black; border-bottom-style: solid; width: 100%; margin: 0; padding: 0;">
<tr style="margin: 0; padding: 0;">
<td style="margin: 0; padding: 0;">
<table width="100%" cellpadding="0" cellspacing="0" style="border-bottom-style: solid; border-bottom-color: black; border-bottom-width: 1px; width: 100%; margin: 0; padding: 0px;">
<tr style="margin: 0; padding: 0;">
<td style="margin: 0; padding: 0px;">
<table align="left" cellpadding="0" cellspacing="0" width="320" style="width:310px; min-width: 320px; float: left; margin: 0; padding: 0;">
<tr style="margin: 0; padding: 0;">
<td style="margin: 0; padding: 0px;"><img alt="" title="" src="{{ $message->embed(public_path().'/images/logo-mail.png') }}" style="width:295px; height:78px; margin-bottom:15px" /></td>
</tr>
</table>	
<table align="left" cellpadding="0" cellspacing="0" width="250" style="width: 250px; min-width: 250px; float: left; margin: 0; padding: 0;">
<tr style="margin: 0; padding: 0;">
<td style="margin: 0; padding: 0px 0px 13px;">				
<p style="font-family: Tahoma, Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0;">
<strong>{{ Config::get('content.company_address.name') }}</strong><br>
{{ Config::get('content.company_address.street') }}, {{ Config::get('content.company_address.city') }}<br>
{{ Config::get('content.company_address.country') }}<br>
Телефон: <a style="color:#337ab7" href="tel:{{ Config::get('content.company_address.phone_url') }}">{{ Config::get('content.company_address.phone') }}</a><br>
E-mail: <a style="color:#337ab7" href="mailto:{{ Config::get('content.company_address.email') }}">{{ Config::get('content.company_address.email') }}</a>
</p>
</td>
</tr>
</table>
<span style="display: block; clear: both; margin: 0; padding: 0;"></span>	
</td>
</tr>
</table>
<br style="margin: 0; padding: 0;" />
<div style="font-family: Tahoma, Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0;">
@yield('content')					
</div>
<br style="margin: 0; padding: 0;" />		
</td>
</tr>
</table>
</div>
</td>
<td style="margin: 0; padding: 0;"></td>
</tr>
</table>
</body>
</html>
