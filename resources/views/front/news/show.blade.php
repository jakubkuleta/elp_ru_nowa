@php
$content = $news->content;
$content = str_replace('xxxx', asset('images/news/'.$news->news_id), $content);
$content = preg_replace('#(href|src)="([^:"]*)(?:")#','$1="'.asset('images/news/'.$news->news_id.'/'.'$2').'"',$content);
@endphp

@extends('front.layouts.master')
@section('content')
<div class="container news">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb')
    </ul>
    <div class="row">
        <div id="content" class="col-sm-12 article">
            <div class="row">
                <div class="col-sm-3">
                    <img style="" src="{{ ViewHelper::newsImage($news) }}" alt="" title="">
                </div>
                <div class="col-sm-9 article-content">
                    @if ($news->news_id != 4)<h1>{{ $news->title }}</h1>@endif
                    <p>{{ FormatHelper::date($news->created_at) }}</p>  
                    {!! $content !!}
                </div>
            </div>
        </div>
    </div>
<hr>
</div>
@endsection