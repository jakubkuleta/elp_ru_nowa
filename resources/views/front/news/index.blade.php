@extends('front.layouts.master')
@section('content')
<div id="news-page" class="container news newslist">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb')
    </ul>

    @foreach ($news as $item)
    <div class="row news-article">
        <div class="col-sm-2">
            <a href="{{ UrlHelper::news($item) }}"><img src="{{ ViewHelper::newsImage($item) }}" alt="" title="" /></a>
        </div>
        <div class="col-sm-10">
            <h1 class="rochester font-gold news-title"><a href="{{ UrlHelper::news($item) }}">{{ $item->title }}</a></h1>			
            {{ $item->intro }}
            <div class="date">{{ FormatHelper::date($item->created_at) }}</div>
        </div>
        <hr>
    </div>
    <hr >
    @endforeach

    @if ($news->links())
        <div style="text-align:center">
        {{ $news->links() }}
        </div>
        <hr >
    @endif

</div>
@endsection