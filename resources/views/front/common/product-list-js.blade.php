<script>
$(document).ready(function () {
    var maxHeight = -1;

    $('#product-list .name').each(function() {
        maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
    });

    $('#product-list .name').each(function() {
        $(this).height(maxHeight);
    });
});
</script>