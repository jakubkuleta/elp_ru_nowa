@if (isset($showmenu)) <li><a href=""><i class="fa fa-bars" id="showmenu"></i></a></li>@endif
<li><a href="{!! UrlHelper::home() !!}"><i class="fa fa-home"></i></a></li>
@if ($breadcrumbs)
    @foreach ($breadcrumbs as $item)        
    <li><a href="{{ $item->url }}">{{ $item->name }} </a></li>
    @endforeach
@endif