@extends('front.layouts.master')
@section('content')
<div id="category-page" class="container">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb')
    </ul>
    <div class="row">

        <div id="content" class="col-sm-12">
            <div class="col-lg-12">
                <div class="desc" style="margin-left:0; margin-bottom:20px">
                    <h1 class="category-title">{{ $sitemap->name }}</h1>   
                    @if ($sitemap->description)
                        <p>{!! $sitemap->description !!}</p>
                    @endif
                </div>
            </div>

            <div class="row sitemap-products" id="product-list">
                <div class="col-xs-12">
                    @foreach ($products as $item)
                    <div class="col-xs-6 col-sm-3 category">
                        <a href="{{ UrlHelper::product($item) }}">
                            <div class="category-box">
                                <h2 class="name">{{ $item->name }}</h2>
                                <div class="img"><img src="{{ ViewHelper::productImage($item, 'list') }}" alt="{{ $item->name }}" title="{{ $item->name }}"></div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <hr>    
</div>
@include('front.common.product-list-js')
@endsection  