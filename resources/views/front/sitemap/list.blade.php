@php
$rows = count($sitemap) / 3;
$chunked = array_chunk($sitemap, $rows);
@endphp

@extends('front.layouts.master')
@section('content')
<div class="container sitemap">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb')
    </ul>
    <div class="row" style="margin: 0 -15px;">

        @for ($i = 0; $i < count($chunked); $i++)
        <div class="col-sm-4" style="padding:0 15px">
            <div class="box">
                <ul class="items">
                    @foreach ($chunked[$i] as $item)
                    <li><a href="{{ UrlHelper::sitemap($item) }}">{{ $item->name }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endfor
    </div>
    <hr>    
</div>
@endsection  