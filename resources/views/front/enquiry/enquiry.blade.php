<?php
    $style = ($productId) ? 'margin-top:30px' : '';
?>

@extends('front.layouts.master')
@section('showLayout', $productId ? false : true)
@section('content')
<div id="text-page" class="container">
<div id="cover"><div id="loader"></div></div>

    @if (!$productId)
    <ul class="breadcrumb">
        @include('front.common.breadcrumb')
    </ul>
    @endif

    <div class="row">
        <div id="content" class="enquiry" style="{!! $style !!}">
            {{ Form::open(array('method'=>'post', 'id'=>'enquiry-form', 'url'=>route('enquiry.store'))) }}
                <div class="step step1">
                    <h2>{{ trans('enquiry.labels.cam') }}</h2>	
                    <div class="step-inside">
                        <strong class="title">Выберите один из представленных вариантов монтажа хвостовика и определите размеры @if (!$productId) "Y", @endif "X", "B":</strong>
                        <ul class="ul1">
                            <li class="border"><label><img src="{{ asset('images/enquiry/A1.png') }}" alt="" width="190" height="190"><input type="radio" name="cam" value="1"><br><span>{{ trans('enquiry.options.cam.1') }}</span></label></li>
                            <li class="border"><label><img src="{{ asset('images/enquiry/A2.png') }}" alt="" width="190" height="190"><input type="radio" name="cam" value="2"><br><span>{{ trans('enquiry.options.cam.2') }}</span></label></li>
                            <li class="border"><label><img src="{{ asset('images/enquiry/A3.png') }}" alt="" width="190" height="190"><input type="radio" name="cam" value="3"><br><span>{{ trans('enquiry.options.cam.3') }}</span></label></li>
                        </ul>
                        <p>
                            @if (!$productId)<label style="display:block">Y = <input class="text" type="text" name="cam_dimension_Y" maxlength="10"> мм</label>@endif
                            <label style="display:block">X = <input class="text" type="text" name="cam_dimension_X" maxlength="10"> мм</label>
                            <label style="display:block">B = <input class="text" type="text" name="cam_dimension_B" maxlength="10"> мм</label>
                        </p>
                        <br class="cl hidden-xs">
                    </div>
                </div>

                @if (!$productId)
                <div class="step step2">
                    <h2>{{ trans('enquiry.labels.fixing_hole') }}</h2>
                    <div class="step-inside">	
                        <strong class="title">Определите размер монтажного отверстия:</strong>		
                        <p class="p1 border">
                            <img src="{{ asset('images/enquiry/B.png') }}" alt="" width="145" height="212">
                        </p>
                        <p class="p2">
                            <label style="display:block">&empty; = <input class="text" type="text" name="fixing_hole_size_F" maxlength="10"> мм</label>
                            <label style="display:block;">Z = <input class="text" type="text" name="fixing_hole_size_Z" maxlength="10"> мм</label>
                        </p>
                        <br class="cl hidden-xs">
                    </div>
                </div>	
                @endif

                <div class="step step4">
                    <h2>{{ trans('enquiry.labels.position') }}</h2>
                    <div class="step-inside">	
                        <strong class="title">Определите положение хвостовика при закрытом замке:</strong>		
                        <p class="border"><img src="{{ asset('images/enquiry/D.png') }}" alt="" width="120" height="120"></p>
                        <ul class="ul2">
                            <li><label><input class="check" type="radio" name="position" value="1"><span>{{ trans('enquiry.options.position.1') }}</span></label></li>
                            <li><label><input class="check" type="radio" name="position" value="2"><span>{{ trans('enquiry.options.position.2') }}</span></label></li>
                            <li><label><input class="check" type="radio" name="position" value="3"><span>{{ trans('enquiry.options.position.3') }}</span></label></li>
                            <li><label><input class="check" type="radio" name="position" value="4"><span>{{ trans('enquiry.options.position.4') }}</span></label></li>
                        </ul>
                        <br class="cl">			
                    </div>		
                </div>	

                <div class="step step3">
                    <h2>{{ trans('enquiry.labels.movement') }}</h2>
                    <div class="step-inside">		
                        <strong class="title">Определите оборот ключа в пустом квадрате под рисунком.<br>Стрелка обозначает направление ключа для открытия замка, черная точка &bull; возможность вынуть ключ:</strong>		
                        <ul class="ul1">
                            <li class="border"><label><img src="{{ asset('images/enquiry/C1.png') }}" alt="" width="107" height="107"><input type="radio" name="movement" value="1"><br><span>{{ trans('enquiry.options.movement.1') }}</span></label></li>
                            <li class="border"><label><img src="{{ asset('images/enquiry/C2.png') }}" alt="" width="107" height="107"><input type="radio" name="movement" value="2"><br><span>{{ trans('enquiry.options.movement.2') }}</span></label></li>
                            <li class="border"><label><img src="{{ asset('images/enquiry/C3.png') }}" alt="" width="107" height="107"><input type="radio" name="movement" value="3"><br><span>{{ trans('enquiry.options.movement.3') }}</span></label></li>
                        </ul>
                        <br class="cl hidden-xs">
                    </div>
                </div>

                <div class="step step5">
                    <h2>{{ trans('enquiry.labels.key') }}</h2>		
                    <div class="step-inside">		
                    <strong class="title">Все замки:</strong>		
                        <ul class="ul2">
                            <li><label><input class="check" type="radio" name="key" value="1"><span>{{ trans('enquiry.options.key.1') }}</span></label></li>
                            <li><label><input class="check" type="radio" name="key" value="2"><span>{{ trans('enquiry.options.key.2') }}</span></label></li>
                            <li style="padding-top:15px"><label><input class="check" type="checkbox" name="masterkey" value="1"><span>{{ trans('enquiry.labels.key_master') }}</span></label></li>
                        </ul>
                      </div>
                </div>

                <div class="step step6">
                    <h2>Запрос и контактные данные</h2>	
                    <div class="step-inside">	
                    
                        <ul class="ul2">		
                            <li><label class="lab"><em>*</em>{{ trans('enquiry.inputs.enquiry') }}:</label>{{ Form::textarea('enquiry', null, array('class'=>'text req', 'style'=>'height:100px')) }}</li>			
                            <li><label class="lab"><em>*</em>{{ trans('enquiry.inputs.quantity') }}:</label>{{ Form::text('quantity', null, array('class'=>'text req', 'style'=>'width:60px', 'maxlength'=>10)) }}</li>
                            <li style="padding-top:20px"><label class="lab"><em>*</em>{{ trans('enquiry.inputs.company_name') }}:</label>{{ Form::text('company_name', array_get($enquiry, 'company_name'), array('class'=>'text req', 'maxlength'=>100)) }}</li>
                            <li><label class="lab"><em>*</em>{{ trans('enquiry.inputs.country') }}:</label>{{ Form::text('country', array_get($enquiry, 'country'), array('class'=>'text req', 'maxlength'=>100)) }}</li>
                            <li><label class="lab"><em>*</em>{{ trans('enquiry.inputs.city') }}:</label>{{ Form::text('city', array_get($enquiry, 'city'), array('class'=>'text req', 'maxlength'=>100)) }}</li>
                            <li><label class="lab"><em>*</em>{{ trans('enquiry.inputs.street') }}:</label>{{ Form::text('street', array_get($enquiry, 'street'), array('class'=>'text req', 'maxlength'=>100)) }}</li>
                            <li><label class="lab"><em>*</em>{{ trans('enquiry.inputs.postcode') }}:</label>{{ Form::text('postcode', array_get($enquiry, 'postcode'), array('class'=>'text req', 'style'=>'width:60px', 'maxlength'=>6)) }}</li>
                            <li><label class="lab"><em>*</em>{{ trans('enquiry.inputs.person') }}:</label>{{ Form::text('person', array_get($enquiry, 'person'), array('class'=>'text req', 'maxlength'=>100)) }}</li>
                            <li><label class="lab"><em>*</em>{{ trans('enquiry.inputs.phone') }}:</label>{{ Form::text('phone', array_get($enquiry, 'phone'), array('class'=>'text req', 'maxlength'=>100)) }}</li>
                            <li><label class="lab"><em>*</em>{{ trans('enquiry.inputs.email') }}:</label>{{ Form::text('email', array_get($enquiry, 'email'), array('class'=>'text req', 'maxlength'=>100)) }}</li>
                            <li style="padding-top:20px"><label class="lab hidden-xs" style="margin-right:5px"></label><label><input class="check" name="send_to_me" type="checkbox" value="1" checked="checked"><span>Отправить запрос также на мой e-mail</span></label></li>
                            <li style="padding-top:20px">

                            <div class="alert" style="display:none" id="enquiry-message"></div>                                
                            <label class="lab hidden-xs"></label><input class="submit btn btn-primary" id="enquiry-submit" type="button" value="Отправить">                            
                            </li>                            
                        </ul>

                        <br class="cl">			
                        <p class="sent" id="enquiry-sent"><strong>Благодарим.</strong><br>Запрос отправлен.<br><br><a href="" class="enquiry-again-a">[ Отправить запрос снова ]</a></p>
                        <em>*</em> - обязательно заполнить
                    </div>		
                </div>
            {{ Form::hidden('product_id', $productId) }}
            {{ Form::close() }}
        </div>
    </div>
    <hr>
</div>

<script>
jQuery(document).ready(function(){

    $('#enquiry-form').submit(function() {
        return false;
    });
    
    $('#enquiry-submit').click(function() {	

        $('#cover').show();;
        
        $.ajax({
            type: 'POST',
            data: $('#enquiry-form').serialize(),
            success: function(data) {
                try {
                    showMessage(data.status, data.message);
                    $('#cover').hide();

                    if (data.status) {
                        $('#enquiry-submit').hide();
                    }
                }
                catch(err) {
                    showError();
                }                
	        },
            error: function() {
                showError();
            }			
        });			
        
        return false;
    });		
});

function showError()
{
    showMessage(false, 'Unexpected error occurred')
    $('#cover').hide();    
}

function showMessage(status, message)
{
    var div = $('#enquiry-message');
    div.removeClass('alert-success');
    div.removeClass('alert-danger');   

    if (status) {
        div.addClass('alert-success');
    } else {
        div.addClass('alert-danger');
    }

    div.html(message).show();
}
</script>
@endsection