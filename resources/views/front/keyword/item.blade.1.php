@extends('front.layouts.master')
@section('content')
<div class="container text-page about">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb')
    </ul>
    <div class="row">
        <div class="col-sm-6 text">
            <h1>{{ $title }}</h1>
            @include('front.keyword.keyword-'.$keyword)

        </div>
        <div class="col-sm-1"></div>
        <div class="col-sm-4">
            <div class="about-images">
                <div class="about-image">
                    <img class="img-responsive" src="{{ asset('images/cadimg.jpg') }}" alt="" title="">
                </div>
                <div class="about-image">
                    <img class="img-responsive" src="{{ asset('images/lockkey01.jpg') }}" alt="" title="">
                </div>
            </div>
        </div>
        <div class="col-sm-1"></div>
    </div>
    <hr>
</div>
@endsection
