@inject('productRepository', 'App\Repositories\ProductRepository')

@php
$products = $productRepository->getItemsByProductId([24, 25, 72, 79]);
@endphp

<p>Мало кто задумывается при выборе <strong>почтовых замков</strong> о том, что такое угроза конфиденциальности. А стоило бы.</p>
<p>Основной функцией ящиков, одежных шкафов, локеров, ячеек и других изделий, в которых установлен данный тип замка, является ограничение доступа к их содержимому. В случае с некачественными <strong>почтовыми замками</strong>, где зачастую ключом от одного почтового ящика или шкафчика, можно открыть другое или даже несколько других изделий, данная функция не выполняется, и к вашим личным вещам может получить доступ, например, ваш сосед по подъезду или раздевалке либо иное лицо с неясными намерениями.</p>
<p>Поэтому <strong>почтовые замки</strong> должны быть прежде всего надёжными. Уровень секретности должен строго соответствовать заявленному и комбинации ключей не должны повторяться в границах конкретного общего пространства.</p>
<p>Именно такие надёжные и качественные <strong>почтовые замки</strong> производит компания Euro-Locks, которая является европейским лидером в производстве замков и запирающих систем.</p>

<div id="category-page" style="padding-top:20px">
    <div id="content" class="col-sm-12">
        <div class="row" id="product-list">
            @foreach ($products as $item)
                <div class="col-xs-6 col-sm-3 category">
                    <a href="{{ UrlHelper::product($item) }}">
                        <div class="category-box" style="margin-left:0">
                            <h2 class="name">{{ $item->name }}</h2>
                            <div class="img"><img src="{{ ViewHelper::productImage($item, 'list') }}" alt="{{ $item->name }}" title="{{ $item->name }}"></div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>