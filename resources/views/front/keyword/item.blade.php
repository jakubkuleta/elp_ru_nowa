@extends('front.layouts.master')
@section('content')
<div class="container text-page about">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb')
    </ul>
    <div class="row">
        <div class="col-sm-12">
            <h1>{{ $title }}</h1>
            @include('front.keyword.keyword-'.$keyword)
        </div>
    </div>
    <hr>
</div>
@include('front.common.product-list-js')
@endsection
