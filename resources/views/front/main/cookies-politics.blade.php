@extends('front.layouts.master')
@section('content')
<div class="container text-page">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb')
    </ul>

    <div id="content" class="col-sm-12">
        <div class="row">
            <h1>{{ trans('content.cookies_politics') }}</h1>
            <p>EURO-LOCKS Sp. z o. o., ul. Pawła 29, 41-708 Ruda Śląska, Польша информирует, что сервис {{ Config::get('content.domain') }} использует механизм "cookies", действие которого основано на записи фрагментов текстовой информации на диске пользователя. "Cookies" используются в статистических, функциональных целях, а также для правильной работы сервиса.</p> 
            <p>Каждый вид браузера предлагает возможность блокировки и уничтожения файлов "cookies". Блокировка файлов "cookies" приведет к потере возможности пользования некоторыми функциями сервиса (напр. входа).</p>
        </div>
        <hr>
    </div>

</div>   
@endsection