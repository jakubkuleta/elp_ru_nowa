@extends('front.layouts.master')
@section('content')
<div class="container text-page">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb')
    </ul>
    <div class="row">
        <div id="content" class="col-sm-12">
            <div class="row">
                <div class="locations col-lg-10 col-md-10">
                    <h1>Расположение</h1>
                    <p>&nbsp;</p>
                    <p>Отдел Euro-Locks Sp. z o.o. находящится в Руде Слёнской (Польша), здесь мы проектируем и производим значительную часть наших продуктов. Наш отдел специализируется на обслуживании следующих стран:</p>
                    <div class="locationlist">
                        <div class="location row">
                            <div class="col-sm-3 col-xs-6">Азербайджан</div>
                            <div class="col-sm-3 col-xs-6">Армения</div>
                            <div class="col-sm-3 col-xs-6">Беларусь</div>
                            <div class="col-sm-3 col-xs-6">Грузия</div>
                        </div>
                        <div class="location row">
                            <div class="col-sm-3 col-xs-6">Казахстан</div>
                            <div class="col-sm-3 col-xs-6">Латвия</div>
                            <div class="col-sm-3 col-xs-6">Литва</div>
                            <div class="col-sm-3 col-xs-6">Польша</div>
                        </div>
                        <div class="location row">
                            <div class="col-sm-3 col-xs-6">Россия</div>
                            <div class="col-sm-3 col-xs-6">Таджикистан</div>
                            <div class="col-sm-3 col-xs-6">Туркменистан</div>
                            <div class="col-sm-3 col-xs-6">Узбекистан</div>
                        </div>
                        <div class="location row">
                            <div class="col-sm-3 col-xs-6">Украина</div>
                            <div class="col-sm-3 col-xs-6">Эстония</div>
                        </div>   
                    </div>
                </div>
            </div>
            <hr>
        </div>
    </div>
</div>
@endsection  