@extends('front.layouts.master')
@section('content')
<div class="container text-page">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb')
    </ul>
    <div class="row">
        <div class="col-sm-6 pr-30-md-up">
            <h1>{{ trans('content.about') }}</h1>
            
            <p>Фирма Euro-Locks Sp. z o. o. является частью группы компаний Lowe & Fletcher, занимающейся проектированием, производством и поставкой замков клиентам из разных стран мира. Фирма была основана в 1889 г. и за более чем вековую историю приобрела богатый опыт, сохранив при этом формат семейной компании. В настоящее время мы специализируемся в производстве запирающих систем, предназначенных для различных отраслей промышленности, а заводы группы производят более 60 миллионов изделий ежегодно.</p>
            <p>Вы можете перейти по ссылкам ниже, чтобы узнать больше о фирмах Euro-Locks Sp. z o.o. и Lowe & Fletcher, а также ознакомиться с нашими ценностями и производственными возможностями.</p>

            <ul style="padding-top:15px; padding-bottom:15px">
            @foreach ($aboutCategories as $link)
                <li><a class="strong" href="{{ $link->url }}">{{ $link->title }}</a></li>
            @endforeach
            </ul>
        </div>

        <div class="col-sm-6">
            <div class="about-images">
                <div class="embed-responsive embed-responsive-4by3">
                    <iframe class="embed-responsive-item" src="//www.youtube.com/embed/nD5zc0crffM?rel=0" allowfullscreen></iframe>
                </div>                
            </div>
        </div>
    </div>
    <hr>
</div>
@endsection
