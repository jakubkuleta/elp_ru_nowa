﻿@extends('front.layouts.master')
@section('content')

<!--
<div class="alert alert-info" role="alert">
  <strong>Уважаемые господа</strong>, В связи с праздниками инвентаризацией сообщаем, что 10-12.06.2020 наш завод будет закрыт.
</div>
-->

<section class="slider">
    <ul class="theslider">

        <li class="slide">
            <div style="background: url('{{ asset('images/slider/8.jpg')}}') 50% 50%/cover;" class="slide-image"></div>
            <div class="text-wrap container">
                <div class="slider-text">
                    <p></p>
                </div>
            </div>
        </li>
        <li class="slide">
            <div style="background: url('{{ asset('images/slider/9.jpg')}}') 50% 50%/cover;" class="slide-image"></div>
            <div class="text-wrap container">
                <div class="slider-text">
                    <p></p>
                </div>
            </div>
        </li>
        <li class="slide">
            <div style="background: url('{{ asset('images/slider/1.jpg')}}') 50% 50%/cover;" class="slide-image"></div>
            <div class="text-wrap container">
                <div class="slider-text">
                    <h2>Высокоточное производство</h2>
                    <p></p>
                </div>
            </div>
        </li>
        <li class="slide">
            <div style="background: url('{{ asset('images/slider/2.jpg')}}') 50% 50%/cover;" class="slide-image"></div>
            <div class="text-wrap container">
                <div class="slider-text">
                     <h2>Инновационность</h2>
                    <p></p>
                </div>
            </div>
        </li>
        <li class="slide">
            <div style="background: url('{{ asset('images/slider/3.jpg')}}')50% 50%/cover;" class="slide-image"></div>
            <div class="text-wrap container">
                <div class="slider-text">
                    <h2>Дизайн</h2>
                    <p></p>
                </div>
            </div>
        </li>
        <li class="slide">
            <div style="background: url('{{ asset('images/slider/4.jpg')}}') 50% 50%/cover;" class="slide-image"></div>
            <div class="text-wrap container">
                <div class="slider-text">
                    <h2>Опыт, передаваемый из поколения в поколение</h2>
                    <p></p>
                </div>
            </div>
        </li>
        <li class="slide">
            <div style="background: url('{{ asset('images/slider/5.jpg')}}') 50% 50%/cover;" class="slide-image"></div>
            <div class="text-wrap container">
                <div class="slider-text">
                    <h2>Многообразие решений</h2>
                    <p></p>
                </div>
            </div>
        </li>
        <li class="slide">
            <div style="background: url('{{ asset('images/slider/6.jpg')}}') 50% 50%/cover;" class="slide-image"></div>
            <div class="text-wrap container">
                <div class="slider-text">
                    <h2>Всемирный масштаб</h2>
                    <p></p>
                </div>
            </div>
        </li>
        <li class="slide">
            <div style="background: url('{{ asset('images/slider/7.jpg')}}') 50% 50%/cover;" class="slide-image"></div>
            <div class="text-wrap container">
                <div class="slider-text">
                    <h2>Euro-Locks в отраслевых СМИ</h2>
	    <br>
                    <p><a href="http://www.metalinfo.ru/ru/news/101201" style="color:white">Металлоснабжение и сбыт</a></p>
                    <p><a href="https://euro-locks.promportal.su/firm_news/26989/novie-shifrovie-zamki" style="color:white">PromPortal</a></p>
                    <p><a href="http://news.stroit.ru/v-text/i-24478.html" style="color:white">Стройка</a></p>
                    <p><a href="https://industrymebel.ru/2018/03/modernizirovannye-sistemy-zapiraniya-novinka-na-rossijskom-rynke/" style="color:white">Индустрия мебели</a></p>
                </div>
            </div>
        </li>
    </ul>
</section>
<script>
$('.theslider').owlCarousel({
    navigation : true, // Show next and prev buttons
    slideSpeed : 300,
    autoPlay: 15000,

    paginationSpeed : 400,
    navigationText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
    singleItem:true
});
</script>


<section id="hp-intro">
<div class="container">
    <div class="row">
        <div class="col-lg-12">

            <?php /*
            <div class="alert alert-danger" style="margin-top:20px;border-width:3px">
            Уважаемые Господа,<br><br>
            Доводим до Вашего сведения,что с  15 по  19 июня 2017 года наш завод не работает.<br>
            Отгрузка товара в эти дни осуществляться не будет.<br>
            Просим Вас провести анализ потребности в товарах нашей фирмы и заранее разместить соответствующие заказы.
            </div>
            */?>

            <div class="intro">

            @if (!Session::has('home-message'))

            <script>
            $(window).load(function () {
                alert("Обновление информации по COVID-19. В данный момент все ключевые аспекты деятельности нашей группы компаний восстановлены и мы по-прежнему в состоянии предоставить нашим клиентам полный спектр современных запирающих систем. Мы предприняли необходимые меры по обеспечению социальной дистанции и санитарных мер в рамках нашей деятельности, чтобы все наши коллеги могли работать безопасно и эффективно. Наши инженеры и специалисты по продажам всегда готовы оказать помощь по любым запросам, касающимися наших изделий. В эти трудные времена мы по-прежнему твердо намерены разрабатывать и производить механические и электронные замки на наших хорошо оборудованных заводах в Европе и Америке. Это позволяет нам предлагать гибкий источник стандартных и произведенных на заказ систем запирания, которые мы целенаправленно изготовляем самостоятельно и поставляем по всему миру как в малых, так и в больших объемах. Спасибо за вашу поддержку. Мы надеемся, что все находятся в безопасности в это ненадежное и беспрецедентное время."
);})
            </script>
            @php
            Session::set('home-message', true);
            @endphp
            @endif

				<br>
				<!--
				<h1>Внимание! Информация о праздничном перерыве</h1>
				 <p>Уважаемые господа,
				В связи с праздниками и запланированной инвентаризацией сообщаем, что 24.12.2018 - 02.01.2019 наш завод будет закрыт.
				В эти дни не будут осуществляться поставки товаров.
				В связи с вышеизложенным, просим проанализировать Ваши потребности и разместить заказы заранее.
				</p> -->
                <h1>Запирающие системы для промышленности</h1>
                <p>Группа Euro-Locks разрабатывает и производит запирающие системы для широкого круга производственных компаний. Наши фабрики поставляют более 60 миллионов изделий ежегодно. В ассортименте компании представлены как простые механические замки, так и сложные электронные замки, а также многочисленные индивидуальные решения, благодаря чему наша продукция отвечает разнообразным требованиям клиентов.</p>
            </div>

            <section id="hp-blocks">
                <div class="row">
                    <div id="content" class="col-lg-12">

                        <div class="col-md-6 col-sm-6">
                            <div class="block">
                                <div class="header">
                                    <h2>Сертификаты</h2>
                                </div>
                                <div class="content">
                                    <a href="{!! UrlHelper::certificates() !!}"><img src="images/certificates.jpg" width="224" alt="" title="" style="margin:0 0 15px 0;border:solid 1px rgb(169, 169, 169)"></a>
                                    <p style="margin-bottom:30px">Наша компания имеет сертификаты ISO9001, ГОСТ, VDS, IMP, подтверждающие опыт и профессионализм команды Euro-Locks, а также соблюдение международных стандартов управления качеством.</p>
                                    <a class="btn" href="{!! UrlHelper::certificates() !!}">Перейти</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="block">
                                <div class="header">
                                    <h2>Новости</h2>
                                </div>
                                <div class="content">
                                    @foreach ($news as $item)
                                    @if ($item->news_id == 4)
                                    <div class="row newsitem" style="padding:5px 0 0 5px; color:#a94442; background-color:#f2dede;border: solid 2px #a94442">
                                        <img class="pull-left" src="{{ ViewHelper::newsImage($item) }}" width="40" height="40" style="margin-bottom:15px;margin-right:7px" alt="" title="">
                                        <p><a href="{{ UrlHelper::news($item) }}">{{ $item->title }}</a></p>
                                    </div>
                                    @else
                                    <div class="row newsitem">
                                        <img class="pull-left" src="{{ ViewHelper::newsImage($item) }}" width="50" height="50" alt="" title="">
                                        <p><a href="{{ UrlHelper::news($item) }}">{{ $item->title }}</a></p>
                                    </div>
                                    @endif
                                    <hr>
                                    @endforeach
                                    <div style="margin-bottom:30px"></div>
                                    <a class="btn" href="{!! UrlHelper::news() !!}">Перейти</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

                <p>Наше предложение охватывает кулачковые замки, <strong>pochtovye_zamki</strong>, <strong>почтовые замки</strong></a>, <strong>замки для металлической мебели</strong>, <strong>замки для деревянной мебели</strong>, <strong>замки на фигурный ключ</strong>, <strong>переключатели</strong>, <strong>электронные замки</strong>, <strong>механические кодовые замки</strong>, <strong>депозитные монетные замки</strong>, <strong>пиновые замки</strong>, <strong>ручки</strong>, <strong>сувальдные замки</strong>, <strong>водонепроницаемые замки</strong>, а также <strong>специальные замки</strong>.</p>
            <p>Мы поставляем запирающие системы ведущим производителям <strong>шкафов</strong>, <strong>сейфов</strong>, <strong>деревянной и металлической офисной мебели</strong>, <strong>медицинской мебели</strong>, <strong>автобоксов</strong>, <strong>багажников для лыж</strong>, <strong>замков для велосипедов</strong>, <strong>крышек бензобаков</strong>, <strong>почтовых ящиков</strong>, <strong>снековых</strong>, <strong>кофейных</strong>, а также <strong>игровых автоматов</strong>, <strong>оборудования</strong>, <strong>фаркопов</strong>, <strong>окон и дверей</strong>, <strong>корпусов энергетических</strong>, <strong>коммуникационных и промышленных шкафов</strong> и др. Наша продукция используется везде, где требуется ограничение доступа: в домах, школах, базах отдыха, предприятиях и поликлиниках.</p>
            <p>Мы являемся частью Группы <a href="http://www.lowe-and-fletcher.com/">Lowe&amp;Fletcher</a> - семейной фирмы, основанной в 1889 году, возглавляемой уже пятым поколением основателей.</p>

        </div>
    </div>
</div>
<hr />
</section>

<script>
function resizeBlocks() {

    if ($(window).width() > 780) {

        max_height = 0;
        $('#hp-blocks .block').css({height: 'auto'});

        $('#hp-blocks .block').each(function () {
            if ($(this).outerHeight() > max_height) {
                max_height = $(this).outerHeight();
            }
        });

        max_height = Math.max(max_height,420);
        $('#hp-blocks .block').css({height: max_height + 'px'});

    } else {
        $('#hp-blocks .block').css({height: 'auto'});
    }
}

$(window).load(function () {
    resizeBlocks();
});

$(window).resize(function () {
    resizeBlocks();
});
</script>

@endsection
