<div class="people row">
    @foreach ($people as $person)
    @php
        $mail = array_get($person, 'email');

        $phone = array_get($person, 'phone');
        $phoneLink = array_get($person, 'phoneLink', str_replace(array('-', ' '), '', $phone));

        $mobile = array_get($person, 'mobile');
        $mobileLink = str_replace(array('-', ' '), '', $mobile);
    @endphp
    <div class="person col-sm-3 col-xs-6">
        <div class="person-inner">
            <img src="{{ asset('images/our-team/'.$person['img']) }}" alt="" title"">
            <h2>{{ $person['name'] }}</h2>
            <span class="position">{{ $person['position'] }}</span>
            @if (1 == 2)
            @if ($phone)<span>тел.: <a href="tel:{{ $phoneLink }}">{{ $phone }}</a></span>@endif
            @if ($mobile)<span>Моб.: <a href="tel:{{ $mobileLink }}">{{ $mobile }}</a></span>@endif
            @if ($mail)<span><a href="mailto:{{ $mail }}">{{ $mail }}</a></span>@endif
            @endif
        </div>
    </div>
    @endforeach
</div>