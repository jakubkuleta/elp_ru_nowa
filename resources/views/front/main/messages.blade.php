@extends('front.layouts.master')
@section('content')
<div id="text-page" class="container">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb')
    </ul>

    <div class="row messages">
        <p>
            <h5><strong>Nasza firma jest beneficjentem Programu Operacyjnego Innowacyjna Gospodarka<br>"Dotacje na innowacje"</strong></h5>
            <img src="{{ asset('images/messages/logo_eu.jpg') }}" usemap="#map" alt="" title="" class="img-responsive">
        </p>
        <hr>

        <p>
            <h5><strong>Firma Euro-Locks jest partnerem wspierającym V Europejski Kongres MiP</strong></h5>
            <a href="http://www.ekmsp.eu/pl/" target="_blank"><img src="{{ asset('images/messages/Banner_EKMSP.jpg') }}" alt="" title="" class="img-responsive"></a>
        </p>
        <hr>

        <p>
        <a href="http://rig.katowice.pl/" target="_blank"><img src="{{ asset('images/messages/rig2.png') }}" alt="" title="" class="img-responsive"></a>
        </p>
        <hr>

        <map name="map">
        <area shape="rect" coords="0,0,233 ,88" href="http://www.poig.gov.pl" alt="">
        <area shape="rect" coords="469,0,681 ,88" href="http://europa.eu/index_pl.htm" alt="">
        </map>      
    </div>
</div>    
@endsection