@extends('front.layouts.master')
@section('content')
<div class="container text-page">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb')
    </ul>

    <div class="col-sm-12">
        <div class="row">
            <h1>{{ trans('content.brand_protection') }}</h1>
            <p>Компания EURO-LOCKS желает довести до сведения, что группе компаний EURO-LOCKS принадлежат исключительные права на использование товарных знаков номер 552144 и 335922 в словесной форме – «EURO-LOCKS» или словесно-графической форме</p>
            <p style="text-align:center"><img src="{{ asset('images/logo2.png') }}" alt="" title=""></p>
			<p style="text-align:center"><img src="{{ asset('images/logoEN.png') }}" alt="" title=""></p>
			<p style="text-align:center"><img src="{{ asset('images/logoRU.png') }}" alt="" title=""></p>
            <p>с целью обозначения продуктов в области производства замков и запирающих систем. В связи с вышесказанным, предложение и введение в оборот продуктов в области замков и запирающих систем, обозначенных этими знаками, а также использование в интернет-ресурсах знаков, похожих на эти, предоставляют владельцу данной марки право на обращение в судебные органы, согласно законодательству Российской Федерации, с иском, содержащим, кроме всего прочего, требования наложения ареста на данные товары и возврата незаконно полученной материальной выгоды, а также выплаты соответствующей компенсации.</p>
            
        </div>
        <hr>
    </div>
</div>
@endsection