﻿@extends('front.layouts.master')
@section('content')
<div class="container text-page certificates">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb')
    </ul>

    <div class="row">
        <div id="content" class="col-sm-12">
            <h1>{{ trans('content.certificates') }}</h1>

            <div class="col-xs-12 col-sm-4">
                <div class="certificate">
                    <p class="name">Сертификат ISO 9001:2015 Система менеджмента качества.</p>
                    <a href="http://www.euro-locks.com.ru/download/ecert_Euro-Locks.pdf" class="popup-gallery"><img class="img-responsive" src="http://www.euro-locks.com.ru/images/certificates/quality_cert_ru.jpg"  alt="Сертификат ISO 9001:2015 Система менеджмента качества." title=""></a>
                </div>
            </div>
        </div>    
    </div>

    <hr>    
</div>    
@endsection    