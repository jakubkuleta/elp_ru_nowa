@extends('front.layouts.master')
@section('content')
<div class="container text-page">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb')
    </ul>
    <div class="row">
        <div class="col-sm-12">
            <h1>{{ trans('content.our_team') }}</h1>
            @include('front.main.our-team-people')
        </div>
    </div>
    <hr>
</div>
@endsection