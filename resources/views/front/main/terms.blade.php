@extends('front.layouts.master')
@section('content')
<div class="container text-page">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb')
    </ul>

    <div id="content" class="col-sm-12">
        <div class="row">
            <h1>{{ trans('content.terms') }}</h1>

            <p><a href="{{ UrlHelper::download('obshchie-usloviya-prodazhi-i-postavok.pdf') }}"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Общие условия продажи и поставок</a> (129 KB)</p>
            <p><a href="{{ UrlHelper::download('general-conditions-of-sale-and-delivery.pdf') }}"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> General Conditions of Sale and Delivery</a> (158 KB)</p>
            <p>&nbsp;</p>
            <p><a href="{{ UrlHelper::download('podrobnyi-poriadok-dejjstvijj-v-sluchae-vozniknovenija-pretenzi.pdf') }}"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Подробный порядок действий в случае возникновения претензий</a> (37 KB)</p>
            <p><a href="{{ UrlHelper::download('detailed-rules-of-procedure.pdf') }}"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Detailed rules of procedure</a> (20 KB)</p>
            <p>&nbsp;</p>
            <p><a href="{{ UrlHelper::download('protokol-po-rassmotreniyu-pretenzii.doc') }}"><i class="fa fa-file-word-o" aria-hidden="true"></i> Протокол по рассмотрению претензии</a> (143 KB)</p>
        </div>
        <hr>
    </div>
</div>
@endsection