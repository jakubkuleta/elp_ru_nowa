@extends('front.layouts.master')
@section('content')
<div class="container text-page">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb')
    </ul>
    <div class="row">
        <div id="content" class="col-sm-6 contact-info-block">
            <h1>{{ trans('content.contact') }}</h1>

             <strong>{{ Config::get('content.company_address.name') }}</strong><br>
            {{ Config::get('content.company_address.street') }}<br>
            {{ Config::get('content.company_address.city') }}<br>
            {{ Config::get('content.company_address.country') }}<br>

            <p></p>
            тел.: {{ Config::get('content.company_address.phone') }}<br>
            факс: {{ Config::get('content.company_address.fax') }}
            
            <p></p>
            тел Москва.: <a href="tel:+74991309633">+7 (499) 130-96-33</a><br>
            Моб.: <a href="tel:+79031309633">+7 (903) 130-96-33</a>

            <p></p>
            www: <a href="http://www.euro-locks.pl">www.euro-locks.pl</a>

            <br><br>
            <strong>Контакт на русском языке:</strong>

            @php
            $people = array_where($people, function ($key, $value) {return isset($value['orderInContact']);});
            $people = array_values(array_sort($people, function ($value) {return $value['orderInContact'];}));
            @endphp

            @foreach ($people as $person)
                @php
                    $mail = array_get($person, 'email');

                    $mobile = array_get($person, 'mobile');
                    $mobileLink = str_replace(array('-', ' ', '(', ')'), '', $mobile);

                    $area = array_get($person, 'area');
                @endphp

            <div class="line-block">
                <span class="title">{{ $person['name'] }}</span><br>
                {{ $person['position'] }}
                @if ($mobile)<br>Моб.: <a href="tel:{{ $mobileLink }}">{{ $mobile }}</a>@endif
                @if ($mail)<br><a href="mailto:{{ $mail }}">{{ $mail }}</a>@endif
                @if ($area)<br><br>Территория:<br>{{ $area }}@endif
            </div>
            @endforeach

        </div>

        <div class="col-sm-6">
            <div>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2548.0921556919234!2d18.872870015726303!3d50.30887227945608!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xee4811f46cac79a1!2sEuro+Locks!5e0!3m2!1sen!2sus!4v1460449412879" style="width:100%;height:450px;border:0" allowfullscreen></iframe>                
            </div>
        </div>
    </div>
    <hr>
</div>
@endsection    