@extends('front.layouts.master')
@section('content')
<div class="container text-page">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb')
    </ul>

    <div class="col-sm-12">
        <div class="row">
            <h1>{{ trans('content.lowe_and_fletcher') }}</h1>

            <img class="img-responsive image-pull-right image-pull-right-margin xs-float-none" alt="" title="" width="362" height="216" src="{{ asset('images/about/lowe_and_fletcher_group.png') }}">

            <p>Компания Lowe & Fletcher была основана в 1889 г. Именно тогда Джон Лау и Томас Флетчер решили заняться производством замков.<br>Местом расположения предприятия стал Вилленхол – сердце британского замочно-скобяного производства в прославленном регионе Блэк Кантри, промышленном центре Британской империи.</p>
            <p>Сегодня компания Lowe & Fletcher, совместно с Euro-Locks, входит в состав <a href="http://www.lowe-and-fletcher.com/locking/">Подразделения Запирающих Систем</a>, в свою очередь являющегося частью Группы Компаний Lowe & Fletcher. Будучи представителями пятого поколения в семейном бизнесе, мы гордимся нашим наследием, которое позволяет нам уверенно смотреть в будущее и строить долгосрочные перспективы.</p>
            <p>Производство запирающих систем по-прежнему остается основным направлением работы Группы Компаний Lowe & Fletcher. Однако сфера деятельности компании была расширена и теперь включает также производство <a href="http://www.lowe-and-fletcher.com/safety-and-security/">Систем охраны и безопасности</a>, используемых в зданиях. Этим сегментом рынка занимаются фирмы <a href="http://www.blegroup.co.uk/">BLE</a> и <a href="http://www.coopersfire.com/">Coopers Fire</a>.</p>
            <p>Перейдите по ссылке, чтобы узнать больше о <a href="http://www.lowe-and-fletcher.com/">Группе Компаний Lowe & Fletcher</a>.</p>
        </div>
        <hr>
    </div>
</div>   
@endsection





