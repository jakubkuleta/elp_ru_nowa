@extends('front.layouts.master')
@section('content')

<div id="category-page" class="container">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb', array('showmenu'=>true))
    </ul>
    <div class="row">
        <!-- include('front.products.categories') -->

        <div id="content" class="col-sm-9">
            <!-- to jest główna lista wyświetlająca kafelki -->
            <div class="row" id="product-list">
                <div class="col-lg-12">
                    <div class="desc">
                        <h1 class="category-title">{{ $breadcrumbs ? $breadcrumbs[(array_key_last($breadcrumbs))]->name : '' }}</h1>
                    </div>
                </div>
                @php
                @endphp
                @if ($isProductList)
                    @foreach ($products as $item)
                    <div class="col-xs-6 col-sm-4 category">
                        <a href="{{ route('products.show', ['category_id' => $category_id, 'name' => str_slug($item->name), 'id' => $item->product_id]) /*UrlHelper::product($item)*/ }}">
                            <div class="category-box">
                                <h2 class="name">{{ $item->name }}</h2>
                                <div class="img"><img src="{{ ViewHelper::productImage($item, 'list') }}" alt="{{ $item->name }}" title="{{ $item->name }}"></div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                @else
                    @foreach ($categories as $category)
                    <div class="col-xs-6 col-sm-4 category">
                        <a href="{{ route($category->route_name, ['name' => $category->path_name, 'id' => $category->category_id]) }}">
                            <div class="category-box">
                                <h2 class="name">{{ $category->name }}</h2>
                                <div class="img"><img src="{{ ViewHelper::categoryImage( $category->image) }}" alt="{{ $category->name}}" title="{{ $category->name }}"></div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                @endif                                                     
            </div>
        </div>
    </div>
    <hr />
</div>
@include('front.common.product-list-js')
@endsection