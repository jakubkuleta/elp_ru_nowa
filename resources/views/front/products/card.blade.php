<?php
$productName = $product->name;
$productImageBig = ViewHelper::productImage($product, 'big');
$productImageSchema = ViewHelper::productImage($product, 'schema');


//zamkniecie opisu pod tabela do <div>
$table = '';
$html = new Htmldom($product->description);
if ($html->find('table')) {
    $table = $html->find('table', 0)->outertext;
    $html->find('table', 0)->outertext = '';
}

if (trim($html)) {
    $description = $table.'<div class="description">'.$html.'</div>';
} else {
    $description = $table;
}

?>
@extends('front.layouts.master')
@section('content')
<div id="product-page" class="container">
    <ul class="breadcrumb">
    @include('front.common.breadcrumb')
    </ul>
    <div class="row">
        <div id="content" class="col-sm-12">

            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="block image product-image" style="height: 400px;">
                                <h1>{{  $productName }}</h1>
                                <div class="content white">
                                    <a href="{{ $productImageBig }}" title="{{ $productName }}" class="popup-gallery"><img src="{{ $productImageBig }}" title="{{ $productName }}" alt="{{ $productName }}"></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="block image product-schema" style="height: 400px;">
                                <div class="header">Чертеж</div>
                                <div class="content white">
                                    <a href="{{ $productImageSchema }}" title="{{ $productName }}" class="popup-gallery"><img src="{{ $productImageSchema }}" title="{{ $productName }}" alt="{{ $productName }}"></a>
                                </div>
                                <a class="btn magnificPopup-iframe" href="{{ UrlHelper::enquiry($product->product_id) }}" id="pdf_download"><i class="fa fa-envelope-o"></i> Выслать запрос на этот продукт</a>                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="block technical product-details">
                                <div class="header">Описание</div>
                                <div class="content white table-responsive">
                                    {!! $description !!}
                                    @if(count($product->productFiles))
                                    <div>
                                        <ul>
                                    @foreach($product->productFiles as $productFile)
                                        <li><a href="{{ asset('files/'.$productFile->file->name) }}" style="padding-left: 10px" target="_blank">{{ $productFile->file->title}}</a></li>
                                    @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                </div>
                            </div>                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<hr />
</div>
<script>
function sizeBoxes() {
    $('.block.image').css({'height': 'auto'});
    if ($(window).width() > 780) {
        height = Math.max($('.block.product-image').outerHeight(), $('.block.product-schema').outerHeight());
        height = height + 30; //enquiry button
        $('.block.image').css({'height': height + 'px'});
    }
}

$(window).load(function () {
    sizeBoxes();
});

$(window).resize(function () {
    sizeBoxes();
});  
</script>
@endsection