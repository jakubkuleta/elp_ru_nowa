@extends('front.layouts.master')
@section('content')
<div id="category-page" class="container">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb', array('showmenu'=>true))
    </ul>
    <div class="row">
        <div class="col-lg-12">
            <h2 class="category-title">{{ trans('content.search_results') }}</h2>
        </div>
    </div>

    <div class="row">

        <div id="content" class="col-sm-9">
            <div class="row">

                @if (!$products)
                    <div style="text-align:left; padding: 0 20px;">
                    @if (isset($noCriteria))
                        <h4>Обозначьте параметры поиска</h4>
                    @else
                        <h4>По заданным параметрам поиска ничего не найдено</h4>
                        <p>Попробуйте поиск заново</p>                        
                    @endif
                    </div>
                @endif

                <div class="col-xs-12 category" id="product-list">

                    @foreach ($products as $item)
                    <div class="col-xs-6 col-sm-4 category">
                        <a href="{{ UrlHelper::product($item) }}">
                            <div class="category-box">
                                <h2 class="name">{{ $item->name }}</h2>
                                <div class="img"><img src="{{ ViewHelper::productImage($item, 'list') }}" alt="{{ $item->name }}" title="{{ $item->name }}"></div>
                            </div>
                        </a>
                    </div>                 
                    @endforeach
                </div>
            </div>
        </div>
    </div>
<hr>
</div>
@include('front.common.product-list-js')
@endsection   