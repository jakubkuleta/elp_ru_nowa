@extends('front.layouts.master')
@section('content')
<div id="category-page" class="container technical-details">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb', array('showmenu'=>true))    
    </ul>

    <div class="row">

        <div id="content" class="col-sm-9">
            <div class="row">
                <div class="col-lg-12">
                    <div class='desc'>
                        <h1 class="category-title">{{ $category->name }}</h1>
                        @if ($category->description)
                            <p>{!! $category->description !!}</p>
                        @endif

                        <div class="images">
                            <img class="img-responsive" src="{{ asset('images/technical-details/technical-details-1.jpg') }}" alt="" title="">
                            <img class="img-responsive" src="{{ asset('images/technical-details/technical-details-2.jpg') }}" alt="" title="">
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr />
</div>
@endsection