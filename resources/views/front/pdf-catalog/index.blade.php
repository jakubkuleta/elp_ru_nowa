@extends('front.layouts.master')
@section('content')

<div id="category-page" class="container">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb')
    </ul>
    <div class="row">

        <div id="content" class="col-sm-9">
            <!-- to jest główna lista wyświetlająca kafelki -->
            <div class="row" id="product-list">
                <div class="col-lg-12">
                    <div class="desc">
                        <h1 class="category-title">Каталоги</h1>
                    </div>
                </div>

                    @foreach ($catalogs as $catalog)
                        <div class="col-xs-6 col-sm-4 category">
                                <div class="category-box">
                                    <h2 class="name"><a href="{{ $catalog->pages ? route('catalogs.show', [str_slug(trans($catalog->display_name)), 'id' => $catalog->catalog_id]) : '' }}">{{ $catalog->display_name }}</a></h2>
                                    <div class="img"><img src="{{ ViewHelper::catalogImage($catalog->image_name) }}" alt="{{ ViewHelper::catalogImage('kotek.jpg') }}" title="{{ $catalog->display_name }}"></div>
                                    <div><i class="fa fa-file-pdf-o"></i><a href="{{ UrlHelper::download($catalog->file_name) }}"> Cкачать {{ $catalog->size }}</a></div>
                                </div>
                        </div>
                    @endforeach

            </div>
        </div>
    </div>
    <hr />
</div>
@include('front.common.product-list-js')
@endsection