@php
    if ($version = array_get($details, 'version')) {
        $version = '?ver='.$version;
    }
@endphp

@extends('front.layouts.master')

@section('content')
    <div class="container">
        <ul class="breadcrumb">
            @include('front.common.breadcrumb')
        </ul>

        <div class="row">
            <div class="col-sm-12">
                @for ($i = 0; $i <= $details['pages']; $i++)
                    <p class="pdf-catalog-item">
                        @if ($i <= 3)
                            <img class="img-responsive" alt="" title="" src="{{ asset('catalogs/'.$directory.'/'.$i.'.jpg'.$version) }}">
                        @else
                            <img class="img-responsive unveil" alt="" title="" src="{{ asset('catalogs/'.$directory.'/bg.png') }}"  data-src="{{ asset('catalogs/'.$directory.'/'.$i.'.jpg'.$version) }}">
                        @endif
                    </p>
                @endfor
            </div>
        </div>
        <hr>
    </div>
@endsection
