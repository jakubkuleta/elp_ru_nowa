@extends('front.layouts.master')
@section('content')
<div id="news-page" class="container news newslist">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb')
    </ul>

    @foreach ($articles as $article)
    <div class="row news-article">
        <div class="col-sm-{{ $article->image ? 10 : 12 }}">
            <h1 class="rochester font-gold news-title"><a href="{{ $article->href }}" target="_blank">{{ $article->title }}</a></h1>			
            {{ $article->content }}
            <div class="date">{{ FormatHelper::date($article->created_at) }}</div>
        </div>
        @if ($article->image)
        <div class="col-sm-2">
            <img src="{{ ViewHelper::articleImage($article) }}" alt="" title="" />
        </div>
        @endif
        <hr>
    </div>
    <hr >
    @endforeach

    @if ($articles->links())
        <div style="text-align:center">
        {{ $articles->links() }}
        </div>
        <hr >
    @endif

</div>
@endsection