@extends('front.layouts.master')
@section('content')
<div id="news-page" class="container news newslist">
    <ul class="breadcrumb">
        @include('front.common.breadcrumb')
    </ul>


    <div class="row">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th scope="col">Kod</th>
                <th scope="col">Adres</th>
                <th scope="col">Wejść</th>
                <th scope="col">Ostatnie wejście</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($qrCodes as $qrCode)
        <tr>
            <th scope="row">{{ $qrCode->url_code }}</th>
            <td>{{ $qrCode->redirect_to }}</td>
            <td>{{ $qrCode->views }}</td>
            <td>{{ $qrCode->updated_at }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>

    </div>
    <hr >
</div>
@endsection