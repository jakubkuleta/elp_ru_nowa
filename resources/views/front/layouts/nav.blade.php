


<nav id="menu" class="visible">
    <button type="button" class="navbar-toggle">
        <span class="sr-only"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>

    @include('partials.nav-dropdown-menu')

    <style>
        .info {
            height: 47px;
        }
        #menu {
            width: 100%;
        }
        .main-menu {
            position: relative;
            margin-top: -1px;
            padding-top: 0;
            width: 100%;
            display: flex;
            justify-content: center;
        }

        .main-menu__item {
            padding: 0;
        }
        .main-menu__item > a {
            display: block;
            padding: 13px 25px;
        }
        .main-menu__item:hover {
            background-color: #006BB6;
        }
        .main-menu__item a:hover {
            text-decoration: none;
        }
        .main-menu__item > a:hover {
            text-decoration: none;
            color: #fff;
            background-color: #006BB6;
        }

        .has-submenu {
            position: relative;
            padding-right: 10px;
        }
        .has-submenu::after {
            content: '›';
            position: absolute;
            right: 10px;
            top: 7px;
            font-size: 18px;
            line-height: 1;
        }
        .main-menu__item.has-submenu::after {
            transform: rotate(90deg) translate(7px, 3px);
        }

        .submenu {
            position: absolute;
            padding: 0;
            top: 100%;
        }

        .submenu .has-submenu::after {
            color: black;
        }

        .submenu__item {
            position: relative;
        }
        .submenu__item a {
            padding: 8px 10px !important;
            width: max-content;
            min-width: 100%;
            max-width: 280px;
        }
        .submenu__item:hover {
            background-color: #ddd;
        }
        .submenu__item a:hover {
            background-color: #ddd;
        }

        .submenu__item .submenu {
            top: 0;
            left: calc(100% + 10px);
        }

        
        @media screen and (max-width: 1249px) {
            .main-menu__item > a {
                padding: 13px 15px;
            }
        }
        @media screen and (max-width: 991px) {
            .main-menu__item > a {
                padding: 13px 10px;
            }
            .main-menu__item.has-submenu::after {
                transform: rotate(90deg) translate(9px, 0px);
            }
        }

        /* mobile */
        .mobile-menu-showing {
        -webkit-transform: translate(-100%, 0);
        -moz-transform: translate(-100%, 0);
        -ms-transform: translate(-100%, 0);
        -o-transform: translate(-100%, 0);
        transform: translate(-100%, 0); }
        
        @media screen and (max-width: 767px) {
            .main-menu {
                display: block;
                width: 100% !important;
            }

            .submenu {
                position: inherit !important;
                left: inherit !important;
                top: inherit !important;
                width: 100% !important;
                height: auto !important;
                margin-left: 0;
            }

            .submenu__item {
                background-color: #fff !important;
            }
            .submenu__item:first-child {
                padding-left: 0 !important;
            }
            .submenu__item a {
                text-align: right;
            }
            .has-submenu::after {
                top: 11px;
            }
            .main-menu__item.has-submenu::after {
                transform: translate(0px, 6px);
            }
        }
    </style>

    <script>
        $(document).ready(function() {
            if ($(window).width() > 768) {
                $('.has-submenu').mouseenter(function() {
                    $(this).children('.submenu').show();
                });
                $('.has-submenu').mouseleave(function() {
                    $(this).children('.submenu').hide();
                });

            } else {
                $('.has-submenu').click(function(e) {
                    e.preventDefault();
                  
                    if ( $(this).data('expanded') && $(this).data('expanded') == 'true' ) {
                        if (e.target.parentElement.classList.contains('main-menu__item')) {
                            $(this).children('.submenu').slideUp();
                            $(this).data('expanded', 'false');
                        }
                    } 
                    else {
                        $(this).children('.submenu').slideDown();
                        $(this).data('expanded', 'true');
                    }
                });
            }
        });
    </script>
</nav>

<!-- MENU END -->