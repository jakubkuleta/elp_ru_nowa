<?php
$sections = View::getSections();
$showLayout = array_get($sections, 'showLayout', true);

$metaNoIndex = isset($metaNoIndex) ? $metaNoIndex : false;
?>
<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="ltr" lang="ru" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="ltr" lang="ru" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="ru">
<!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $metaTitle or '' }}</title>
    <meta name="description" content="{{ $metaDescription or '' }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @if (!$showLayout || $metaNoIndex)<meta name="robots" content="noindex">@endif
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/new-stylesheet.css?ver=29') }}" rel="stylesheet">
    <link href="https://www.euro-locks.com.ru/" hreflang="ru-ru" rel="alternate">
    <script src="{{ asset('js/jquery-2.1.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/new-functions.js?ver=4') }}"></script>

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicons/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicons/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicons/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicons/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicons/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicons/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicons/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicons/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicons/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicons/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicons/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicons/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('favicons/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">
	<meta name="_token" content="{{ csrf_token() }}">
    <meta name="yandex-verification" content="5a09f7cb0a71104f" />
    @if (!App::environment('local'))
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-4649517-5', 'auto');
    ga('send', 'pageview');
    </script>
    @endif
    <script type="application/ld+json">{"@context": "http://schema.org", "@type": "Organization", "url": "http://www.euro-locks.com.ru", "logo": "http://www.euro-locks.com.ru/images/logo.png"}</script>
</head>
<body class="common-home">

@if ($showLayout)
<div id="header-wrapper">
    <header class="container">
        <div class="logo">
            <a href="{!! UrlHelper::home() !!}"><img src="{{ asset('images/logo.png') }}" title="" alt="" class="img-responsive" /></a>
        </div>

        <div class="col-xs-12 col-sm-4 contactlinks">
            <div class="social-icons">
                <a class="icon youtube-icon" target="_blank" href="https://www.youtube.com/EuroLocksPolska"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                <a class="icon vk-icon" target="_blank" href="https://vk.com/eurolocks"><i class="fa fa-vk" aria-hidden="true"></i></a>
            </div>

            <div class="languages">
                    <a href="https://www.euro-locks.pl/">PL</a>
                    <a href="https://www.euro-locks.pl/home">EN</a>
                    <a href="https://www.euro-locks.com.ru" style="padding-right:0">RU</a>
            </div>
            <a href="mailto:{{ Config::get('content.company_address.email') }}" class="contactlink">{{ Config::get('content.company_address.email') }}</a><a href="tel:{{ Config::get('content.company_address.phone_url') }}" class="contactlink">{{ Config::get('content.company_address.phone') }}</a><br class="hidden-xs">
            <div id="search" class="input-group" data-url="{{ UrlHelper::search() }}">
                <input type="text" name="search" autocomplete="off" value="" placeholder="поиск" class="form-control input-lg">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-default btn-lg"><i class="fa fa-search"></i></button>
                </span>
                <div id="search-results">
                    <div class="heading">{{ trans('content.products') }}:<div class="searching"></div>  <div class="found pull-right">найдено <span id="prod-found">0</span></div></div>
                    <div id="result-products"></div>
                </div>
            </div>
        </div>

        @include('front.layouts.nav')
    </header>
    <div class="info"></div>
</div>
@endif

<div id="main-content-wrapper">
    @yield('content')
</div>

@if ($showLayout)
<div class="container">
    <div class="row">
        <div class="col-lg-12" style="text-align:right">
            <img src="{{ asset('images/base.png') }}" alt="" title="">
        </div>
    </div>
</div>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-3 footer-block">
                {{ Config::get('content.company_address.name') }}<br>{{ Config::get('content.company_address.street') }}<br>{{ Config::get('content.company_address.city') }}<br>{{ Config::get('content.company_address.country') }}<br/><br/>тел: {{ Config::get('content.company_address.phone') }}<br>факс: {{ Config::get('content.company_address.fax') }}<div><br /> <a href="mailto:{{ Config::get('content.company_address.email') }}">{{ Config::get('content.company_address.email') }}</a></div>
            </div>
            <div class="col-sm-3 footer-block">
                <ul class="list-unstyled">
                    <li><a href="{!! UrlHelper::locations() !!}"><strong>{{ trans('content.locations') }}</strong></a></li>
                    <li><a href="{!! UrlHelper::certificates() !!}"><strong>{{ trans('content.certificates') }}</strong></a></li>
                    <li><a href="{!! UrlHelper::terms() !!}"><strong>{{ trans('content.terms') }}</strong></a></li>
                    <?php /*<li><a href="{!! UrlHelper::sitemap() !!}"><strong>{{ trans('content.sitemap') }}</strong></a></li>*/?>
                    <li><a href="{!! UrlHelper::cookiesPolitics() !!}"><strong>{{ trans('content.cookies_politics') }}</strong></a></li>
                    <li><a href="{!! UrlHelper::termsAndConditions() !!}"><strong>{{ trans('content.terms_and_conditions') }}</strong></a></li>
                </ul>
            </div>
            <div class="col-sm-3"></div>
            <div class="col-sm-3 footer-block">
                <ul class="list-unstyled">
                    <li><a target="blank" href="http://www.loweandfletcher.com"><strong>Lowe &amp; Fletcher Group</strong></a></li>
                    <li><a target="blank" href="http://www.blegroup.co.uk/"><strong>BLE Group</strong></a></li>
                    <li><a target="blank" href="http://www.coopersfire.co.uk"><strong>Coopers Fire</strong></a></li>
                    <li><a target="blank" href="https://www.linkedin.com/company/euro-locks-inc"><strong>Euro-Locks LinkedIn</strong></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:2389320,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
@if (!isset($_COOKIE['jwCookieConsent']))
<script>
var cookieNotice = (function (options) {
    var cookieNotice;
    cookieNotice = {
        getCookie: function (cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ')
                c = c.substring(1);
                if (c.indexOf(name) == 0)
                return c.substring(name.length, c.length);
            }
            return "";
        },
        check: function () {
            var theC = cookieNotice.getCookie("jwCookieConsent");
            if (theC === "") {
            cookieNotice.append(options);
            }
        },
        append: function (options) {
            var defaultOptions = {
                style: {position: "fixed", bottom: "0", width: "100%", color: "#fff", padding: "20px", "z-index": "100", "line-height": "25px", "background-color": "#8f99a0", "opacity": "0.95"},
                text: "Сервис {{ Config::get('content.domain') }} записывает информацию в форме сооkies, использующихся в статистических и функциональных целях, а также для правильной работы сервиса. Если Вы не выражаете согласия, выключите обслуживание сооkies в настройках Вашего браузера - <a href=\"{!! UrlHelper::cookiesPolitics() !!}\" style=\"font-weight: bold; color: #fff\">читать больше&nbsp;&nbsp;</a>",
                buttonText: "Акцептирую, не показывай данное сообщение",
                buttonOpen: '<a class="btn btn-sm btn-default" id="cookieAgree">',
                buttonClose: '</a>',
                open: '<div id="cookieBar"><div class="container"><div class="col-xs-12">',
                close: '</div></div></div>'
            };

            $("body").on("click", "#cookieAgree", function () {
                document.cookie = "jwCookieConsent=true; expires=Thu, 18 Dec 2030 12:00:00 UTC; path=/";
                $(cookieFooter).hide(500);
            });

            if (typeof options === 'object') {
                options = $.extend(defaultOptions, options);
            } else {
                options = defaultOptions;
            }

            var o = options;
            var cookieFooter = $(o.open + o.text + o.buttonOpen + o.buttonText + o.buttonClose + o.close).css(o.style);
            $("body").append(cookieFooter);
            return cookieFooter;
        }
    };
    return cookieNotice;
});
$(document).ready(function () {
    cookieNotice().check();
});
</script>
<!-- Hotjar Tracking Code for www.euro-locks.pl -->

@endif
@endif
</body></html>
