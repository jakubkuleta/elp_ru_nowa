

<ul class="main-menu">
@php
    $categoriesL0 = $navbarCategoryRepository->subcategoriesByParent(0);
@endphp
@foreach($categoriesL0 as $categoryL0)
    @if($categoryL0->has_children)
        <li class="main-menu__item has-submenu">
        <a href="{{route($categoryL0->route_name, ['name' => $categoryL0->path_name, "k-".'id' => $categoryL0->category_id])}}">{{$categoryL0->name}}</a>
            <ul class="submenu">
            @php
            $categoriesL1 = $navbarCategoryRepository->subcategoriesByParent($categoryL0->category_id, 1);
            @endphp
            @foreach($categoriesL1 as $categoryL1)
                @if($categoryL1->has_children)
                    <li class="submenu__item has-submenu">
                    <a href="{{route($categoryL1->route_name, ['name' => $categoryL1->path_name, "k-".'id' => $categoryL1->category_id])}}">{{$categoryL1->name}}</a>
                        <ul class="submenu">
                        @php
                        $categoriesL2 = $navbarCategoryRepository->subcategoriesByParent($categoryL1->category_id, 2);
                        @endphp
                        @foreach($categoriesL2 as $categoryL2)
                            @if($categoryL2->has_children)
                                <li class="submenu__item has-submenu">
                                <a href="{{route($categoryL2->route_name, ['name' => $categoryL2->path_name, "k-".'id' => $categoryL2->category_id])}}">{{$categoryL2->name}}</a>
                                    <ul class="submenu">
                                    @php
                                    $categoriesL3 = $navbarCategoryRepository->subcategoriesByParent($categoryL2->category_id, 3);
                                    @endphp
                                    @foreach($categoriesL3 as $categoryL3)
                                        @if($categoryL3->has_children)
                                            <li class="submenu__item has-submenu">
                                            <a href="{{route($categoryL3->route_name, ['name' => $categoryL3->path_name, "k-".'id' => $categoryL3->category_id])}}">{{$categoryL3->name}}</a>
                                                <ul class="submenu">
                                                @php
                                                $categoriesL4 = $navbarCategoryRepository->subcategoriesByParent($categoryL3->category_id, 4);
                                                @endphp
                                                @foreach($categoriesL4 as $categoryL4)
                                                    <li class="submenu__item">
                                                    <a href="{{route($categoryL4->route_name, ['name' => $categoryL4->path_name, 'id' => $categoryL4->category_id])}}">{{$categoryL4->name}}</a>
                                                    </li>
                                                @endforeach
                                                </ul>
                                        @else
                                            <li class="submenu__item">
                                            <a href="{{route($categoryL3->route_name, ['name' => $categoryL3->path_name, 'id' => $categoryL3->category_id])}}">{{$categoryL3->name}}</a>
                                        @endif
                                        </li>
                                    @endforeach
                                    </ul>
                            @else
                                <li class="submenu__item">
                                <a href="{{route($categoryL2->route_name, ['name' => $categoryL2->path_name, 'id' => $categoryL2->category_id])}}">{{$categoryL2->name}}</a>
                            @endif
                                </li>
                        @endforeach
                        </ul>
                @else
                    <li class="submenu__item">
                    <a href="{{route($categoryL1->route_name, ['name' => $categoryL1->path_name, 'id' => $categoryL1->category_id])}}">{{$categoryL1->name}}</a>
                @endif
                    </li>
            @endforeach
            </ul>
    @else
        <li class="main-menu__item">
        <a href="{{ $categoryL0->route_name == 'products.index' ? route($categoryL0->route_name, ['name' => $categoryL0->path_name, 'id' => $categoryL0->category_id]) : route($categoryL0->route_name) }}">{{ $categoryL0->name }}</a>
    @endif
        </li>
@endforeach
</ul>
