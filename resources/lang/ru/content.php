<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'home' => 'На главную',
    'products' => 'Продукция',
    'news' => 'Новости',
    'articles' => 'Статьи',
    'contact' => 'Контакт',
    'certificates' => 'Сертификаты',
    'sitemap' => 'Карта сайта',
    'locations' => 'География деятельности',
    'enquiry' => 'Бланк запроса',
    'search_results' => 'Результаты поиска',
    'cookies_politics' => 'Политика COOKIES',
    'terms' => 'Общие условия продажи и поставок',

    'about' => 'О Нас',
    'euro_locks' => 'Euro-Locks Sp. z o. o.',
    'lowe_and_fletcher' => 'Группа компаний Lowe & Fletcher',
    'precision_manufacturing' => 'Высокоточное производство',
    'bespoke_locking_solutions' => 'Индивидуальные решения',
    'bespoke_solutions_case_studies' => 'Индивидуальные решения: Примеры',
    'our_team' => 'Наша команда',

    'brand_protection' => 'Защита марки EURO-LOCKS',
    'terms_and_conditions' => 'Условия и положения',

    'catalog' => 'каталог',
    'catalogs' => 'каталоги'
];