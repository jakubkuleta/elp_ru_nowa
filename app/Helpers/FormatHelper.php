<?php

namespace App\Helpers;

class FormatHelper
{
    public static function date($date)
    {
        $timestamp = strtotime($date);
        //$months = array('stycznia','lutego','marca','kwietnia','maja','czerwca','lipca','sierpnia','września','października','listopada','grudnia');
        //$month = date('n', $timestamp) - 1;
        return date('d.m.Y', $timestamp);
        //return date('d', $timestamp).' '.$months[$month].' '. date('Y', $timestamp);
    }

    public static function trim($value, $ifEmpty = null)
    {
        $value = trim($value);
        return ((string)$value != '') ? $value : $ifEmpty;
    }
}
