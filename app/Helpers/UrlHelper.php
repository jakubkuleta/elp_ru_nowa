<?php

namespace App\Helpers;

class UrlHelper
{
    public static function home()
    {
        return route('home');
    }

    public static function news($news = null)
    {
        if ($news) {
            return route('news.show', ['name'=>str_slug($news->title), 'id'=>$news->news_id]);
        } else {
            return route('news.index');
        }
    }

    public static function articles()
    {
        return route('articles-list');
    }

    public static function sitemap($sitemap = null)
    {
        if ($sitemap) {
            return route('sitemap-item', ['name'=>str_slug($sitemap->name), 'id'=>$sitemap->sitemap_id]);
        } else {
            return route('sitemap-list');
        }
    }

    public static function contact()
    {
        return route('contact');
    }

    public static function download($file)
    {
        return route('download', ['name'=>$file]);
    }

    public static function pdfCatalog($file)
    {
        return route('pdf-catalog', ['name'=>$file]);
    }

    public static function cookiesPolitics()
    {
        return route('cookies-politics');
    }

    public static function about()
    {
        return route('about');
    }

    public static function brandProtection()
    {
        return route('brand-protection');
    }

    public static function termsAndConditions()
    {
        return route('terms-and-conditions');
    }

    public static function ourTeam()
    {
        return route('our-team');
    }

    public static function euroLocks()
    {
        return route('euro-locks');
    }

    public static function loweAndFletcher()
    {
        return route('lowe-and-fletcher');
    }

    public static function precisionManufacturing()
    {
        return route('precision-manufacturing');
    }

    public static function bespokeLockingSolutions()
    {
        return route('bespoke-locking-solutions');
    }

    public static function bespokeSolutionsCaseStudies()
    {
        return route('bespoke-solutions-case-studies');
    }

    public static function terms()
    {
        return route('terms');
    }

    public static function keyword($keyword)
    {
        return route($keyword);
    }

    public static function catalog()
    {
        return route('catalog');
    }

    public static function locations()
    {
        return route('locations');
    }

    public static function productCategories($category)
    {
        return route('categories.index', ['name'=>str_slug($category->name), 'id'=>$category->category_id]);
    }

    public static function product($product)
    {
        $name = str_replace('/', '-', $product->name);
        $categoryId = $product->category_id;
        return route('products.show', ['category_id' => 8, 'name'=>str_slug($name), 'id'=>$product->product_id]);
    }

    public static function search()
    {
        return route('search');
    }

    public static function shop()
    {
        return route('shop');
    }

    public static function messages()
    {
        return route('messages');
    }

    public static function certificates()
    {
        return route('certificates');
    }

    public static function enquiry($id = null)
    {
        $query = ($id) ? ('?product_id='.$id) : '';
        return route('enquiry').$query;
    }
}
