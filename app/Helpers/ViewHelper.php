<?php

namespace App\Helpers;

class ViewHelper
{
    public static function articleImage($article)
    {
        return asset('images/articles/'.$article->article_id.'/'.$article->image);
    }

    public static function newsImage($news)
    {
        return asset('images/news/'.$news->news_id.'/'.$news->image);
    }

    public static function categoryImage($fileName)
    {
        return asset('images/products/'.$fileName);
    }

    public static function catalogImage($fileName)
    {
        return asset('images/catalogs/'.$fileName);
    }

    public static function productImage($product, $imageType)
    {

        $fileName = str_replace('/', '_',  $product->symbol);

        //inna nazwa pliku niz symbol
        if ($product->img_filename) {
            $fileName = $product->img_filename;
        }

        //dodakowy podkatalog
        if ($product->img_path) {
            $fileName = $product->img_path.'/'.$fileName;
        }

        switch ($imageType) {
            case 'small':
                $src = $fileName.'.jpg';
                break;

            case 'big':
                $src = $fileName.'_b.jpg';
                break;

            case 'schema':
                $src = $fileName.'_s.jpg';
                break;

            case 'list':
                $src = $fileName.'_l.jpg';
                break;
        }

        return asset('images/products/'.$src);
    }
}
