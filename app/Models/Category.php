<?php

namespace App\Models;

use DB;

class Category extends \Eloquent
{
    protected $table = 'pr_categories';
    protected $primaryKey = 'category_id';
}
