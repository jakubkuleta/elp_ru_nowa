<?php

namespace App\Models;

class Enquiry extends \Eloquent
{
    protected $table = 'ot_enquiries';
    protected $primaryKey = 'enquiry_id';

    protected $fillable = [
        'enquiry',
        'quantity',
        'company_name',
        'country',
        'city',
        'street',
        'postcode',
        'person',
        'phone',
        'email',
        'product_id',
    ];
}
