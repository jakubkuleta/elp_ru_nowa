<?php

namespace App\Models;

use DB;

class QrCode extends \Eloquent
{
    protected $table = 'ot_qr_codes';
    protected $primaryKey = 'qr_code_id';
}
