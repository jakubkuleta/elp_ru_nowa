<?php

namespace App\Models;

use DB;

class Product extends \Eloquent
{
    protected $table = 'pr_products';
    protected $primaryKey = 'product_id';

    public function productFiles()
    {
        return $this->hasMany('App\Models\ProductFile', 'product_id', 'product_id');
    }
}
