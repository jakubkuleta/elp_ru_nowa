<?php

namespace App\Models;

use DB;

class News extends \Eloquent
{
    protected $table = 'ot_news';
    protected $primaryKey = 'news_id';
}
