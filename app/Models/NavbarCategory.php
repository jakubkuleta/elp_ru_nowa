<?php

namespace App\Models;

use DB;

class NavbarCategory extends \Eloquent
{
    protected $table = 'navbar_categories';
    protected $primaryKey = 'category_id';
}
