<?php

namespace App\Models;

class Catalog extends \Eloquent
{
    protected $table = 'catalogs';
    protected $primaryKey = 'catalog_id';
}
