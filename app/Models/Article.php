<?php

namespace App\Models;

use DB;

class Article extends \Eloquent
{
    protected $table = 'ot_articles';
    protected $primaryKey = 'article_id';
}
