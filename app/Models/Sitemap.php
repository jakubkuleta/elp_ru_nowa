<?php

namespace App\Models;

use DB;

class Sitemap extends \Eloquent
{
    protected $table = 'pr_sitemap';
    protected $primaryKey = 'sitemap_id';
}
