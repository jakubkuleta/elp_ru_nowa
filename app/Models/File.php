<?php

namespace App\Models;

class File extends \Eloquent
{
    protected $table = 'ot_files';
    protected $primaryKey = 'file_id';
}
