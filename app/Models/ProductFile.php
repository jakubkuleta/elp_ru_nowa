<?php

namespace App\Models;

class ProductFile extends \Eloquent
{
    protected $table = 'pr_files';
    protected $primaryKey = 'id';

    public function file()
    {
        return $this->belongsTo('App\Models\File', 'file_id', 'file_id');
    }
}
