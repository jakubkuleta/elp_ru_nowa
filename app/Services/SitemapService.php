<?php

namespace App\Services;

use DB;
use Symfony\Component\DomCrawler\Crawler;
use Goutte\Client;
use App\Models\ProductSitemap;
use File;

//use Htmldom;

class SitemapService
{
    private $domain = 'http://ruda.euro-locks.dyndns.org/';
    private $query = 'language=ru_pl';

    public function __construct()
    {
        set_time_limit(600);
    }

    /**
     * Skanuje linki na stronie
     */
    public function scanSite()
    {
        $pages = ProductSitemap::whereRaw("skip = 0 AND visited = 0")->take(50)->get();

        foreach ($pages as $page) {
            $this->scanPage($page);
        }
    }

    private function scanPage($page)
    {
        $links = $this->getCrawler($page)->filter('a')->links();
        foreach ($links as $link) {
            $url = $link->getUri();
            $url = $this->removeDomianFromUrl($url);

            if ($this->ifSkip($url)) {
                continue;
            }

            if (!ProductSitemap::where('url', $url)->first()) {
                $catalog = new ProductSitemap();
                $catalog->url = $url;
                $catalog->save();

                echo '<br>'.$url;
            }
        }

        $page->visited = 1;
        $page->save();
    }

    /**
     * Pobiera tytul, opis, kategorie strony
     */
    public function copySite()
    {
        $pages = ProductSitemap::whereRaw("visited = 1 AND id <> 1 AND title IS NULL")->take(300)->get();
        foreach ($pages as $page) {
            $this->copyPage($page);
        }
    }

    private function copyPage($page)
    {
        $title = null;
        $type = null;
        $description = null;

        $crawler = $this->getCrawler($page);

        //typ strony
        $containers = $crawler->filter('div.container');
        foreach ($containers as  $container) {
            if ($container->getAttribute('id') == 'category-page') {
                $type = 1;
            }
            if ($container->getAttribute('id') == 'product-page') {
                $type = 2;
            }
        }

        if ($type == 1) {
            if ($crawler->filter('div.desc p')->count()) {
                $description =  $crawler->filter('div.desc p')->html();
            }

            $title = $crawler->filter('h1')->html();
        }

        if ($type == 2) {
            $title = $crawler->filter('div.header')->eq(1)->html();
        }


        $page->title = $title;
        $page->type = $type;
        $page->description = $description;

        $categories = explode('_', $page->url);
        $page->category_id1 = $categories[1];
        $page->category_id2 = isset($categories[2]) ? $categories[2] : null;
        $page->category_id3 = isset($categories[3]) ? $categories[3] : null;
        $page->category_id4 = isset($categories[4]) ? $categories[4] : null;
        $page->category_id5 = isset($categories[5]) ? $categories[5] : null;
        $page->category_id6 = isset($categories[6]) ? $categories[6] : null;

        $page->save();
    }

    /**
     * Pobiera zdjecia
     */
    public function copyImages()
    {
        $pages = ProductSitemap::whereRaw("type = 1 AND image_copied = 0")->take(10)->get();
        foreach ($pages as $page) {
            $this->copyImage($page);
        }
    }

    private function copyImage($page)
    {
        $counter = 0;
        $image = '';
        $url = '';
        $crawler = $this->getCrawler($page);
        $containers = $crawler->filter('div.category');
        foreach ($containers as  $container) {
            $image = $crawler->filter('div.category img')->eq($counter)->attr('src');
            $url =  $this->removeDomianFromUrl($crawler->filter('div.category a')->eq($counter)->attr('href'));

            $pageToUpdate = ProductSitemap::where('url', $url)->first();
            if ($pageToUpdate) {
                $copyTo = str_replace('http://ruda.euro-locks.dyndns.org/image/cache/', '', $image);

                File::copy($image, public_path().'/'.$copyTo);

                $pageToUpdate->image =  str_replace('catalog/', '', $copyTo);
                $pageToUpdate->save();
                echo '<br>ustawiono '.$image.' dla '.$url;
            }

            $counter++;
        }

        $page->image_copied = 1;
        $page->save();
    }

    /**
     * Pobiera karty produktow
     * Nie "obrabiam" html tutaj, poniewaz lepiej to zrobic w widoku
     * (tam robie zmiane na "biezaco" a tutaj musialnym od nowa kopiowac wszystkie opisy)
     */
    public function copyProductsCards()
    {
        $pages = ProductSitemap::whereRaw("type = 2 AND card_copied = 0")->take(1)->get();
        foreach ($pages as $page) {
            $this->copyProductCard($page);
        }
    }

    private function copyProductCard($page)
    {
        $crawler = $this->getCrawler($page);

        $card = null;
        $containers = $crawler->filter('div.col-sm-12');
        foreach ($containers as  $container) {
            if ($container->getAttribute('id') == 'content') {
                $card =  trim($crawler->filter('div.col-sm-12')->html());
            }
        }

        if (!$html) {
            echo '<br><strong style="color:red">brak wartosci</strong>';
        }

        $page->card_copied = 1;
        $page->card = $card;
        $page->save();
    }

    private function getCrawler($page)
    {
        $url = $page->url;
        echo '<br><strong>skanuje: '.$url.'</strong>';

        $client = new Client();
        return $client->request('GET', $this->getFullUrl($url));
    }

    private function getFullUrl($url)
    {
        $fullUrl = $this->domain.$url;

        if (substr($url, 0, 5) == 'index') {
            $fullUrl.= '&';
        } else {
            $fullUrl.= '?';
        }

        $fullUrl.= $this->query;

        return $fullUrl;
    }

    private function removeDomianFromUrl($url)
    {
        return str_replace($this->domain, '', $url);
        ;
    }

    private function ifSkip($url)
    {

        //kopiuje tylko strony "pochodne" z glownych kategorii
        if (
            strpos($url, '66_59') !== false
            || strpos($url, '66_72') !== false
            || strpos($url, '66_190') !== false
            || strpos($url, '66_73') !== false
            || strpos($url, '66_76') !== false
            || strpos($url, '66_69') !== false
            || strpos($url, '66_192') !== false
            || strpos($url, '66_75') !== false
            || strpos($url, '66_191') !== false
            || strpos($url, '66_137') !== false
            || strpos($url, '66_71') !== false
            || strpos($url, '66_67') !== false
            || strpos($url, '66_70') !== false
            || strpos($url, '66_65') !== false
            || strpos($url, '66_630') !== false
            || strpos($url, 'route=product/product/group&path=') !== false

        ) {
            return false;
        } else {
            return true;
        }

        /*
                if (!$url
                || substr($url, 0, 10) == 'index.php?'
                || substr($url, 0, 7) == 'mailto:'
                || substr($url, 0, 5) == 'file:'
                || substr($url, 0, 7) == 'http://'
                || substr($url, 0, 8) == 'https://'
                || substr($url, 0, 10) == '?language='
                || substr($url, -4) == '.jpg'
                || substr($url, -4) == '.pdf'
                || strpos($url, 'aplikacje-i-branze') !== false

                ) {
                return true;
                } else {
                    return false;
                }
        */
    }
}
