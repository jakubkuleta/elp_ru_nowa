<?php

namespace App\Services;

use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use App\Helpers\UrlHelper;

class SitemapGeneratorService
{
    public static function run()
    {
        $categories = CategoryRepository::getMainCategories();
        foreach ($categories as $category) {
            self::printItem(UrlHelper::products($category), 1);
        }

        $productRepository = new ProductRepository();
        $products = $productRepository->getItems(true, array('name'=>'%'));
        foreach ($products as $product) {
            self::printItem(UrlHelper::product($product), 0.8);
        }        
    }

    public static function printItem($url, $priority)
    {
echo '<url><loc>'.$url.'</loc><lastmod>2017-02-08</lastmod><changefreq>monthly</changefreq><priority>'.$priority.'</priority></url>
';
    }
}