<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Repositories\NavbarCategoryRepository;
use App\Helpers\UrlHelper;

class MainComposer
{
    public function __construct(NavbarCategoryRepository $navbarCategoryRepository)
    {
        $this->navbarCategoryRepository = $navbarCategoryRepository;
    }

    public function compose(View $view)
    {
        $view->with('navbarCategoryRepository',  $this->navbarCategoryRepository);

        $aboutCategories = array(
            array('url' => UrlHelper::euroLocks(), 'title' => trans('content.euro_locks'), 'image' => 'fluk.jpg'),
            array('url' => UrlHelper::ourTeam(), 'title' => trans('content.our_team'), 'image' => 'fluk.jpg'),
            array('url' => UrlHelper::loweAndFletcher(), 'title' => trans('content.lowe_and_fletcher'), 'image' => 'fluk.jpg'),
            array('url' => UrlHelper::precisionManufacturing(), 'title' => trans('content.precision_manufacturing'), 'image' => 'Precision-Manufacturing2.jpg'),
            array('url' => UrlHelper::bespokeLockingSolutions(), 'title' => trans('content.bespoke_locking_solutions'), 'image' => 'locking.jpg'),
            array('url' => UrlHelper::bespokeSolutionsCaseStudies(), 'title' => trans('content.bespoke_solutions_case_studies'), 'image' => 'fluk.jpg'),
        );

        $view->with('aboutCategories',  json_decode(json_encode($aboutCategories)));
    }
}
