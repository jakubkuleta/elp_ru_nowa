<?php

namespace App\Http\Middleware;

use Closure;

class RedirectUrl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $url = $request->url();

        if (str_contains($url, 'index.php/')) {
            $url = str_replace('index.php/', '', $url);
            return redirect($url, 301);
        }

        if (ends_with($url, '/index.php')) {
            $url = str_replace('/index.php', '', $url);
            return redirect($url, 301);
        }

        return $next($request);
    }
}
