<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::pattern('id', '[0-9]+');
Route::pattern('category_id', '[0-9]+');
Route::pattern('name', '[0-9a-zA-Z_-]+');
Route::pattern('symbol', '[0-9a-zA-Z_-]+');

/*
Route::get('/c,{name},{id}', 'RedirectController@getRedirectCategory');
Route::get('/c,{name},{id}', 'RedirectController@getRedirectCategory');
Route::get('/p,{name},{symbol}', 'RedirectController@getRedirectProduct');
Route::get('/pr,{name},{id}', 'RedirectController@getRedirectProduct2');*/

Route::get('/', 'BaseController@getHome')->name('home');

Route::get('/{name}/k-{id}', 'CategoryController@index')->name('categories.index');

Route::get('/{name}/{id}', 'ProductController@index')->name('products.index');
Route::get('/k-{category_id}/{name}/p-{id}', 'ProductController@show')->name('products.show');

Route::get('/'.str_slug(trans('content.news')), 'NewsController@index')->name('news.index');
Route::get('/'.str_slug(trans('content.news')).'/{name}/{id}', 'NewsController@show')->name('news.show');

Route::match(['get', 'post'], '/'.str_slug(trans('content.search_results')), 'ProductController@getSearch')->name('search');

Route::get('/download/{name}', 'BaseController@download')->name('download');

Route::get('/'.str_slug(trans('content.catalogs')), 'PdfCatalogController@index')->name('catalogs.index');
Route::get('/'.str_slug(trans('content.catalog')).'/{name}/{catalog_id}', 'PdfCatalogController@show')->name('catalogs.show');

Route::get('/'.str_slug(trans('content.enquiry')), 'EnquiryController@getEnquiry')->name('enquiry');
Route::post('/'.str_slug(trans('content.enquiry')), 'EnquiryController@store')->name('enquiry.store');

Route::get('/'.str_slug(trans('content.contact')), 'BaseController@getContact')->name('contact');
Route::get('/'.str_slug(trans('content.locations')), 'BaseController@getLocations')->name('locations');
Route::get('/'.str_slug(trans('content.certificates')), 'BaseController@getCertificates')->name('certificates');
Route::get('/'.str_slug(trans('content.cookies_politics')), 'BaseController@getCookiesPolitics')->name('cookies-politics');
Route::get('/'.str_slug(trans('content.terms')), 'BaseController@getTerms')->name('terms');
Route::get('/'.str_slug(trans('content.about')), 'BaseController@getAbout')->name('about');
Route::get('/'.str_slug(trans('content.euro_locks')), 'BaseController@getEuroLocks')->name('euro-locks');
Route::get('/'.str_slug(trans('content.lowe_and_fletcher')), 'BaseController@getLoweAndFletcher')->name('lowe-and-fletcher');
Route::get('/'.str_slug(trans('content.precision_manufacturing')), 'BaseController@getPrecisionManufacturing')->name('precision-manufacturing');
Route::get('/'.str_slug(trans('content.bespoke_locking_solutions')), 'BaseController@getBespokeLockingSolutions')->name('bespoke-locking-solutions');
Route::get('/'.str_slug(trans('content.bespoke_solutions_case_studies')), 'BaseController@getBespokeSolutionsCaseStudies')->name('bespoke-solutions-case-studies');
Route::get('/'.str_slug(trans('content.our_team')), 'BaseController@getOurTeam')->name('our-team');
Route::get('/'.str_slug(trans('content.brand_protection')), 'BaseController@getBrandProtection')->name('brand-protection');
Route::get('/'.str_slug(trans('content.terms_and_conditions')), 'BaseController@getTermsAndConditions')->name('terms-and-conditions');

Route::get('/qr/68ZJY6NLED2ZVGAN2TNB6AZSZ', 'QrCodeController@getList')->name('qrcode.list');
Route::get('/qr/{name}', 'QrCodeController@redirect')->name('qrcode.redirect');

Route::get('/'.str_slug(trans('content.articles')), 'ArticleController@getList')->name('articles-list');

//keywords
foreach (trans('keywords') as $keyword => $item) {
    Route::get('/'.str_slug($item['name']), 'KeywordController@getItem')->name($keyword);
}
Route::get('/{name}/s-{id}', 'SitemapController@getItem')->name('sitemap-item');
Route::get('/'.str_slug(trans('content.sitemap')), 'SitemapController@getList')->name('sitemap-list');

Route::get('/test-menu', 'BaseController@testMenu')->name('base-test');
