<?php

namespace App\Http\Controllers;

use App\Repositories\ArticleRepository;
use App\Helpers\UrlHelper;

class ArticleController extends BaseController
{
    protected $limit = 20;
    private $articleRepository;

    public function __construct(ArticleRepository $articleRepository)
    {
        parent:: __construct();
        $this->articleRepository = $articleRepository;
    }

    public function getList()
    {
        $this->setMetaTags(trans('content.articles'));
        $this->pushBreadcrumb(trans('content.articles'));
        return $this->render('articles.list', array('articles'=>$this->articleRepository->getItems($this->limit)));
    }
}