<?php

namespace App\Http\Controllers;

use App\Repositories\NewsRepository;
use App\Helpers\UrlHelper;

class NewsController extends BaseController
{
    private $newsRepository;

    public function __construct(NewsRepository $newsRepository)
    {
        parent:: __construct();
        $this->newsRepository = $newsRepository;
    }

    public function index()
    {
        $this->setMetaTags(trans('content.news'));
        $this->pushBreadcrumb(trans('content.news'));
        return $this->render('news.index', array('news'=>$this->newsRepository->getItems()));
    }

    public function show($name, $id)
    {
        $news = $this->newsRepository->getItem($id);

        $this->setMetaTags($news->title, $news->metadescription);
        $this->pushBreadcrumb(trans('content.news'), UrlHelper::news());
        $this->pushBreadcrumb($news->title, UrlHelper::news($news));

        return $this->render('news.show', array('news'=>$news));
    }
}
