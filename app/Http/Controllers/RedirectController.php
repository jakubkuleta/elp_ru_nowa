<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;

class RedirectController extends Controller
{
    public function getRedirectCategory($name = null, $id = null)
    {
        $category = Category::where('origin_category_id', $id)->first();
        if ($category) {
            return redirect()->route('products', ['name'=>str_slug($category->name), 'id'=>$category->category_id], 301);
        } else {
            abort(404);
        }
    }

    public function getRedirectProduct($name = null, $symbol = null)
    {
        $symbol = str_replace('/', '', $symbol);
        $symbol = str_replace('_', '/', $symbol);

        $product = Product::where('symbol', $symbol)->first();
        return $this->redirectToProduct($product);
    }

    public function getRedirectProduct2($name = null, $id = null)
    {
        $product = Product::where('product_id', $id)->first();
        return $this->redirectToProduct($product);
    }

    protected function redirectToProduct($product)
    {
        if ($product) {
            return redirect()->route('products.show', ['name'=>str_slug($product->name), 'id'=>$product->product_id], 301);
        } else {
            abort(404);
        }
    }
}
