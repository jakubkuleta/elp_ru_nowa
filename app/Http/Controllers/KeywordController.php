<?php

namespace App\Http\Controllers;

use Route;

class KeywordController extends BaseController
{
    public function getItem()
    {
        $keyword = Route::currentRouteName();
        $title = trans('keywords.'.$keyword.'.name');
        
        $this->setMetaTags($title, trans('keywords.'.$keyword.'.meta_description'));
        $this->pushBreadcrumb($title);
        return $this->render('keyword.item', array('keyword'=>$keyword, 'title'=>$title));
    }
}
