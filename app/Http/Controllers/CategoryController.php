<?php

namespace App\Http\Controllers;

use App\Repositories\ProductRepository;
use App\Repositories\NavbarCategoryRepository;
use Request;
use Route;
use App\Helpers\UrlHelper;

class CategoryController extends BaseController
{
    private $productRepository;
    private $navbarCategoryRepository;
    protected $breadcrumbs = array();

    public function __construct(ProductRepository $productRepository, NavbarCategoryRepository $navbarCategoryRepository)
    {
        parent:: __construct();
        $this->productRepository = $productRepository;
        $this->navbarCategoryRepository = $navbarCategoryRepository;
    }

    public function index($name = null, $category_id = null)
    {
        $viewData = array(
            'isProductList' => false,
            'categories' => $this->navbarCategoryRepository->subcategoriesByParent($category_id),
        );
        $viewFile = 'products.list';

        $this->setCategoryBreadcrumb($category_id);
        return $this->render($viewFile, $viewData);
    }

    public function show($name, $id, array $parameters = null)
    {
        $product =  $this->productRepository->getItem($id);
        $categoryId = $product->category_id;

        $viewData = array(
            'product' =>  $product,
            'categoryId' => $this->navbarCategoryRepository->categoryById($categoryId),
        );

        $this->setMetaTags($product->metatitle, $product->metadescription);
        $this->setCategoryBreadcrumb($categoryId);
        $this->pushBreadcrumb($product->name, UrlHelper::product($product));

        return $this->render('products.card', $viewData);
    }

    private function setCategoryBreadcrumb($categoryId)
    {
        $breadcrumbCategories = array_reverse($this->navbarCategoryRepository->subcategoriesByParentAllLevels($categoryId));

        foreach($breadcrumbCategories as $breadcrumbCategory)
        {
            $this->pushBreadcrumb($breadcrumbCategory['name'], $breadcrumbCategory['url']);
        }
    }
}
