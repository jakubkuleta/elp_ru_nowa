<?php

namespace App\Http\Controllers;

use Config;
use UrlHelper;
use App\Repositories\CatalogRepository;

class PdfCatalogController extends BaseController
{
    private $catalogRepository;

    public function __construct(CatalogRepository $catalogRepository)
    {
        $this->catalogRepository = $catalogRepository;
    }

    public function index()
    {
        $this->setMetaTags(trans('content.catalogs'));
        $this->pushBreadcrumb(trans('content.catalogs'));

        return $this->render('pdf-catalog.index', ['catalogs' => $this->catalogRepository->all()]);
    }

    public function show($name, $catalog_id)
    {
        $catalog = $this->catalogRepository->getById($catalog_id);
        $this->setMetaTags(trans($catalog->display_name));
        $this->pushBreadcrumb(trans('content.catalogs'), route('catalogs.index'));

        array_get(Config::get('content.downloads'), $catalog->file_name);

        $details = array_get(Config::get('content.downloads'), $catalog->file_name);

        if (!$details) {
            abort(404);
        }

        return $this->render('pdf-catalog.show', ['directory'=>$catalog->file_name, 'details'=>$details]);
    }
}