<?php

namespace App\Http\Controllers;

use Request;
use App\Helpers\UrlHelper;
use App\Repositories\NewsRepository;
use App\Repositories\PersonRepository;
use Config;
use Session;
use URL;
use Redirect;

class BaseController extends Controller
{
    protected $breadcrumbs = array();
    protected $metaTitle;
    protected $metaDescription;
    protected $metaNoIndex = false;
    protected $showBreadcrumbs = true;

    public function __construct()
    {
        if (!Session::has('referer')) {
            Session::put('referer', URL::previous());
        }
    }

    public function getHome(NewsRepository $newsRepository)
    {
        $this->setMetaTags('Евро-Локс - производитель замков, петель и запирающих устройств', 'Производитель кулачковых замков, почтовых замков, замков для корпусов, замков для шкафов, применяемых в металлических  ящиках, ящиках из искусственного материала, металлической мебели, металлических  шкафах, канцелярской мебели, почтовых ящиках, платяных шкафaх. Производитель петель, замков-ручек, замков с механизмом для тяг, аксессуаров и комплектующих для систем запирания. Поставляем уплотнитель EPDM и ПВХ, самоклеящийся и кромочный, ревизионные окна и дверцы.');
        $news =  $newsRepository->getItems(4);
        return $this->render('main.home', array('news'=>$news));
    }

    public function getContact()
    {
        $this->setMetaNoIndex();

        $this->setMetaTags(trans('content.contact'));
        $this->pushBreadcrumb(trans('content.contact'));
        return $this->render('main.contact', array(
            'people' => PersonRepository::getItems(),
        ));        
    }

    public function getCookiesPolitics()
    {
        $this->setMetaTags(trans('content.cookies_politics'));
        $this->pushBreadcrumb(trans('content.cookies_politics'));
        return $this->render('main.cookies-politics');
    }

    public function getLocations()
    {
        $this->setMetaTags(trans('content.locations'));
        $this->pushBreadcrumb(trans('content.locations'));
        return $this->render('main.locations');
    }

    public function getCertificates()
    {
        $this->setMetaTags(trans('content.certificates'));
        $this->pushBreadcrumb(trans('content.certificates'));
        return $this->render('main.certificates');
    }

    public function getTerms()
    {
        $this->setMetaTags(trans('content.terms'));
        $this->pushBreadcrumb(trans('content.terms'));
        return $this->render('main.terms');
    }

    public function getAbout()
    {
        $this->setMetaTags(trans('content.about'), 'Фирма Euro-Locks Sp. z o. o. является частью группы компаний Lowe & Fletcher, занимающейся проектированием, производством и поставкой замков клиентам...');
        $this->pushBreadcrumb(trans('content.about'));
        return $this->render('main.about');
    }

    public function getEuroLocks()
    {
        $this->setMetaTags(trans('content.euro_locks'), 'Будучи фирмой, специализирующейся в производстве запирающих систем, Euro-Locks Sp. z o. o. располагает широким ассортиментом замков для промышленности...');
        $this->pushBreadcrumb(trans('content.about'), UrlHelper::about());
        $this->pushBreadcrumb(trans('content.euro_locks'));
        return $this->render('main.euro-locks');
    }
    
    public function getLoweAndFletcher()
    {
        $this->setMetaTags(trans('content.lowe_and_fletcher'), 'Компания Lowe & Fletcher была основана в 1889 г. Именно тогда Джон Лау и Томас Флетчер решили заняться производством замков.');
        $this->pushBreadcrumb(trans('content.about'), UrlHelper::about());
        $this->pushBreadcrumb(trans('content.lowe_and_fletcher'));
        return $this->render('main.lowe-and-fletcher');
    }

    public function getPrecisionManufacturing()
    {
        $this->setMetaTags(trans('content.precision_manufacturing'), 'Grupa Lowe & Fletcher производит замки с 1889 г. Более 850 сотрудников, как локально, так и на международном уровне...');
        $this->pushBreadcrumb(trans('content.about'), UrlHelper::about());
        $this->pushBreadcrumb(trans('content.precision_manufacturing'));
        return $this->render('main.precision-manufacturing');
    }

    public function getBespokeLockingSolutions()
    {
        $this->setMetaTags(trans('content.bespoke_locking_solutions'), 'Команда Отдела Продаж Euro-Locks тесно сотрудничает со всеми клиентами, с целью поставки замков и запирающих систем, соответствующих требуемым параметрам.');
        $this->pushBreadcrumb(trans('content.about'), UrlHelper::about());
        $this->pushBreadcrumb(trans('content.bespoke_locking_solutions'));
        return $this->render('main.bespoke-locking-solutions');
    }

    public function getBespokeSolutionsCaseStudies()
    {
        $this->setMetaTags(trans('content.bespoke_solutions_case_studies'), 'Разработка индивидуальных решений является очень важной частью деятельности компании Euro-Locks. Усилия направленные на удовлетворение разносторонних...');
        $this->pushBreadcrumb(trans('content.about'), UrlHelper::about());
        $this->pushBreadcrumb(trans('content.bespoke_solutions_case_studies'));
        return $this->render('main.bespoke-solutions-case-studies');
    }

    public function getOurTeam()
    {
        $this->setMetaNoIndex();

        $this->setMetaTags(trans('content.our_team'),'');
        $this->pushBreadcrumb(trans('content.about'), UrlHelper::about());
        $this->pushBreadcrumb(trans('content.our_team'));
        return $this->render('main.our-team', array(
            'people' => PersonRepository::getItems(),
        ));
    }

    public function getBrandProtection()
    {
        $this->setMetaTags(trans('content.brand_protection'), '');
        $this->pushBreadcrumb(trans('content.brand_protection'), UrlHelper::brandProtection());
        return $this->render('main.brand-protection');
    }

    public function getTermsAndConditions()
    {
        $this->setMetaTags(trans('content.terms_and_conditions'), '');
        $this->pushBreadcrumb(trans('content.terms_and_conditions'), UrlHelper::termsAndConditions());
        return $this->render('main.terms-and-conditions');
    }

    public function download($name)
    {
        $details = array_get(Config::get('content.downloads'), $name);
        if (!$details) {
            abort(404);
        }

        $file = public_path(). '/download/'.$name.'.pdf';
        return response()->download($file, $details['name'].'.pdf');
    }

    public function testMenu()
    {
        return $this->render('main.test-menu');
    }

    protected function render($view, array $viewData = null)
    {
        return view('front.'.$view, (array)$viewData)
        ->with('breadcrumbs', $this->breadcrumbs)
        ->with('metaTitle', $this->metaTitle)
        ->with('metaDescription', $this->metaDescription)
        ->with('metaNoIndex', $this->metaNoIndex);
    }

    protected function setMetaTags($title, $desctiption = '')
    {
        $this->metaTitle = $title;
        $this->metaDescription = $desctiption;
    }

    protected function setMetaNoIndex()
    {
        $this->metaNoIndex = true;
    }

    protected function pushBreadcrumb($name, $url = null)
    {
        if (!$url) {
            $url = route(Request::route()->getName());
        }
        if($this->showBreadcrumbs) {
            $this->breadcrumbs[] = (object)array('name' => $name, 'url' => $url);
        }
    }
}
