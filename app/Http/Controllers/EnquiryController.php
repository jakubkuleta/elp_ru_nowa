<?php

namespace App\Http\Controllers;

use Mail;
use Validator;
use Request;
use Config;
use App\Models\Enquiry;
use App\Repositories\ProductRepository;
use Cookie;
use App;
use Illuminate\Support\Facades\Log;

class EnquiryController extends BaseController
{
    public function getEnquiry()
    {
        $productId = (int)Request::get('product_id');
        $this->setMetaTags(trans('content.enquiry'));
        $this->pushBreadcrumb(trans('content.enquiry'));

        $enquiry = null;
        if ($enquiryId = Cookie::get('enquiry')) {
            $enquiry = Enquiry::find($enquiryId);
        }

        return $this->render('enquiry.enquiry', array('productId'=>$productId, 'enquiry'=>$enquiry));
    }

    public function store()
    {
        $validator = Validator::make(Request::all(), [
            'enquiry' => 'required',
            'quantity' => 'required',
            'company_name' => 'required',
            'country' => 'required',
            'city' => 'required',
            'street' => 'required',
            'postcode' => 'required',
            'person' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
        ]);

        $validator->setAttributeNames(trans('enquiry.inputs'));

        if ($validator->fails()) {
            return response()->json(['status'=>false, 'message'=>$this->getValidatorErrors($validator->messages())]);
        }

        /*try {*/

            //save enquiry
            $enquiry = Enquiry::create(Request::all());

            //set enquiry_id to Cookie
            Cookie::queue(Cookie::forever('enquiry', $enquiry->enquiry_id));

            //mail to company
            $this->sendMail(true);

            //mail to cutomer
            if (Request::get('send_to_me')) {
                $this->sendMail(false);
            }

            return response()->json(['status'=>true, 'message'=>trans('enquiry.messages.success')]);
        /*} catch (\Exception $e) {
            Log::critical($e->getMessage());
            return response()->json(['status'=>false, 'message'=>trans('enquiry.messages.error')]);
        }*/
    }

    protected function sendMail($emailToCompany)
    {
        $subject = Config::get('enquiry.subject');

        if ($emailToCompany) {
            $subject = $subject.': '.Request::get('company_name');
            $to = App::environment('local') ? Config::get('enquiry.send_to_local') : Config::get('enquiry.send_to');
        } else {
            $to = Request::get('email');
        }
        
        $viewData = $this->getViewData();
        $viewData['emailToCompany'] = $emailToCompany;

        $response = Mail::send('emails.enquiry', $viewData, function ($message) use ($subject, $to) {
            $message
            ->to($to)
            ->subject($subject);
        });
    }

    protected function getViewData()
    {
        $productId = (int)Request::get('product_id');

        $data = array();
        $data['product'] = ($productId) ? (new ProductRepository())->getItem($productId) : null;
        return $data;
    }

    protected function getValidatorErrors($error)
    {
        $errors = array();
        foreach ($error->toArray() as $column => $message) {
            $errors[] = $message[0];
        }

        return implode('<br>', $errors);
    }
}
