<?php

namespace App\Http\Controllers;

use App\Repositories\SitemapRepository;
use App\Helpers\UrlHelper;

class SitemapController extends BaseController
{
    private $sitemapRepository;

    public function __construct(SitemapRepository $sitemapRepository)
    {
        parent:: __construct();
        $this->sitemapRepository = $sitemapRepository;
    }

    public function getList()
    {
        $this->setMetaTags(trans('content.sitemap'));
        $this->pushBreadcrumb(trans('content.sitemap'));
        return $this->render('sitemap.list', array('sitemap'=>$this->sitemapRepository->getItems()));
    }

    public function getItem($name, $id)
    {
        $sitemap = $this->sitemapRepository->getItem($id);
        $products = $this->sitemapRepository->getRelatedProducts($id);

        $this->setMetaTags($sitemap->name);
        $this->pushBreadcrumb(trans('content.sitemap'), UrlHelper::sitemap());
        $this->pushBreadcrumb($sitemap->name, UrlHelper::sitemap($sitemap));

        return $this->render('sitemap.item', array('sitemap'=>$sitemap, 'products'=>$products));
    }
}
