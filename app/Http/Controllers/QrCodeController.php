<?php

namespace App\Http\Controllers;

use App\Models\QrCode;

class QrCodeController extends BaseController
{
    public function __construct(QrCode $qrCode)
    {
        parent:: __construct();
        $this->qrCode = $qrCode;
    }

    public function getList()
    {
        $this->setMetaTags('QR kody');
        $this->pushBreadcrumb('QR kody');
        return $this->render('qrcode.list', array('qrCodes'=>$this->qrCode->all()));
    }

    public function redirect($code)
    {
        $qrCode = $this->qrCode->where('url_code', '=', $code)->firstOrFail();
        $qrCode->views++;
        $qrCode->save();

        return redirect($qrCode->redirect_to);
    }

}