<?php

namespace App\Http\Controllers;

use App\Repositories\ProductRepository;
use App\Repositories\NavbarCategoryRepository;
use Illuminate\Support\Str;
use Request;
use Route;
use App\Helpers\UrlHelper;
use App\Helpers\ViewHelper;

class ProductController extends BaseController
{
    private $productRepository;
    private $navbarCategoryRepository;
    protected $breadcrumbs = array();

    public function __construct(ProductRepository $productRepository, NavbarCategoryRepository $navbarCategoryRepository)
    {
        parent:: __construct();
        $this->productRepository = $productRepository;
        $this->navbarCategoryRepository = $navbarCategoryRepository;
    }

    public function index($name = null, $category_id = null, array $parameters = null)
    {
        $categoryId = $category_id;
        $viewData = array(
            'isProductList' => true,
            'products' => $this->productRepository->productsByCategory($category_id),
            'category_id' => $categoryId,
        );
        $viewFile = 'products.list';

        $this->setCategoryBreadcrumb($categoryId);
        return $this->render($viewFile, $viewData);
    }

    public function getSearch()
    {
        $viewData = array();
        $products = array();

        $name = Request::input('name');

        if ($name) {
            $products = $this->productRepository->searchResults(trim($name));
        } else {
            $viewData['noCriteria'] = true;
        }

        $viewData['products'] = $products;

        foreach ($products as $product) {
            $product->category_id = $this->productRepository->productCategoryOne($product->product_id);
        }

        $jsonData = array();
            foreach ($products as $product) {

                $jsonData[] = array(
                    'href' => UrlHelper::product($product),
                    'image' => ViewHelper::productImage($product, 'small'),
                    'name' => $product->name,
                );
            }

            return response()->json(
                array(
                    'products' => array(
                        'count' => count($jsonData),
                        'items' => $jsonData,
                    ),
                )
            );
    }

    public function show($category_id, $name, $id)
    {
        if(request()->input('showBreadcrumbs'))
        {
            $this->showBreadcrumbs = false;
        }


        $product =  $this->productRepository->getItem($id);

        $categoryId = $category_id;
                $viewData = array(
                    'product' =>  $product,
                    'categoryId' => $this->navbarCategoryRepository->categoryById($categoryId),
                );
        $this->setMetaTags($product->metatitle, $product->metadescription);
        $this->setCategoryBreadcrumb($categoryId);

        return $this->render('products.card', $viewData);
    }

    private function setCategoryBreadcrumb($categoryId)
    {
        $breadcrumbCategories = array_reverse($this->navbarCategoryRepository->subcategoriesByParentAllLevels($categoryId));

        foreach($breadcrumbCategories as $breadcrumbCategory)
        {
            $this->pushBreadcrumb($breadcrumbCategory['name'], $breadcrumbCategory['url']);
        }
    }
}
