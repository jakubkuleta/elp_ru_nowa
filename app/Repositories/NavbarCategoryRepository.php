<?php

namespace App\Repositories;

use App\Models\NavbarCategory;
use DB;

class NavbarCategoryRepository
{
    private $model;
    private $parentOfCurrent;

    public function __construct()
    {
        $this->model = NavbarCategory::all();
    }

    public function categoryById($categoryId)
    {
        return $this->model->find($categoryId);
    }

    public function subcategoriesByParent($parent_category_id)
    {
        return NavbarCategory::where('parent_category_id', $parent_category_id)->orderBy('sort')->get();
    }

    public function subcategoriesByParentAllLevels($parent_category_id)
    {
        $this->parentOfCurrent = $this->model->find($parent_category_id);
        $depthLevel = $this->parentOfCurrent->depth_level;

        $parentCategoryName[] = ['name' => $this->parentOfCurrent->name,
                                 'url' => $this->urlByCategoryId($this->parentOfCurrent->category_id)];

        while($depthLevel > 0)
        {
            $parentOfParentCategory = $this->model->find($this->parentOfCurrent->parent_category_id);
            $parentCategoryName[] = ['name' => $parentOfParentCategory->name,
                                     'url' => $this->urlByCategoryId($parentOfParentCategory->category_id)];

            $this->parentOfCurrent = $this->model->find($parentOfParentCategory->category_id);
            $depthLevel = $depthLevel - 1;
        }

    return $parentCategoryName;
    }

    public function urlByCategoryId($category_id)
    {
        $url = $this->model->find($category_id);

        return route($url->route_name, [$url->path_name, $category_id]);
    }
}
