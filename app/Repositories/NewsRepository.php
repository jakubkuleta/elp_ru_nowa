<?php

namespace App\Repositories;

use DB;
use App\Models\News;

class NewsRepository
{
    public function getItems($limit = null, $page = null)
    {
        //return DB::select('SELECT news_id, href, title, intro, image, created_at FROM ot_news WHERE hide = 0 AND COALESCE(publish_from, NOW()) <= NOW() ORDER BY created_at DESC, news_id DESC'.self::getLimitSql($limit, $page));
        return News::selectRaw('news_id, href, title, intro, image, created_at')->whereRaw('hide = 0 AND COALESCE(publish_from, NOW()) <= NOW()')->orderByRaw('created_at DESC, news_id DESC')->paginate($limit);
    }

    public function getItem($id)
    {
        return News::whereRaw('hide = 0 AND COALESCE(publish_from, NOW()) <= NOW()')->findOrFail($id);
    }
}
