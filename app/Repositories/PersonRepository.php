<?php

namespace App\Repositories;

class PersonRepository
{
    public static function getItems()
    {
        return array(
            array(
                'name' => 'Януш Кулета',
                'img' => 'Janusz-Kuleta.jpg',
                'position' => 'Директор EURO-LOCKS Польша',
                'mobile' => '+48 602-718-652',
                'email' => 'jkuleta@euro-locks.pl',
                'orderInContact' => 60,
            ),
           array(
                'name' => 'Пётр Гульски',
                'img' => 'Piotr-Gulski.jpg',
                'position' => 'Начальник отдела продаж',
                'mobile' => '+48 602-609-104',
                'email' => 'piotrgulski@euro-locks.pl',
                'orderInContact' => 50,
            ),
            array(
                'name' => 'Александр Дульский-Гребенщиков',
                'img' => 'Aleksander-Dulski.jpg',
                'position' => 'Начальник отдела экспорта',
                'mobile' => '+48 882-489-151',
                'email' => 'aleksanderdulski@euro-locks.pl',
                'area' => 'Россия (кроме областей принадлежащих А. Карпову), страны СНГ.',
                'orderInContact' => 10,
            ),
            array(
                'name' => 'Лукаш Мировски',
                'img' => 'Lukasz-Mirowski.jpg',
                'position' => 'Руководитель направления фурнитуры для энергетики',
                'mobile' => '+48 538-816-116',
                'email' => 'lukaszmirowski@euro-locks.pl',
				'orderInContact' => 45,
            ),
            array(
                'name' => 'Станислав Боровски',
                'img' => 'Stanislaw-Borowski.jpg',
                'position' => 'Торговый представитель',
                'mobile' => '+48 662-092-065',
                'email' => 'stanislawborowski@euro-locks.pl',
            ),
            array(
                'name' => 'Магдалена Зелёнка',
                'img' => 'Magdalenia-Zielonka.jpg',
                'position' => 'Руководитель Отдела обслуживания клиентов',
                'phone' => '+48 32-344-78-82',
                'email' => 'magdalenazielonka@euro-locks.pl',
            ),
            array(
                'name' => 'Анатолий Карпов',
                'img' => 'Anatoly-Karpov.jpg',
                'position' => 'Технико-коммерческий представитель',
                'mobile' => '+7 (965) 194-30-99',
                'phone' => '+7 499-130-96-33 Доб. 200',
                'phoneLink' => '+74991309633',
                'email' => 'anatolykarpov@euro-locks.com.ru',
                'area' => 'Московская, Тверская, Владимирская, Рязанская, Тульская, Калужская, Смоленская, Ярославская, Ивановская области.',
                'orderInContact' => 20,
            ),
            array(
                'name' => 'Таиса Людвиковска',
                'img' => 'Taisa-Ludwikowska.jpg',
                'position' => 'Специалист отдела экспорта',
                'mobile' => '+48 883-375-458',
                'email' => 'taisaludwikowska@euro-locks.pl',
                'orderInContact' => 30,
            ),
            array(
                'name' => 'Екатерина Павленко',
                'img' => 'Kataryna-Pavlenko.jpg',
                'position' => 'Специалист отдела экспорта',
                'mobile' => '+48 660-569-140',
                'email' => 'katerynapavlenko@euro-locks.pl',
                'orderInContact' => 40,
            ),
            array(
                'name' => 'Агнешка Левандовска-Ракочи',
                'img' => 'Agnieszka-Lewandowska.jpg',
                'position' => 'Младший специалист отдела экспорта',
                'phone' => '+48 32 706 90 45',
                'email' => 'agnieszkalewandowska@euro-locks.pl',
                'orderInContact' => 39,
            ),
        );
    }
}
