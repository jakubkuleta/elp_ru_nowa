<?php

namespace App\Repositories;

use DB;
use Cache;
use App\Models\Catalog;


class CatalogRepository
{
    public function all()
    {
        return Catalog::all();
    }

    public function getById($catalog_id)
    {
        return Catalog::where('catalog_id', $catalog_id)->first();
    }
}
