<?php

namespace App\Repositories;

use DB;
use App\Models\Product;

class ProductRepository
{
    public function productsByCategory($category_id)
    {
        return DB::select("SELECT *
                    FROM pr_products a
                    INNER JOIN navbar_categories_products b on a.product_id = b.product_id
                    WHERE b.category_id = $category_id AND a.hide = 0
                    ORDER BY sort");
    }

    public function searchResults(string $name = null)
    {
        $where = array('hide = 0');
        $bindings = array();
        $sort = 'sort, CASE WHEN type = 2 THEN name ELSE symbol END';

        $where[] = ' (name LIKE :name OR keywords LIKE :keywords)';
        $bindings['name'] = '%'.$name.'%';
        $bindings['keywords'] = '%'.$name.'%';

        return DB::select('SELECT a.product_id, a.name, a.symbol, a.href, a.img_path, a.img_filename 
                           FROM pr_products a
                           WHERE '.join(' AND ', $where).' 
                           ORDER BY coalesce(sort, 9999)', $bindings);
    }

    public function getItemsByProductId(array $productId)
    {
        return $this->getItems(true, ['product_id'=>$productId]);
    }

    public function getItem($id)
    {
        return Product::whereRaw('hide = 0')->findOrFail($id);
    }

    public function productCategoryOne($product_id)
    {
        return DB::table('navbar_categories_products')->limit(1)->where('product_id', $product_id)->value('category_id');
    }
}
