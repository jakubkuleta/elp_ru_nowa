<?php

namespace App\Repositories;

use DB;
use App\Models\Sitemap;

class SitemapRepository
{
    public function getItems()
    {
        return DB::select('SELECT sitemap_id, group_id, name, href FROM
        pr_sitemap
        WHERE hide = 0 AND
        sitemap_id IN (
            SELECT sitemap_id FROM pr_sitemap_products WHERE product_id IN (
                SELECT product_id FROM pr_products WHERE hide = 0)
            )
        ORDER BY group_id, name, sitemap_id');
    }

    public function getItem($id)
    {
        return Sitemap::whereRaw('hide = 0')->findOrFail($id);
    }

    public function getRelatedProducts($id)
    {
        return DB::select('SELECT product_id, name, symbol, href, img_path, img_filename
        FROM pr_products
        WHERE hide = 0 AND product_id IN (SELECT product_id FROM pr_sitemap_products WHERE sitemap_id = '.$id.') ORDER BY sort, CASE WHEN type = 2 THEN name ELSE symbol END');
    }
}
