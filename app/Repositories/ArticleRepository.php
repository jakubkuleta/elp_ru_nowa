<?php

namespace App\Repositories;

use App\Models\Article;

class ArticleRepository
{
    public function getItems($limit = null, $page = null)
    {
        return Article::selectRaw('article_id, href, title, content, image, created_at')->whereRaw('hide = 0 AND COALESCE(publish_from, NOW()) <= NOW()')->orderByRaw('created_at DESC, article_id DESC')->paginate($limit);
    }
}
