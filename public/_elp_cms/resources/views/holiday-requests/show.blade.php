@extends('show', [
    'here' => 'Wniosek urlopowy ID: '.$holidayRequest->id,
])
@section('box-body')
<table class="table">
<tr><td style="width:150px">ID wniosku:</td><td>{{ $holidayRequest->id }}</td></tr>
<tr><td>czas utworzenia:</td><td>{{ $holidayRequest->czas_utworzenia }}</td></tr>
<tr><td>pracownik:</td><td>{{ $holidayRequest->employee->name }}</td></tr>
<tr><td>rodzaj urlopu:</td><td>{{ $holidayRequest->absenceType->name }}</td></tr>
<tr><td>od:</td><td>{{ $holidayRequest->urlop_od }}</td></tr>
<tr><td>do:</td><td>{{ $holidayRequest->urlop_do }}</td></tr>
<tr><td>dni roboczych:</td><td>{{ $holidayRequest->dni_roboczych }}</td></tr>
<tr><td>zastępstwo:</td><td>{{ $holidayRequest->zastepstwo }}</td></tr>
<tr><td>uwagi pracownika:</td><td>{{ $holidayRequest->uwagi_pracownika }}</td></tr>
<tr><td>przełożony:</td><td>{{ $holidayRequest->supervisor->name }}</td></tr>
</table>

<hr>
<table class="table">
    <tr><td style="width:150px;">status wniosku:</td><td style="background-color:{{ $holidayRequest->present()->statusColor() }}"><strong>{{ $holidayRequest->present()->statusName() }}</strong></td></tr>
@if ($holidayRequest->id_status != $holidayRequest::STATUS_PENDING)
    <tr><td>czas {{ $holidayRequest->id_status == $holidayRequest::STATUS_ACCEPTED ? 'akceptacji' : 'odrzucenia' }}:</td><td>{{ $holidayRequest->czas_zmiany_statusu }}</td></tr>
    <tr><td>uwagi przełożonego:</td><td>{{ $holidayRequest->uwagi_przelozonego }}</td></tr>
@endif
</table>

@endsection

@if ($holidayRequest->id_status == $holidayRequest::STATUS_PENDING && $holidayRequest->id_prac_przelozony == Auth::user()->present()->employeeId())
    @section('box-footer')
    <div class="form-group">
        <label for="id-supervisor_remarks">uwagi przełożonego:</label>
        <div>
            {{ Form::text('', null, ['class' => 'form-control', 'maxlength' => 200, 'id' => 'supervisor-remarks']) }}
        </div>
    </div>

    <button type="submit" class="btn btn-success pull-right" id="accept" style="margin-left:5px">Zaakceptuj</button>
    <button type="submit" class="btn btn-danger pull-left" id="reject">Odrzuć</button>

    {{ Form::open(['method' => 'POST', 'id' => 'form-accept', 'class' => 'xhide', 'url' => route('holiday-requests.accept', $holidayRequest->id)]) }}
    {{ Form::hidden('uwagi_przelozonego', null, ['id' => 'supervisor-remarks-accept']) }}
    {{ Form::close() }}

    {{ Form::open(['method' => 'POST', 'id' => 'form-reject', 'class' => 'xhide', 'url' => route('holiday-requests.reject', $holidayRequest->id)]) }}
    {{ Form::hidden('uwagi_przelozonego', null, ['id' => 'supervisor-remarks-reject']) }}
    {{ Form::close() }}
    @endsection

    @section('js')
    $('#accept').click( function(e) {
        if (confirm('Czy na pewno zaakceptować wniosek ?')) {
            EA.loader.show();
            $('#supervisor-remarks-accept').val($('#supervisor-remarks').val());
            $('#form-accept').submit();
        }
    });

    $('#reject').click( function(e) {
        if (confirm('Czy na pewno odrzucić wniosek ?')) {
            EA.loader.show();
            $('#supervisor-remarks-reject').val($('#supervisor-remarks').val());
            $('#form-reject').submit();
        }
    });
    @endsection
@endif