@extends('index', ['rightBoxWidth' => 1593])
@section('table-body')
    @php
    $table = new TableHelper('list-caption');
    $table->addThSortable('wh50', 'ID wnio-<br>sku', 'id');
    $table->addThSortable('wh130', 'czas utworzenia', 'czas_utworzenia');
    $table->addThSortable('wh150', '<strong>pracownik</strong>', 'pracownik');
    $table->addThSortable('wh150', 'rodzaj urlopu', 'rodzaj_urlopu');
    $table->addThSortable('wh80', 'urlop od', 'urlop_od');
    $table->addThSortable('wh80', 'urlop do', 'urlop_do');
    $table->addThSortable('wh40', 'dni robo-<br>czych', 'dni_roboczych');
    $table->addThSortable('wh130', 'zastępstwo', 'zastepstwo');
    $table->addThSortable('wh200', 'uwagi pracownika', 'uwagi_pracownika');
    $table->addThSortable('wh150', 'przełożony', 'przelozony');
    $table->addThSortable('wh110', 'status<br>wniosku', 'id_status');
    $table->addThSortable('wh130', 'czas akcpetacji/<br>odrzucenia', 'czas_zmiany_statusu');
    $table->addThSortable('wh200', 'uwagi przełożonego', 'uwagi_przelozonego');
    echo $table->render();
    @endphp

    @foreach ($holidayRequests as $holidayRequest)
    <tr>
        <td class="alc">{{ $holidayRequest->id }}</td>
        <td class="alc">{{ $holidayRequest->czas_utworzenia }}</td>
        <td class="wh150"><a href="{{ route('holiday-requests.show', ['id' => $holidayRequest->id]) }}">{{ $holidayRequest->pracownik }}</a></td>
        <td class="wh150">{{ $holidayRequest->rodzaj_urlopu }}</td>
        <td class="alc">{{ $holidayRequest->urlop_od }}</td>
        <td class="alc">{{ $holidayRequest->urlop_do }}</td>
        <td class="alc">{{ $holidayRequest->dni_roboczych }}</td>
        <td class="wh130">{{ $holidayRequest->zastepstwo }}</td>
        <td class="wh200">{{ $holidayRequest->uwagi_pracownika }}</td>
        <td class="wh150">{{ $holidayRequest->przelozony }}</td>
        <td style="background-color:{{ Arr::get(Config::get('system.holiday_requests.statusColors'), $holidayRequest->id_status) }}">{{ Arr::get(Config::get('system.holiday_requests.statusNames'), $holidayRequest->id_status) }}</td>
        <td class="alc">{{ $holidayRequest->czas_zmiany_statusu }}</td>
        <td class="wh200">{{ $holidayRequest->uwagi_przelozonego }}</td>
    @endforeach
@endsection