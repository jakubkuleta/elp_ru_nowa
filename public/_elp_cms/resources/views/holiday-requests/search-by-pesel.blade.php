@php
$pesel = request()->get('pesel');
$alerts = [];

$settings = [
    'showHeader' => false,
    'here' => 'Dodaj wniosek urlopowy',
    'formUrl' => route('holiday-requests.create-by-pesel'),
];
@endphp

@extends('show', $settings)
@section('box-body')
    @php
    if (request()->has('pesel') && !$pesel) {
        $alerts[] = 'Pole [PESEL] jest wymagane.';
    }

    if ($pesel) {
        $alerts[] = 'Nie odnaleziono pracownika na podstawie podanego [PESEL].';
    }
    @endphp

    {!! FormHelper::labelAndText('PESEL*', 'pesel', ['autofocus' => true]) !!}
@endsection

@section('box-footer')
    <button type="submit" class="btn btn-primary pull-right">Dalej</button>
@endsection

@section('alert')
    @if ($alerts)
        <div class="alert alert-danger">{{ join('<br>', $alerts) }}</div>
    @endif
@endsection