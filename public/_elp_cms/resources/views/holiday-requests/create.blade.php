@extends('edit', [
    'showHeader' => $byPesel ? false : true,
    'cancelButtonUrl' => $byPesel ? route('holiday-requests.create-by-pesel') : null,

])
@section('box-body')

@if ($byPesel)
<div class="form-group">
    <label>pracownik:</label><div style="font-size:150%"><strong>{{ $employee->name }}</strong></div>
</div><br>
@endif

<div class="form-group">
    <label>przełożony:</label><div>{{ $employee->supervisor ? $employee->supervisor->name : '' }}</div>
</div>

<hr>
<div class="form-group">
        <label><strong>urlop wypoczynowy:</strong></label>
    <table class="table table-hover table-bordered">
        <tr class="tr-caption">
            <th colspan="5" style="height:auto">DNI</th>
        </tr>
        <tr class="tr-caption">
            <th style="width:20%">wymiar<br>urlopu</th>
            <th style="width:20%">pozostało<br>z roku {{ date('Y') - 1 }}<br></th>
            <th style="width:20%">wykorzystano</th>
            <th style="width:20%">pozostało do<br>wykorzystania<br>z roku {{ date('Y') - 1 }}</th>
            <th style="width:20%">pozostało do<br>wykorzystania razem</th>
        </tr>
        <tr>
            <td class="alc">{{ $employee->present()->daysOfHoliday() }}</td>
            <td class="alc">{{ $employee->present()->daysOfHolidayOutstanding() }}</td>
            <td class="alc">{{ $employee->present()->daysOfHolidayUsed() }}</td>
            <td class="alc">{{ $employee->present()->daysOfHolidayToUseOutstanding() }}</td>
            <td class="alc">{{ $employee->present()->daysOfHolidayToUse() }}</td>
        </tr>
    </table>
</div>

<hr>
{!! FormHelper::labelAndSelect('rodzaj urlopu*', 'id_rodzaj_nieobecnosci', $absenceTypes) !!}
{!! FormHelper::labelAndText('urlop od*', 'urlop_od', ['maxlength' => 10, 'class' => 'datepicker']) !!}
{!! FormHelper::labelAndText('urlop do*', 'urlop_do', ['maxlength' => 10, 'class' => 'datepicker']) !!}

@if ($byPesel)
{!! FormHelper::labelAndText('telefon*', 'telefon', ['maxlength' => 9]) !!}
@endif

@if ($employee->present()->isReplacementRequired())
    {!! FormHelper::labelAndText('zastępstwo*', 'zastepstwo', ['maxlength' => 50]) !!}
@endif
{!! FormHelper::labelAndText('uwagi', 'uwagi_pracownika', ['maxlength' => 200]) !!}

@if ($byPesel)
{{ Form::hidden('pesel', $employee->pesel) }}
@endif

@endsection