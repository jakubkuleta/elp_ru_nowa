@component('mail::message')
{{-- Greeting --}}

@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level == 'error')
# Whoops!
@else
# Witaj!
@endif
@endif


{{-- Intro Lines --}}
@foreach ($introLines as $line)
{!! $line !!}

@endforeach

{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
            $color = 'green';
            break;
        case 'error':
            $color = 'red';
            break;
        default:
            $color = 'blue';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}
@endforeach

{{-- Salutation --}}
{{--
@if (! empty($salutation))
{{ $salutation }}
@else
Pozdrawiamy,<br>{{ config('app.name') }}
@endif
--}}

{{-- Subcopy --}}
@isset($actionText)
@component('mail::subcopy')
Jeśli masz problem z kliknięciem przycisku "{{ $actionText }}", skopiuj i wklej poniższy adres URL
do swojej przeglądarki internetowej: [{{ $actionUrl }}]({{ $actionUrl }})
@endcomponent
@endisset
@endcomponent
