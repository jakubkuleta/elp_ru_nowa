@php
$id = isset($id) ? $id : 0;
$here =  isset($here) ?  $here : (($id ? 'Edytuj' : 'Dodaj').' '.$titles->createButton);

$showHeader = isset($showHeader) ? $showHeader : true;
$showUserInfo = isset($showUserInfo) ? $showUserInfo : false;

$showSubmitButton = isset($showSubmitButton) ? $showSubmitButton : true;
$showCancelButton = isset($showCancelButton) ? $showCancelButton : true;
$showDeleteButton = isset($showDeleteButton) ? $showDeleteButton : true;

$cancelButtonUrl = isset($cancelButtonUrl) ? $cancelButtonUrl : route($routeName.'.index');
$cancelButtonTitle = isset($cancelButtonTitle) ? $cancelButtonTitle : 'Anuluj';

$submitButtonTitle = isset($submitButtonTitle) ? $submitButtonTitle : ($id ? 'Zapisz zmiany' : 'Dodaj');

$formClass = isset($formClass) ? $formClass : '';
$formHorizontal = isset($formHorizontal) ? $formHorizontal : false;
$formUrl = isset($formUrl) ? $formUrl : $url; //zmienic nazwe tego w contoller
$formMethod = isset($formMethod) ? $formMethod : ($id ? 'PUT' : 'POST');
$formTarget = isset($formTarget) ? $formTarget : '_self';

if ($formHorizontal) {
    FormHelper::setLayoutToHorizontal(); //za pozno on to wystawia
    //$formClass = 'form-horizontal';
}

$columns = isset($columns) ? $columns : 1;
$fullWidth = isset($fullWidth) ? $fullWidth : false;

//$canSave = $id ? Auth::user()->can('update', $routeName.'|'.$tabName) : Auth::user()->can('create', $routeName.'|main');
//$canDelete = Auth::user()->can('delete', $routeName.'|main');
$canSave = true;
$canDelete = true;

$breadcrumbs = isset($breadcrumbs) ? $breadcrumbs : [];
if ($titles->index) {
    $breadcrumbs = [route($routeName.'.index')=>$titles->index] + $breadcrumbs;
}
@endphp

@extends('layouts.app', ['showHeader'=>$showHeader])
@section('content-app')
@include('breadcrumb', ['here' => $here, 'breadcrumbs' => $breadcrumbs])
<section class="content">
    {!! Form::open(['url'=>$formUrl, 'id'=>'form-save', 'class'=>$formClass, 'method'=>$formMethod, 'target' => $formTarget]) !!}
    <div class="row">
        <div class="{{ $columns == 2 || $fullWidth || !$showHeader ? 'col-md-12' : 'col-md-6 col-md-offset-3' }}">

            @include('alert')
            @yield('alert')

            <div class="box box-primary">

                {{-- tabs --}}
                @if ($id && count($tabs) > 1)
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        @foreach ($tabs as $tab)
                            <li{!! $tab->current ? ' class="active"' : '' !!}><a href="{{ route($routeName.'.edit', $id).'?'.$tabIdQueryName.'='.$tab->tab_id }}">{{ $tab->title }}</a></li>
                        @endForeach
                    </ul>
                </div>
                @endif

                <div class="box-body">
                    @yield('box-body')
                </div>

                <div class="box-footer">
                    @if ($showSubmitButton)<button type="submit" class="btn btn-primary pull-right"{{ !$canSave ? ' disabled' : '' }} style="margin-left:5px">{{ $submitButtonTitle }}</button>@endif
                    @if ($showCancelButton)<a href="{{ $cancelButtonUrl }}" class="btn btn-default pull-right">{{ $cancelButtonTitle }}</a>@endif
                    @if ($showDeleteButton && $id && $tabId == 1)<button class="btn btn-primary"{{ !$canDelete ? ' disabled' : '' }} id="btn-delete">Usuń</button>@endif
                </div>
            </div>

            @if (!$showHeader)
                @include('user-info')
            @endif
        </div>
    </div>

    @if ($id)
    {!! Form::hidden($tabIdQueryName, $tabId) !!}
    @endif
    
    {!! Form::close() !!}

    @yield('content-footer')
</section>

{{-- form delete --}}
@if ($showDeleteButton && $id && $tabId == 1)
    <div style="display:none">
    {!! Form::open(['method'=>'DELETE', 'id'=>'form-delete', 'route'=>[$routeName.'.destroy', $id]]) !!}
    {!! Form::close() !!}
    </div>
@endif

@yield('form')
@endsection