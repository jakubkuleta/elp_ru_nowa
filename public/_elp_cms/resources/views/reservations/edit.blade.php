@php
Form::model($reservation);
@endphp

@extends('edit', ['showSubmitButton' => $reservation ? false : true])
@section('box-body')

    @if ($reservation)
        <p><strong>Przedmiot rezerwacji:</strong> {!! $reservation->item->name !!}</p>
        <p><strong>Termin:</strong> {!! FormatHelper::dateTimeWithoutSeconds($reservation->date_start) !!}</p>
        <p><strong>Opis:</strong> {!! $reservation->description !!}</p>

    @else
        {!! FormHelper::labelAndSelect('Przedmiot rezerwacji*', 'item_id', $reservationItems) !!}
        <div class="form-group">
            <label class="" for="id-item_id">Data: <em class="req">*</em></label>
            <div>
                {!! Form::text('date_start', Null, ['maxlength'=> 8, 'class' => 'form-control d-inline-block wh50 datepicker', 'autocomplete' => 'off']) !!}
                Godzina od: <input type="text"  name="hour_start" maxlength="5" class="form-control d-inline-block wh50" data-inputmask="'alias': 'hh:mm'" data-mask="", autocomplete="off">
                Godzina do: <input type="text"  name="hour_end" maxlength="5" class="form-control d-inline-block wh50" data-inputmask="'alias': 'hh:mm'" data-mask="", autocomplete="off">
            </div>
        </div>

        {!! FormHelper::labelAndText('Opis*', 'description', ['maxlength' => 50]) !!}
    @endif

@endsection

