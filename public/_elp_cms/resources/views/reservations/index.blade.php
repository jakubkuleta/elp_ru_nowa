@extends('index', ['rightBoxWidth' => 990])
@section('table-body')
    @php
        $table = new TableHelper('list-caption');
        $table->addThSortable('wh50', 'ID', 'reservation_id');
        $table->addThSortable('wh150', '<strong>rezerwujący</strong>', 'user_id');
        $table->addThSortable('wh150', 'przedmiot', 'item_id');
        $table->addThSortable('wh120', 'data początkowwa', 'date_start');
        $table->addThSortable('wh120', 'data końcowa', 'date_end');
        $table->addThSortable('wh400', 'opis', 'description');
        echo $table->render();
    @endphp

    @foreach ($reservations as $reservation)
        <tr>
            <td class="alc"><a href="{{ route('reservations.edit', ['id' => $reservation->reservation_id]) }}">{{ $reservation->reservation_id }}</a></td>
            <td class="wh150">{{ $reservation->user->nazwa }}</td>
            <td class="wh150">{{ $reservation->item->name }}</td>
            <td class="alc">{{ FormatHelper::dateTimeWithoutSeconds($reservation->date_start) }}</td>
            <td class="alc">{{ FormatHelper::dateTimeWithoutSeconds($reservation->date_end) }}</td>
            <td class="wh400">{{ $reservation->description }}</td>
    @endforeach
@endsection
