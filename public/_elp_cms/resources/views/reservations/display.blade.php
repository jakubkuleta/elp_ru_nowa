<!doctype html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="refresh" content="300">
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <style>
        body {background-color:rgb(0, 107, 182);}
        div.dosrodka {text-align:center }
        div.dosrodka img{width:40em; max-width: 100%;
            height: auto;}
        .table {background-color: #8f99a0}
        .table th, .table td {font-size:120%}
        .table-dark .tr-first th {
            background-color: #32383e;
            color: #fff
        }
        p.blocktext {
            color: white;
            margin-left: auto;
            margin-right: auto;
            text-align: center;
            width:8em;
            font-size: 2em;
            font-weight: bold;
        }
        .btn-secondary {
            color: #fff;
            background-color: #6c757d;
            border-color: #6c757d;
        }
        .btn-secondary:hover {
            color: #fff;
            background-color: #5a6268;
            border-color: #545b62;
        }
    </style>
    <title>Rezerwacje</title>
</head>
<body>

<div class="dosrodka">
    <img src="{{ asset('images/logo-reservations.png') }}">
    <p class="blocktext">{!! $item->name !!}</p>
</div>




<a class="btn btn-secondary btn-lg {{ $addDays ? '' : 'disabled' }}" href="{{ route('reservations.display', ['id' => $item->item_id, 'add_days' => $addDays - 1]) }}" role="button">Poprzednie</a>

<a class="btn btn-secondary btn-lg pull-right" href="{{ route('reservations.display', ['id' => $item->item_id, 'add_days' => $addDays + 1]) }}" role="button">Następne</a>

<table class="table table-dark">
    <thead>

    <tr class="tr-first">
        <th scope="col">Rezerwujący</th>
        <th scope="col">Początek</th>
        <th scope="col">Koniec</th>
        <th scope="col">Opis</th>
    </tr>
    </thead>

    <tbody>

    @foreach ($reservations as $reservation)

    <tr>
        <td scope="row">{{ $reservation->user->nazwa }}</td>
        <td>{{ FormatHelper::dateTimeWithoutSeconds($reservation->date_start) }}</td>
        <td>{{ FormatHelper::dateTimeWithoutSeconds($reservation->date_end) }}</td>
        <td>{{ $reservation->description }}</td>
    </tr>
    @endforeach

    </tbody>
</table>

</body>
</html>
