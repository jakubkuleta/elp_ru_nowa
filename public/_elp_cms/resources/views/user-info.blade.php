@if (Auth::check())
<div class="box box-primary">
    <div class="box-body">
        <strong>Zalogowany:</strong> {{ Auth::user()->nazwa }}
        <a class="ml-10 mr-10 show-loader btn btn-primary btn-sm" href="{{ route('home.index') }}">wróć</a>
        {{--
        <a id="btn-logout" class="ml-10 mr-10 show-loader btn btn-primary btn-sm" href="">wyloguj</a>
        <a class="btn btn-primary btn-sm" href="{{ route('auth.change-password.index') }}">zmień hasło</a>
        --}}
    </div>
</div>
@endif