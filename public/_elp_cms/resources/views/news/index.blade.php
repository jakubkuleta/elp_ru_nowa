@extends('index', ['rightBoxWidth' => 990])
@section('table-body')

    @php
        $table = new TableHelper('list-caption');
        $table->addThSortable('wh50', 'id notatki', 'news_id');
        $table->addThSortable('wh50', 'tytuł', 'title');
        $table->addThSortable('wh50', 'nagłówek', 'intro');
        $table->addThSortable('wh400', 'ukryj', 'hide');
        $table->addThSortable('wh400', 'ikona', 'image');
        $table->addThSortable('wh120', 'utworzono', 'created_at');
        $table->addThSortable('wh400', 'opublikuj w dniu', 'publish_from');
        echo $table->render();
    @endphp

    @foreach ($news as $new)
        <tr>
            <td class="wh150"><a href="{{ route('news.edit', ['id' => $new->news_id]) }}">{{ $new->news_id }}</a></td>
            <td class="wh150">{{ $new->title }}</td>
            <td class="wh150">{{ $new->intro }}</td>
            <td class="wh150">{{ $new->hide }}</td>
            <td class="wh150">{{ $new->image }}</td>
            <td class="wh150">{{ $new->created_at }}</td>
            <td class="wh150">{{ $new->publish_from }}</td>
    @endforeach
@endsection

