@php
    Form::model($news);
@endphp

@extends('edit', ['fullWidth' => true])
@section('box-body')



    @if ($news)

        <p><strong>id news:</strong> {{ $news->news_id }} </p>

    @endif

    {!! FormHelper::labelAndText('tytuł', 'title') !!}
    {!! FormHelper::labelAndText('nagłówek', 'intro') !!}
    {!! FormHelper::labelAndTextarea('treść', 'content') !!}
    {!! FormHelper::labelAndCheckboxWithHidden('ukryj', 'hide') !!}
    {!! FormHelper::labelAndText('ikona', 'image') !!}
    {!! FormHelper::labelAndText('utworzono', 'created_at', ['maxlength'=> 10, 'class' => 'form-control d-inline-block wh50 datepicker', 'autocomplete' => 'off']) !!}
    {!! FormHelper::labelAndText('opublikuj w dniu', 'publish_from', ['maxlength'=> 10, 'class' => 'form-control d-inline-block wh50 datepicker', 'autocomplete' => 'off']) !!}
    {!! FormHelper::labelAndText('Metadescription', 'metadescription', ['maxlength'=> 255]) !!}
@endsection
