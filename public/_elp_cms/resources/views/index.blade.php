@php
$items = $$itemsName;

$defaultContainerWidth = Config::get('system.default_container_width');
$containerWidth = isset($containerWidth) ? $containerWidth : $defaultContainerWidth; 

$createButtonTitle = isset($createButtonTitle) ? $createButtonTitle : ('Dodaj '.$titles->createButton);

if (isset($rightBoxWidth)) {
    $containerWidth = $rightBoxWidth + 265 + 20; //20 to padding w  $rightBoxWidth 
    $rightBoxWidth = $rightBoxWidth + 20;
} else {
    $rightBoxWidth = $filters ? ($containerWidth - 265) : '';
}

@endphp

@extends('layouts.app', ['containerWidth'=>$containerWidth])
@section('content-app')
@include('breadcrumb', array('here'=>$titles->index))

<section class="content">

    @if ($filters)
    <div class="pull-left filters">
        <div class="box box-primary">
            <div class="box-header with-border"><h3 class="box-title">Filtrowanie</h3></div>
            <div class="box-body">
                 @include('index-filters')
            </div>
        </div>
    </div>
    @endif

    <div{!! $rightBoxWidth ? ' class="pull-left" style="width:'.$rightBoxWidth.'px"' : '' !!}>
        @include('alert')

        <div class="box box-primary">
            @if ($creatingAvailable)
            <div class="box-header no-padding-b">
                <div class="text-left">
                    @if (Auth::user()->can('create', $routeName.'|main') || 1==1)
                    <a href="{{ route($routeName.'.create') }}" class="btn btn-primary">{{ $createButtonTitle }}</a>
                    @else
                    <button class="btn btn-primary disabled">{{ $createButtonTitle }}</button>
                    @endif
                    @yield('table-header')
                </div>
            </div>
            @endif

            <div class="box-body">
                <table class="table table-hover table-bordered">
                    <tbody>
                        @yield('table-body')

                        @if (!count($items))
                        <tr><td colspan="100%" class="text-center">Brak danych</td></tr>
                        @endif
                    </tbody>
                </table>
            </div>

            <div class="box-footer clearfix">
                <div class="pull-left"><strong>Strona {{ $items->currentPage() }} z {{ $items->lastPage() }} ({{ $items->total() }} pozycji)</strong></div>
                <div class="pull-right">{{ $items->appends(Request::except('page'))->links() }}</div>
            </div>
        </div>
    </div>
</section>
@yield('form')
@endsection