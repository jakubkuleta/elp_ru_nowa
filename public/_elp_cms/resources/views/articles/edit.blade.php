@php
    Form::model($article);
@endphp

@extends('edit', ['fullWidth' => true])
@section('box-body')



    @if ($article)

        <p><strong>id artykułu:</strong> {{ $article->article_id }} </p>

    @endif

    {!! FormHelper::labelAndText('tytuł', 'title') !!}
    {!! FormHelper::labelAndTextarea('treść', 'content') !!}
    {!! FormHelper::labelAndText('link', 'href') !!}
    {!! FormHelper::labelAndCheckboxWithHidden('ukryj', 'hide') !!}
    {!! FormHelper::labelAndText('utworzono', 'created_at', ['maxlength'=> 10, 'class' => 'form-control d-inline-block wh50 datepicker', 'autocomplete' => 'off']) !!}
    {!! FormHelper::labelAndText('opublikuj w dniu', 'publish_from', ['maxlength'=> 10, 'class' => 'form-control d-inline-block wh50 datepicker', 'autocomplete' => 'off']) !!}

@endsection
