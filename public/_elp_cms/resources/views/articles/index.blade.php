@extends('index', ['rightBoxWidth' => 990])
@section('table-body')

    @php
        $table = new TableHelper('list-caption');
        $table->addThSortable('wh50', 'id artykułu', 'article_id');
        $table->addThSortable('wh50', 'tytuł', 'title');
        $table->addThSortable('wh50', 'link', 'href');
        $table->addThSortable('wh400', 'ukryj', 'hide');
        $table->addThSortable('wh120', 'utworzono', 'created_at');
        $table->addThSortable('wh400', 'opublikuj w dniu', 'publish_from');
        echo $table->render();
    @endphp

    @foreach ($articles as $article)
        <tr>
            <td class="wh150"><a href="{{ route('articles.edit', ['id' => $article->article_id]) }}">{{ $article->article_id }}</a></td>
            <td class="wh150">{{ $article->title }}</td>
            <td class="wh150">{{ $article->href }}</td>
            <td class="wh150">{{ $article->hide }}</td>
            <td class="wh150">{{ $article->created_at }}</td>
            <td class="wh150">{{ $article->publish_from }}</td>
    @endforeach
@endsection

