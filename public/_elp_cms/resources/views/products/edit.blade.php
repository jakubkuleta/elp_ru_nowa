@php
    Form::model($product);
@endphp

@extends('edit', ['fullWidth' => true])
@section('box-body')

    @if ($product)

        <p><strong>id produkt:</strong> {{ $product->product_id }} </p>

    @endif

    {!! FormHelper::labelAndText('nazwa', 'name', ['maxlength' => 230]) !!}
    {!! FormHelper::labelAndText('symbol', 'symbol') !!}
    {!! FormHelper::labelAndTextarea('opis', 'description') !!}
    {!! FormHelper::labelAndText('sortowanie', 'sort') !!}
    {!! FormHelper::labelAndText('słowo kluczowe', 'keywords') !!}
    {!! FormHelper::labelAndText('ścieżka obrazu', 'img_path', ['maxlength' => 255]) !!}
    {!! FormHelper::labelAndText('nazwa pliku obrazu', 'img_filename') !!}
    {!! FormHelper::labelAndText('metatitle', 'metatitle') !!}
    {!! FormHelper::labelAndText('metadescription', 'metadescription') !!}

    <strong>Kategorie produktu</strong>
    {!! FormHelper::labelAndSelect('', 'category_ids[]', $categories, null, ['class' => 'select2', 'multiple' => true, 'selectedItems' => $productCategoryRepositoryNew->pluckCategoriesForProduct($product->product_id)]) !!}


@endsection
