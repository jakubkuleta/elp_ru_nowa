@extends('index', ['rightBoxWidth' => 1200])
@section('table-body')

    @php
        $table = new TableHelper('list-caption');
        $table->addThSortable('wh50', 'id produktu', 'product_id');
        $table->addThSortable('wh50', 'nazwa produktu', 'name');
        $table->addThSortable('wh50', 'kategoria produktu', 'category_id');
        $table->addThSortable('wh50', 'nazwa kategorii', 'name');
        $table->addThSortable('wh150', 'symbol', 'symbol');
        $table->addThSortable('wh120', 'sortowanie', 'sort');
        $table->addThSortable('wh400', 'słowa kluczowe', 'keywords');
        $table->addThSortable('wh400', 'ukryj', 'hide');
        $table->addThSortable('wh400', 'ścieżka obrazu', 'img_path');
        $table->addThSortable('wh400', 'nazwa pliku obrazu', 'img_filename');
        $table->addThSortable('wh400', 'metatitle', 'metatitle');
        $table->addThSortable('wh400', 'metadescription', 'metadescription');
        echo $table->render();
    @endphp

    @foreach ($products as $product)
        <tr>
            <td class="wh150"><a href="{{ route('products.edit', ['id' => $product->product_id]) }}">{{ $product->product_id }}</a></td>
            <td class="wh150">{{ $product->name }}</td>
            <td class="wh150">{{ $product->category_id }}</td>
            <td class="wh150">{{ $product->category->name }}</td>
            <td class="wh150">{{ $product->symbol }}</td>
            <td class="alc">{{ $product->sort }}</td>
            <td class="wh400">{{ $product->keywords }}</td>
            <td class="wh400">{{ $product->hide }}</td>
            <td class="wh400">{{ $product->img_path }}</td>
            <td class="wh400">{{ $product->img_filename }}</td>
            <td class="wh400">{{ $product->metatitle }}</td>
            <td class="wh400">{{ $product->metadescription }}</td>
    @endforeach
@endsection

