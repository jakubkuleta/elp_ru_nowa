@include('layouts.inc-head', ['additionalCss'=>['dist/css/iCheck/blue.css']])
<body class="hold-transition login-page">

<div class="login-box">
  <div class="login-logo">
  
    <a href="{{ route('login') }}">
        <img src="{{ asset('images/logo.png') }}" class="img-responsive" style="width:295px; margin:0 auto 25px auto">
        <b>{{ Config::get('app.name') }}</b>
    </a>
    </a>
  </div>

  @yield('content')
</div>

@include('layouts.inc-scripts')
<script src="{{ asset('dist/js/iCheck/icheck.min.js') }}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
<div id="loader"><div></div></div>
</body>
</html>
