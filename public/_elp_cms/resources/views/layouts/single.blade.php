@include('layouts.inc-head')
<body class="body-background">

<div class="content">
    <div class="container">
        @yield('content')
    </div>
</div>

@include('layouts.inc-scripts')
<div id="loader"><div></div></div>
@include('layouts.inc-form-logout')
</body>
</html>