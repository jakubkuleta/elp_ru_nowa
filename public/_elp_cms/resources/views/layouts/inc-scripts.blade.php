@php
$wysiwyg = isset($wysiwyg) ? $wysiwyg : false;
@endphp

{{--
<script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>
<script src="{{ asset('dist/js/app.min.js') }}"></script>
--}}

<script src="{{ asset('dist/js/jquery-2.2.3.min.js') }}"></script>
<script src="{{ asset('dist/js/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('dist/js/datepicker/locales/bootstrap-datepicker-pl.js') }}"></script>

<script src="{{ asset('dist/js/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('dist/js/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('dist/js/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script src="{{ asset('dist/js/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('dist/js/jquery-ui.min.js') }}"></script>

@if ($wysiwyg)
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
@endif

<script src="{{ asset('dist/js/app.min.js?ver=1') }}"></script>
@include('layouts.inc-js')
