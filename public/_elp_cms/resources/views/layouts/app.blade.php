@php
$defaultContainerWidth = Config::get('system.default_container_width');
$containerWidth = isset($containerWidth) ? $containerWidth : $defaultContainerWidth;
$routeName = isset($routeName) ? $routeName : '';
$showHeader = isset($showHeader) ? $showHeader : true;

$containerStyle = '';
if ($containerWidth <> $defaultContainerWidth) {
    $containerStyle = ' style = "width:'.$containerWidth.'px;"';
}

$navbarMenu = [
    [
        'title' => 'Strona główna',
        'icon' => 'fa-home',
        'url' => route('home.index'),
    ],
    [
        'title' => 'Produkty',
        'icon' => 'fa-check-circle-o',
        'group' => 'products',
        'url' => route('products.index')
    ],
    [
        'title' => 'Nowości',
        'icon' => 'fa-check-circle-o',
        'url' => route('news.index'),
    ],
    [
        'title' => 'Artykuły',
        'icon' => 'fa-check-circle-o',
        'url' => route('articles.index'),
    ],
];
ViewHelper::setRouteName($routeName);
@endphp

@include('layouts.inc-head')
<body class="hold-transition skin-blue layout-top-nav fixed">
<div class="wrapper">

  @if ($showHeader)
  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <a href="{{ route('home.index') }}" class="navbar-brand"><b>{{ Config::get('app.name') }}</b></a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            @foreach ($navbarMenu as $menu)
                {!! ViewHelper::menuLi($menu) !!}
            @endforeach
          </ul>
        </div>

        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li class="user-menu">
              <!-- Menu Toggle Button -->
              <a href="" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <img src="{{ asset('dist/img/profile.png') }}" class="user-image" alt="User Image">
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs">{{ str_limit(Auth::user()->nazwa, 25) }}</span>
              </a>
              <ul class="dropdown-menu" style="right: auto; width: auto">
                  {{--<li><a href="{{ route('auth.change-password.index') }}"><i class="fa fa-circle-o"></i> Zmień hasło</a></li>--}}
                  <li><a href="" id="btn-logout" class="show-loader"><i class="fa fa-power-off"></i> Wyloguj</a></li>
              </ul>

            </li>
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  @endif

  <div class="content-wrapper">
    <div class="container"{!! $containerStyle !!}>
     @yield('content-app')
    </div>
  </div>
</div>

@include('layouts.inc-scripts')
<div id="loader"><div></div></div>
@include('layouts.inc-form-logout')
</body>
</html>
