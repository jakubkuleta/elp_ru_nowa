<?php
$showHeader = isset($showHeader) ? $showHeader : true;
$wysiwyg = isset($wysiwyg) ? $wysiwyg : false;
$additionalCss = isset($additionalCss) ? $additionalCss : [];
?>
<!DOCTYPE html>
<html lang="pl">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>@yield('metaTitle')</title>
@if (array_key_exists('metaNoIndex', View::getSections()))<meta name="robots" content="noindex">@endif
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('dist/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('dist/js/datepicker/datepicker3.css') }}">
<link rel="stylesheet" href="{{ asset('dist/js/select2/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css?v=4') }}">
<link rel="stylesheet" href="{{ asset('dist/css/custom.css') }}">
@if ($wysiwyg)
<link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endif
@foreach ($additionalCss as $file)
<link rel="stylesheet" href="{{ asset($file) }}">
@endforeach
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<style>
body {background-color: #ecf0f5;}
.skin-blue .wrapper, .skin-blue .main-sidebar, .skin-blue .left-side {background-color: transparent}
.content-wrapper, .right-side {background-color: transparent}
.wrapper, body {overflow-x: visible;}

.wh30{width:30px}
.wh35{width:35px}
.wh40{width:40px}
.wh50{width:50px}
.wh60{width:60px}
.wh70{width:70px}
.wh80{width:80px}
.wh90{width:90px}
.wh100{width:100px}
.wh120{width:120px}
.wh130{width:130px}
.wh135{width:135px}
.wh150{width:150px}
.wh180{width:180px}
.wh200{width:200px}
.wh250{width:250px}
.wh300{width:300px}
.wh350{width:350px}
.wh400{width:400px}
.wh500{width:500px}
.wh700{width:700px}

.no-padding-b{padding-bottom: 0}
em.req {color:red}
.pagination{margin:0}

.pr-10 {padding-right:10px}
.pl-10 {padding-left:10px}
.pb-10 {padding-bottom:10px}
.pr-0 {padding-right:0}
.mt-5 {margin-top:5px}
.mr-10 {margin-right:10px}
.ml-10 {margin-left:10px}

.alc {text-align: center}
.alr {text-align: right}

.form-group .control-label{padding-right:0 !important}

.bg-green-li{background-color: #B0D8B0 !important;}

.d-none {display: none}
.d-block {display: block}
.d-inline-block {display:inline-block}

.form-group {margin-bottom: 5px;}

.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 4px;
}

.filters {min-width: 220px; margin-right: 15px}
.filters label {display: block; font-weight: normal; margin-bottom: 2px;}
.filters p {margin: 0 0 10px 0}
.filters .form-control {display: inline-block; width: 200px}
.filters .form-control-textarea {height:100px}
.filters .submit {padding-top:10px}

.form-control {padding: 3px 6px; height: 30px;}
input.datepicker {padding-left: 0; padding-right:0; width:80px !important; text-align: center}


.navbar-nav>li>a i {padding-right:3px}

.content-header, .content {padding-left: 0; padding-right: 0}

@media (max-width: 767px) {
.fixed .content-wrapper, .fixed .right-side {
    padding-top: 50px
}}

.clearfix {display: block; content: ""; clear: both;}

.align-text-top {
    vertical-align: text-top!important;
}

@if(!$showHeader)
.content-wrapper {padding-top:15px !important}
@endif

@yield('css')
</style>
</head>