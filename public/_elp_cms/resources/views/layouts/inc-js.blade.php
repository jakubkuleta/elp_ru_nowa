<script>
EA = {
    v: "v0.1.0",
    DEBUG: false,
    _: function () {
        if (EA.DEBUG && window.console) {
            console.log.apply(console, arguments);
        }
    },
    settings: {
    },
    loader: {
        show: function(){
            $('#loader').show();
        },
        hide: function(){
            $('#loader').hide();
        }
    },
    buttonsAndForms: {
        init: function(){
            $('.show-loader').not('.disabled').on('click', function(e){
                EA.loader.show();
            });

            $('.redirect').not('.disabled').on('click', function() {
		        location.href = $(this).data('href');
	        });

            $('#btn-delete').on('click', function(e){
                e.preventDefault();

                if ($(this).hasClass('disabled')) {
                    return;
                }

                if (confirm('Czy na pewno usunąć ?')) {
                    EA.loader.show();
                    $('#form-delete').submit();
                }
            });

            $('#btn-logout').on('click', function(e){
                e.preventDefault();
                $('#form-logout').submit();
            });

            $('#form-save').on('submit', function(e){
                e.preventDefault();

                if ($(this).hasClass('do-not-submit')) {
                    return;
                }

                if ($(this).hasClass('submitted')) {
                    return;
                }

                EA.loader.show();

                if(typeof beforeSubmitFormSave == 'function'){
                    beforeSubmitFormSave();
                }

                $(this).addClass('submitted');

                this.submit();
            });

            $('input.enter-to-tab').keydown( function(e) {
                var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
                if(key == 13) {
                    e.preventDefault();
                    var inputs = $(this).closest('form').find(':input:visible').not('.enter-to-tab-ommit');
                    inputs.eq( inputs.index(this)+ 1 ).focus();
                }
            }); 
        }

    },
    plugins: {
        init: function () {

            $('.datepicker').datepicker({
                autoclose: true,
                language: 'pl',
                format: "yyyy-mm-dd",
            });

            $('[data-mask]').inputmask();

            $(".select2").select2();
        }
    },
    onReady: function () {
        EA._("EA: ready");

        EA.buttonsAndForms.init();
        EA.plugins.init();
        @yield('js')
    }
}

@yield('jsFunctions')
$(document).ready(EA.onReady);
</script>