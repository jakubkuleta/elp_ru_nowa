@extends('index', ['rightBoxWidth' => 850])
@section('table-body')
    @php
    $table = new TableHelper('list-caption');
    $table->addThSortable('wh50', 'ID', 'customer_id');
    $table->addThSortable('wh400', '<strong>nazwa</strong>', 'name');
    $table->addThSortable('wh400', 'ulica', 'street');
    echo $table->render();
    @endphp

    @foreach ($testCustomers as $testCustomer)
    <tr>
        <td class="alc">{{ $testCustomer->customer_id }}</td>
        <td class="wh400"><a href="{{ route('test-customers.edit', ['id' => $testCustomer->customer_id]) }}">{{ $testCustomer->name }}</a></td>
        <td class="wh400">{{ $testCustomer->street }}</td>
    @endforeach
@endsection