@extends('layouts.auth')
@section('metaTitle', 'Logowanie')
@section('content')
<!-- /.login-logo -->

<div class="login-box-body">
    <p class="login-box-msg">Logowanie</p>

    {!! Form::open(array('url'=>route('login'))) !!}
      <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
       @if ($errors->has('email'))<label class="control-label"><i class="fa fa-times-circle-o"></i> {{ $errors->first('email') }}</label>@endif
       {!! Form::text('email', old('email'), array('class'=>'form-control', 'placeholder'=>'E-mail', 'autofocus'=>true)) !!}
      </div>

      <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
        @if ($errors->has('password'))<label class="control-label"><i class="fa fa-times-circle-o"></i> {{ $errors->first('password') }}</label>@endif
        {!! Form::password('password', array('class'=>'form-control', 'placeholder'=>'Hasło')) !!}
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              {!! Form::checkbox('remember', old('remember') ? true : false) !!} &nbsp; Zapamiętaj mnie
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block show-loader">Zaloguj</button>
        </div>
        <!-- /.col -->
      </div>

    {!! Form::close() !!}

    <a href="{{ route('password.request') }}">Zapomniałem hasła</a><br>
  </div>
  <!-- /.login-box-body -->
@endsection