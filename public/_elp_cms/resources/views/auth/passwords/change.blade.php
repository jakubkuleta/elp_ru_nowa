@extends('layouts.app')
@section('content-app')
@include('breadcrumb', ['here'=>'Zmień hasło'])
<section class="content">
    {!! Form::open(['url'=>route('auth.change-password.update'), 'id'=>'form-save', 'method'=>'POST']) !!}
    <div class="row">
        <div class="col-md-6 col-md-offset-3">

            @include('alert')

            <div class="box box-primary">

                <div class="box-body">
                    <div class="form-group">
                        <label class="" for="id-current_password">bieżące hasło <em class="req">*</em></label>
                        {{  Form::input('password', 'current_password', old('current_password'), array('class'=>trim('form-control'), 'maxlength' => 30, 'autocomplete'=>'off', 'id'=>'id-current_password')) }}
                    </div>
                    <div class="form-group">
                        <label class="" for="id-new_password">nowe hasło <em class="req">*</em></label>
                        {{  Form::input('password', 'new_password', old('new_password'), array('class'=>trim('form-control'), 'maxlength' => 30, 'autocomplete'=>'off', 'id'=>'id-new_password')) }}
                    </div>
                    <div class="form-group">
                        <label class="" for="id-confirm_new_password">powtórz nowe hasło <em class="req">*</em></label>
                        {{  Form::input('password', 'confirm_new_password', old('confirm_new_password'), array('class'=>trim('form-control'), 'maxlength' => 30, 'autocomplete'=>'off', 'id'=>'id-confirm_new_password')) }}
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left:5px">Zmień hasło</button>
                </div>
            </div>
        </div>
    </div>
    
    {!! Form::close() !!}
</section>
@endsection