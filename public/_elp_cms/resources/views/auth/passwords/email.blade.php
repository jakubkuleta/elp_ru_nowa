@extends('layouts.auth')
@section('metaTitle', 'Resetuj hasło')
@section('content')

  <div class="login-box-body">
    <p class="login-box-msg">Resetuj hasło</p>

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    {!! Form::open(array('url'=>route('password.email'))) !!}
      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
       @if ($errors->has('email'))<label class="control-label"><i class="fa fa-times-circle-o"></i> {{ $errors->first('email') }}</label>@endif
       {!! Form::text('email', old('email'), array('class'=>'form-control', 'placeholder'=>'E-mail', 'autofocus'=>true)) !!}
      </div>

      <div class="row">
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block pull-right show-loader" style="max-width:200px">Wyślij link resetujący hasło</button>
        </div>
      </div>

    {!! Form::close() !!}

    <br><a href="{{ route('home.index') }}">Wróć do logowania</a>
  </div>

@endsection
