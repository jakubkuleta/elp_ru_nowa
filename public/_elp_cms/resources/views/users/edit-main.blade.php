@php
Form::model($user);

$homepages = [];
foreach (Config::get('system.homepages') as $homepageId => $homepageData) {
    $homepages[$homepageId] = $homepageData['name'];
}
@endphp

@extends('edit', ['formHorizontal' => true])
@section('content')
{!! FormHelper::labelAndText('nazwisko i imię*', 'name', ['maxlength'=>50]) !!}
{!! FormHelper::labelAndText('inicjały*', 'initials', ['maxlength'=>30]) !!}
{!! FormHelper::labelAndText('e-mail*', 'email', ['maxlength'=>100]) !!}
{!! FormHelper::labelAndPassword('hasło', 'password', ['maxlength'=>100]) !!}
{!! FormHelper::labelAndPassword('powtórz hasło', 'password_confirmation', ['maxlength'=>100]) !!}
{!! FormHelper::labelAndCheckboxWithHidden('czy admin', 'is_admin') !!}
{!! FormHelper::labelAndCheckboxWithHidden('aktywny', 'active') !!}
{!! FormHelper::labelAndSelect('obszar', 'area_id', $areas) !!}
{!! FormHelper::labelAndSelect('strona startowa', 'homepage_id', $homepages) !!}
@endsection