@php
$userPermissions = $user->permissions;
$header = '';
@endphp

@extends('edit')

@section('css')
.table-permissions td.header-1 {padding: 8px 5px; background-color:#E5E3E0; border-top-width:2px !important}
.table-permissions td.header-1 strong {cursor: pointer; }
.table-permissions td.header-1 strong span {color: #696969; padding-left:5px}
.table-permissions td.header-1 strong span.has-prmissions {color:#FF0000;}
.table-permissions td.header-2 {font-weight:bold;  background-color:#F2F2F2; padding-left: 10px}
.table-permissions td.permission-1 {padding-left: 21px;}
.table-permissions td input.checkbox {min-height:auto}
@endsection

@section('js')
$('.checkbox').change(function(){
    if ($(this).is(':checked')) {
        $(this).closest('td').addClass('bg-green-li');
    } else {
        $(this).closest('td').removeClass('bg-green-li');
    }

    permissionCount();
});

$('.select-permissions').click(function(){
    var group_name = $(this).data('group_name');
    var read_only = $(this).data('read_only');		
    var checked = ($(this).data('checked')) ? true : false;

    var find = '.checkbox.' + group_name;

    if (read_only) {
        find = find + '.read-only'; 
    }	

    $(this).closest('form').find(find).prop('checked', checked).trigger('change');
    return false;
});

$('.header').click(function(){	
    var group_name = $(this).data('group_name');
    $(this).closest('table').find('tr.cl-'+group_name).fadeToggle('fast');
    $(this).closest('td').find('a').fadeToggle("fast");		
});

permissionCount();

function permissionCount()
{
    var headers = $('.table-permissions').find('.header');
    var group_name;
    var items;
    
    headers.each(function() {
        group_name = $(this).data('group_name');
        if (group_name) {
            items = $('.table-permissions').find('input.checkbox.'+group_name+':checked').length;
            $(this).find('span').html('('+items+')').removeClass('has-prmissions');

            if (items) {
                $(this).find('span').addClass('has-prmissions');
            }
        }
    });

}
@endsection


@section('content')

<table class="table table-hover table-bordered table-permissions">
@foreach ($permissions as $permission)

    @php
    $name = 'permission_id['.$permission->permission_id.']';
    $hasPermission = count($userPermissions->where('permission_id', $permission->permission_id));

    if ($permission->header) {
        $header = '<strong>'.$permission->name.'</strong>';
    }

    //naglowek glowny
    if ($permission->header == 1 && $permission->group_name) {
        $tools = '';			
        $tools.= '<a href="" class="d-none pl-10 select-permissions pull-right" data-checked="1" data-group_name="'.$permission->group_name.'" data-read_only="1"><small>zaznacz przeglądanie</small></a>';
        $tools.= '<a href="" class="d-none pl-10 select-permissions pull-right" data-checked="0" data-group_name="'.$permission->group_name.'"><small>odznacz wszystko</small></a>';
        $tools.= '<a href="" class="d-none pl-10 select-permissions pull-right" data-checked="1" data-group_name="'.$permission->group_name.'"><small>zaznacz wszystko</small></a>';

        $header = '<strong style="cursor:pointer" class="header" data-group_name="'.$permission->group_name.'">'.$permission->name.'<span></span></strong>'.$tools;	
    }

    $trClass = '';
    if ($permission->header != 1 && $permission->group_name) {
        $trClass = 'd-none cl-'.$permission->group_name;
    }

    $readOnly = ($permission->read_only) ? 'read-only' : '';

    @endphp

    <tr class="{{ $trClass }}">
        @if ($permission->header)
            <td colspan="2" class="header-{{ $permission->header }}">{!! $header !!}</td>
        @else
            <td class="permission-{{ $permission->style }}">{{ $permission->name }}</td>
            <td class="text-center{{ $hasPermission ? ' bg-green-li' : '' }}">{{ !$permission->header ? Form::checkbox($name, 1, (bool)$hasPermission, array('style'=>'display:inline', 'class'=>'inp checkbox '.$permission->group_name.' '.$readOnly)) : '' }}</td>
        @endif
    </tr>

@endforeach
</table>

@endsection

