@extends('index')
@section('content')
    @php
    $table = new TableHelper('list-caption');
    $table->addThSortable('wh50', 'ID', 'user_id');
    $table->addThSortable('', '<strong>nazwisko i imię</strong>', 'name');
    $table->addThSortable('wh130', 'inicjały', 'initials');
    $table->addThSortable('', 'e-mail', 'email');
    $table->addThSortable('', 'obszar');
    $table->addThSortable('wh50', 'czy<br>admin', 'is_admin');
    $table->addThSortable('wh50', 'akty-<br>wny', 'active');
    echo $table->render();
    @endphp

    @foreach ($users->load(['area']) as $user)
    <tr>
        <td class="alc">{{ $user->user_id }}</td>
        <td><a href="{{ route('users.edit', $user->user_id) }}">{{ $user->name }}</a></td>
        <td>{{ $user->initials }}</td>
        <td>{{ $user->email }}</td>
        <td>{{ $user->area_id ? $user->area->name : '' }}</td>
        <td class="alc"><span class="label label-{{ $user->is_admin ? 'success' : 'danger' }}">{{ $user->is_admin ? 'Tak' : 'Nie' }}</span></td>
        <td class="alc"><span class="label label-{{ $user->active ? 'success' : 'danger' }}">{{ $user->active ? 'Tak' : 'Nie' }}</span></td>
    </tr>
    @endforeach
@endsection