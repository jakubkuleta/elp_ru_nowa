{!! Form::open(array('method'=>'get')) !!}
@php
$lp = 0;
foreach ($filters as $item){
    $lp++;
    $multipleParam = array();
    
    $iClass = ($item['iClass']) ? ' '.$item['iClass'] : '';
    $iStyle = ($item['iStyle']) ? ' '.$item['iStyle'] : ''; 
    $iParameters = ($item['iParameters']) ? $item['iParameters'] : array();
    $inline = $item['inline'];
    
    $pClass = ($item['pClass']) ? (' class="'.$item['pClass'].'"') : ''; 
    $pId = ($item['pId']) ? (' id="'.$item['pId'].'"') : ''; 
                                
    $id = 'f_'.$lp;				
    $label = ($item['label']) ? $item['label'] : $item['name'];

    if ($item['type'] == 'hidden') {
        echo Form::hidden($item['name'], Request::get($item['name']));
    } else {
        //nie hidden

        echo '<p'.$pClass.$pId.'>';

        if (!$inline) {
            echo '<label for="'.$id.'">'.$label.':</label>';
        }

        switch ($item['type']) {
    
            case 'text':
                echo Form::text($item['name'], Request::get($item['name']), array('class'=>'form-control'.$iClass, 'id'=>$id, 'autocomplete'=>'off'), 50);
                break;

            case 'textarea':
                echo Form::textarea($item['name'], Request::get($item['name']), array('class'=>'form-control form-control-textarea'.$iClass, 'id'=>$id, 'autocomplete'=>'off'));
                break;

            case 'checkbox':
                echo Form::checkbox($item['name'], 1, (bool)Request::get($item['name']), array('class'=>'inp'.$iClass, 'id'=>$id));
                if ($inline) {
                    echo '<label for="'.$id.'" class="inline">'.$label.'</label>';
                }
                break;
    
            case 'select':
                $name = $item['name'];
                $list = $item['source'] == 'optionsYesNo' ? FormHelper::optionsYesNo() : ${$item['source']};
                $addEmpty = true;
                
                if ($item['multiple']) {
                    $multipleParam = array('multiple'=>true, 'size'=>1);
                    $name.= '[]'; //dodaje [] jak jest multiple
                    $addEmpty = false;
                }
                
                if ($addEmpty) {
                    $list = FormHelper::prependEmptyOption($list);
                }

                echo Form::select($name, $list, Request::get($item['name']), array_merge(array('class'=>'form-control'.$iClass, 'id'=>$id, 'style'=>$iStyle), (array)$iParameters, (array)$multipleParam));
                break;
                
            case 'date_from_to':
                echo Form::text($item['name'], Request::get($item['name']), array('class'=>'form-control datepicker', 'id'=>$id, 'autocomplete'=>'off'), 10) . ' - ' . Form::text($item['name2'], Request::get($item['name2']), array('class'=>'form-control datepicker', 'autocomplete'=>'off'), 10);
                break;

            case 'text_from_to':
                echo Form::text($item['name'], Request::get($item['name']), array('class'=>'form-control text-center', 'style'=>'width:65px', 'id'=>$id, 'autocomplete'=>'off'), 10) . ' - ' . Form::text($item['name2'], Request::get($item['name2']), array('class'=>'form-control text-center', 'style'=>'width:70px', 'autocomplete'=>'off'), 10);
                break;
        }
        echo '</p>';
    }
    
}
@endphp

@if ($filters)
    <div class="submit">
        <button type="submit" class="btn btn-primary mr-10">Zastosuj</button>
        <a href="{{ route($routeName.'.index') }}">Wyczyść kryteria</a>
        @if ($routeName == 'failures'
        || $routeName == 'scanned-pallets-comparison'
        || $routeName == 'packed-products'
        || $routeName == 'inventories'
        ) {{-- do skonczenia --}}
        <br><a href="{{ Request::fullUrlWithQuery(['exportToExcel' => 1]) }}" class="d-inline-block mt-5"><i class="fa fa-file-excel-o"></i> Eksportuj do Excel</a>
        @endif
    </div>
@endif
{{ Form::hidden('orderBy', Request::get('orderBy')) }}
{{ Form::hidden('filter', 1) }}
{!! Form::close() !!}