@php
$showHeader = isset($showHeader) ? $showHeader : true;

$here =  isset($here) ?  $here : '';

$formUrl = isset($formUrl) ? $formUrl : '';

$columns = isset($columns) ? $columns : 1;
$fullWidth = isset($fullWidth) ? $fullWidth : false;

$breadcrumbs = isset($breadcrumbs) ? $breadcrumbs : [];
if ($titles->index) {
    $breadcrumbs = [route($routeName.'.index')=>$titles->index] + $breadcrumbs;
}
@endphp

@extends('layouts.app', ['showHeader' => $showHeader])
@section('content-app')
@include('breadcrumb', ['here' => $here, 'breadcrumbs' => $breadcrumbs])
<section class="content">

    @if ($formUrl)
        {{ Form::open(['url' => $formUrl, 'id' => 'form', 'method' => 'GET']) }}
    @endif

    <div class="row">
        <div class="{{ $columns == 2 || $fullWidth || !$showHeader ? 'col-md-12' : 'col-md-6 col-md-offset-3' }}">

            @include('alert')
            @yield('alert')

            <div class="box box-primary">
                <div class="box-body">
                    @yield('box-body')
                </div>

                <div class="box-footer">
                    @yield('box-footer')
                </div>
            </div>

            @if (!$showHeader)
                @include('user-info')
            @endif
        </div>
    </div>

    @if ($formUrl)
        {!! Form::close() !!}
    @endif

</section>

@yield('form')
@endsection