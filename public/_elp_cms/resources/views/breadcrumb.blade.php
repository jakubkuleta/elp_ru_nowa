@php
$showHeader = isset($showHeader) ? $showHeader : true;
@endphp

@section('metaTitle', $here)

@if ($showHeader)
    <section class="content-header">
    <h1>{{ $here }}</h1>
    <ol class="breadcrumb">
    <li><a href="{{ route('home.index') }}"><i class="fa fa-home"></i> Strona Główna</a></li>
    @if (isset($breadcrumbs))
    @foreach ($breadcrumbs as $url => $href)
        @if ($href)
        <li><a href="{{ $url }}"><i></i> {{ $href }}</a></li>
        @endif
    @endforeach
    @endif
    <li class="active">{{ $here }}</li>
    </ol>
    </section>
@else
    <section class="content-header" style="padding:0"><h1>{{ $here }}</h1></section>
@endif