<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::pattern('id', '[0-9]+');
Route::pattern('id2', '[0-9]+');
Route::pattern('name', '[0-9a-zA-Z_-]+');
Route::pattern('user', '[0-9]+');

Auth::routes(['register' => false]);
Route::get('/change-password', array('as' => 'auth.change-password.index', 'uses' => '\App\Http\Controllers\Auth\ChangePasswordController@index'));
Route::post('/change-password', array('as' => 'auth.change-password.update', 'uses' => '\App\Http\Controllers\Auth\ChangePasswordController@update'));
Route::get('/autologin/{user}', array('as' => 'auth.autologin', 'uses' => '\App\Http\Controllers\Auth\AutologinController@autologin'));


Route::get('home', function () {
    return redirect('/');
});

Route::group(array('middleware' => 'auth'), function () {
    Route::get('/', array('as' => 'home.index', 'uses' => 'HomeController@index'));

    Route::get('/news', array('as' => 'news.index', 'uses' => 'NewsController@index'));
    Route::get('/news/create', array('as' => 'news.create', 'uses' => 'NewsController@create'));
    Route::get('/news/{id}/edit', array('as' => 'news.edit', 'uses' => 'NewsController@edit'));
    Route::put('/news/{id}', array('as' => 'news.update', 'uses' => 'NewsController@update'));
    Route::post('/news', array('as' => 'news.store', 'uses' => 'NewsController@store'));
    Route::delete('/news/{id}', array('as' => 'news.destroy', 'uses' => 'NewsController@destroy'));

    Route::get('/articles', array('as' => 'articles.index', 'uses' => 'ArticleController@index'));
    Route::get('/articles/create', array('as' => 'articles.create', 'uses' => 'ArticleController@create'));
    Route::get('/articles/{id}/edit', array('as' => 'articles.edit', 'uses' => 'ArticleController@edit'));
    Route::put('/articles/{id}', array('as' => 'articles.update', 'uses' => 'ArticleController@update'));
    Route::post('/articles', array('as' => 'articles.store', 'uses' => 'ArticleController@store'));
    Route::delete('/articles/{id}', array('as' => 'articles.destroy', 'uses' => 'ArticleController@destroy'));

    Route::get('/products', array('as' => 'products.index', 'uses' => 'ProductController@index'));
    Route::get('/products/create', array('as' => 'products.create', 'uses' => 'ProductController@create'));
    Route::get('/products/{id}/edit', array('as' => 'products.edit', 'uses' => 'ProductController@edit'));
    Route::put('/products/{id}', array('as' => 'products.update', 'uses' => 'ProductController@update'));
    Route::post('/products', array('as' => 'products.store', 'uses' => 'ProductController@store'));
    Route::delete('/products/{id}', array('as' => 'products.destroy', 'uses' => 'ProductController@destroy'));
});
