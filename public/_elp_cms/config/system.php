<?php

return [

    'default_container_width' => 1170,
    'holiday_requests' => [
        'statusNames' => [
            1 => 'oczekuje',
            2 => 'zaakceptowany',
            3 => 'odrzucony',
        ],
        'statusColors' => [
            1 => '#d2d6de',
            2 => '#85e085',
            3 => '#ff8080',
        ],
        'hr_email' => env('HOLIDAY_REQUESTS_HR_EMAIL'),
        'bcc_email' => env('HOLIDAY_REQUESTS_BCC_EMAIL'),
    ]
];

