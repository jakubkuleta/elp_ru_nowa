<?php

namespace App\Http\Controllers;

use App\Repositories\ReservationRepository;
use App\Http\Requests\ReservationRequest;
use App\Repositories\ReservationItemRepository;

class ReservationController extends BaseController
{
    protected $reservationItemRepository;

    public function __construct(ReservationRepository $reservationRepository, ReservationItemRepository $reservationItemRepository)
    {
        $this->setRepository($reservationRepository);
        $this->setRequestName('reservationRequest');
        $this->setTitles('rezerwacje', 'rezerwacja', 'rezerwację');
        $this->reservationItemRepository = $reservationItemRepository;
    }

    public function index(array $parameters = null)
    {
        $this->pushFilter('text', ['name' => 'description', 'label'=>'opis']);
        return parent::index();
    }

    public function store(array $parameters = null)
    {
        return parent::store(['backToIndex' => true]);
    }

    public function update($id, array $parameters = null)
    {
        abort(404);
    }

    protected function viewData($view)
    {
        $data = [];

        switch ($view) {
            case self::VIEW_EDIT:
            case self::VIEW_CREATE:
                $data = [
                    'reservationItems' => $this->reservationItemRepository->pluck(),
                ];
                break;
        }

        return $data;
    }

    public function display($id)
    {
        $addDays = (int) request()->get('add_days');

        $item = $this->reservationItemRepository->findOrFail($id);
        $reservations = $this->getRepository()->getCurrunt($id, $addDays);

        return view('reservations.display', ['item' => $item, 'reservations' => $reservations, 'addDays' => $addDays]);
    }

}
