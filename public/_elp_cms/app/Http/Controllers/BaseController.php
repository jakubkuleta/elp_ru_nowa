<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\ToolTrait;
use App\Http\Controllers\Traits\TabTrait;
use App\Http\Controllers\Traits\AbilityTrait;
use App\Http\Controllers\Traits\FilterTrait;
use App\Http\Controllers\Traits\ExcelTrait;
use Request;
use Route;
use \Auth;
use \Exception;
use Illuminate\Support\Arr;

class BaseController extends Controller
{
    use ToolTrait;
    use TabTrait;
    use AbilityTrait;
    use FilterTrait;
    use ExcelTrait;

    const VIEW_INDEX = 'index';
    const VIEW_SHOW = 'show';
    const VIEW_EDIT = 'edit';
    const VIEW_CREATE = 'create';

    const FLASH_MESSAGE_CREATED = 'created';
    const FLASH_MESSAGE_UPDATED = 'updated';
    const FLASH_MESSAGE_DELETED = 'deleted';
    const FLASH_MESSAGE_APPROVED = 'approved';
    const FLASH_MESSAGE_CANCELED = 'canceled';

    protected $repository;
    protected $request;
    protected $requestName;
    protected $viewData = [];

    protected $checkBasicAbilities = false; //do usuniecia bo korzystam z Permission

    protected $creatingAvailable = true;

    protected $item;

    public function __construct()
    {
    }

    protected function render($view, array $viewData = null)
    {
        return view($view, (array) $viewData)
        ->with('titles',  $this->getTitles())
        ->with('routeName', $this->getRouteName())
        ->with('creatingAvailable', $this->creatingAvailable);
    }

    public function addViewData($key, $val)
    {
        $this->viewData[$key] = $val;
    }

    protected function setRepository($repository)
    {
        $this->repository = $repository;
    }

    protected function getRepository()
    {
        return $this->repository;
    }

    protected function setRequestName($requestName)
    {
        $this->requestName = $requestName;
    }

    protected function getRequest()
    {
        return $this->request;
    }

    public function index(array $parameters = null)
    {
        if ($this->checkBasicAbilities) {
            $this->checkAbility('index');
        }

        //do skonczenia
        if ($excel = (int) Request::input('exportToExcel')) {
            return $this->exportToExcel(array('id'=>$excel));
        }

        $items = $this->repository
        ->pushAppliedFilters($this->getAppliedFilters())
        ->setOrderBy($this->getAppliedOrderBy())
        ->paginate();

        $viewData = array_merge([
            'itemsName' => $this->getItemsName(),
            $this->getItemsName() => $items,
            'filters' => $this->getFilters(),
        ],  $this->getViewData(self::VIEW_INDEX), (array)array_get($parameters, 'viewData'));

        return $this->render($this->getViewDirectory().'.index', $viewData);
    }

    public function show($id, array $parameters = null)
    {
        if ($this->checkBasicAbilities) {
            $this->checkAbility('view'); //tu powinien byc "index" a sprawdzenie w widoku?
        }

        $item =  $this->repository->findOrFail($id);

        $viewData = array_merge([
            'id' => $id,
            $this->getItemName() => $item,
        ],  $this->getViewData(self::VIEW_SHOW, $item), (array)array_get($parameters, 'viewData'));

        return $this->render($this->getViewDirectory().'.show', $viewData);
    }

    public function edit($id, array $parameters = null)
    {
        if ($this->checkBasicAbilities) {
            $this->checkAbility('view'); //tu powinien byc "index" a sprawdzenie w widoku?
        }

        $item =  $this->repository->findOrFail($id);

        $viewData = array_merge([
            'id' => $id,
            $this->getItemName() => $item,
            'url' => route($this->getRouteName().'.update', $id),
            'tabs' => $this->getTabs(),
            'tabId' => $this->getTabId(),
            'tabName' => $this->getTabName(),
            'tabIdQueryName' => $this->getTabIdQueryName(),
        ],  $this->getViewData(self::VIEW_EDIT, $item), (array)array_get($parameters, 'viewData'));

        return $this->render($this->getViewForEdit($parameters), $viewData);
    }

    public function create(array $parameters = null)
    {
        if ($this->checkBasicAbilities) {
            $this->checkAbility('create');
        }

        $viewData = array_merge([
            $this->getItemName() => null,
            'url' => route($this->getRouteName().'.store'),
        ],  $this->getViewData(self::VIEW_CREATE), (array)array_get($parameters, 'viewData'));

        return $this->render($this->getViewForCreate($parameters), $viewData);
    }

    public function update($id, array $parameters = null)
    {

        if ($this->checkBasicAbilities) {
            $this->checkAbility('update');
        }

        $requestName = Arr::get($parameters, 'requestName', ($this->requestName ? $this->requestName : 'BaseRequest'));
        $this->request = \App::make('App\Http\Requests\\'.$requestName);

        $this->repository->update($this->request->all(), $id);

        $this->setFlashMessage(self::FLASH_MESSAGE_UPDATED);

        $redirectToRoute = array_get($parameters, 'redirectToRoute', $this->getRouteName().'.edit');

        if (Route::has($redirectToRoute)) {
            return redirect()->route($redirectToRoute, $id);
        }
    }

    public function store(array $parameters = null)
    {
        if ($this->checkBasicAbilities) {
            $this->checkAbility('create');
        }

        $requestName = Arr::get($parameters, 'requestName', ($this->requestName ? $this->requestName : 'BaseRequest'));
        $this->request = \App::make('App\Http\Requests\\'.$requestName);

        $this->setItem($this->repository->create($this->request->all()));

        $this->setFlashMessage(self::FLASH_MESSAGE_CREATED);

        $redirectToRoute = array_get($parameters, 'redirectToRoute', $this->getRouteName().'.edit');

        if (array_get($parameters, 'backToIndex')) {
            $redirectToRoute = $this->getRouteName().'.index';
        }

        if (Route::has($redirectToRoute)) {
             return redirect()->route($redirectToRoute, $this->getItem());
        }
    }

    protected function setItem($item)
    {
        $this->item = $item;
    }

    protected function getItem()
    {
        return $this->item;
    }

    public function destroy($id, array $parameters = null)
    {
        if ($this->checkBasicAbilities) {
            $this->checkAbility('delete');
        }

        $this->repository->delete($id);

        $this->setFlashMessage(self::FLASH_MESSAGE_DELETED);

        $redirectToRoute = array_get($parameters, 'redirectToRoute', $this->getRouteName().'.index');

        if (Route::has($redirectToRoute)) {
            return redirect()->route($redirectToRoute);
        }
    }

    protected function getUserId()
    {
        return Auth::id();
    }

    protected function getViewDirectory()
    {
        return $this->getRouteName();
    }

    /**
     * Dla UserController zwroci: "users"
     */
    protected function getItemsName()
    {
        return camel_case($this->getRouteName());
    }

    /**
     * Dla UserController zwroci: "user"
     */
    protected function getItemName()
    {
        return str_singular(camel_case($this->getRouteName()));
    }

    /**
     * Dla user.index zwroci: "user"
     */
    protected function getRouteName()
    {
        $parts = explode('.', Route::currentRouteName());
        array_pop($parts);
        return join('.', $parts);
    }

    /**
     * Zwraca dane z metody viewData controllera nadrzednego
     */
    protected function getViewData($view, $item = null)
    {
        return method_exists($this, 'viewData') ? $this->viewData($view, $item) : [];
    }

    protected function getViewForEdit(array $parameters = null)
    {
        $viewFile = 'edit'.$this->getTabNameForView();

        if ($customViewFile = array_get($parameters, 'viewFile')) {
            $viewFile = $customViewFile;
        }

        return $this->getViewDirectory().'.'.$viewFile;
    }


    protected function getViewForCreate(array $parameters = null)
    {
        $viewDirectory = $this->getViewDirectory();

        //domyslnie szuka widoku "create", jesli nie znajdzie szuka "edit"
        $viewFile = 'create';
        if (!view()->exists($viewDirectory.'.create')) {
            $viewFile = 'edit'.$this->getTabNameForView();
        }

        if ($customViewFile = array_get($parameters, 'viewFile')) {
            $viewFile = $customViewFile;
        }

        return $viewDirectory.'.'.$viewFile;
    }

    protected function checkPermission($permission)
    {
        if (!auth()->user()->hasPermissionTo($permission)) {
            abort(403);
        }
    }
}
