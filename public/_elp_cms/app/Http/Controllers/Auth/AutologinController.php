<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use \Auth;

class AutologinController extends Controller
{
    public function autologin(User $user, Request $request)
    {
        if (!$redirectTo = $request->get('redirect_to')) {
            $redirectTo = '/';
        }

        if (!$request->hasValidSignature()) {
            return redirect()->to($redirectTo);
        }

        Auth::login($user);

        return redirect()->to($redirectTo);
    }
}
