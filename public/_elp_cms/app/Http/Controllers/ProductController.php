<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProductRepository;
use App\Models\ProductCategoryNew;
use App\Repositories\NavbarCategoryRepository;
use App\Repositories\ProductCategoryRepositoryNew;

class ProductController extends BaseController
{
    protected $navbarCategoryRepository;
    protected $productCategoryRepositoryNew;
    protected $request;
    protected $productCategory;

    public function __construct(ProductRepository $productRepository, ProductCategoryRepositoryNew $productCategoryRepositoryNew, NavbarCategoryRepository $navbarCategoryRepository)
    {
        $this->setRepository($productRepository);
        $this->setRequestName('ProductRequest');
        $this->setTitles('Produkty', 'produkty', 'produkt');
        $this->productCategoryRepositoryNew = $productCategoryRepositoryNew;
        $this->navbarCategoryRepository = $navbarCategoryRepository;
    }

    public function index(array $parameters = null)
    {
        $this->pushFilter('text', ['name' => 'symbol', 'label'=>'symbol']);
        $this->pushFilter('text', ['name' => 'name', 'label'=>'nazwa produktu']);
        $this->pushFilter('select', ['name' => 'category_id', 'label'=>'nazwa kategorii', 'source' => 'categories']);
        return parent::index();
    }

    public function update($id, array $parameters = null)
    {
        ProductCategoryNew::where('product_id', $id)->delete();

        if((request()->input('category_ids')))
        {
            foreach(request()->input('category_ids') as $category_id)
            {
                $this->productCategoryNew = ProductCategoryNew::create(['product_id' => $id, 'category_id' => $category_id]);
            }
        }

        return parent::update($id, $parameters);
    }

    protected function viewData($view)
    {
        $data = [];
    //wstrzykuje zmienne do widoku przy użyciu wybranych metod (case self)
        switch ($view) {
            case self::VIEW_EDIT:
            case self::VIEW_CREATE:
            case self::VIEW_INDEX:
                $data = [
                    'categories' => $this->navbarCategoryRepository->pluckCategories(),
                    'productCategoryRepositoryNew' => $this->productCategoryRepositoryNew,
                ];
                break;
        }

        return $data;
    }
}
