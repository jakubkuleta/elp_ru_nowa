<?php

namespace App\Http\Controllers\Traits;

use \Auth;
use \Exception;

trait AbilityTrait
{
    protected function hasAbility($ability, $for = '')
    {
        if ($for) {
            $for = explode('|', $for);
            $controller = $for[0];
            $tab = isset($for[1]) ? $for[1] : null;
        } else {
            $controller = $this->getRouteName();
            $tab = $this->getTabName();
        }

        switch ($ability) {
            case 'index':
            case 'run':
            case 'send':
                $for = $controller;
                break;

            case 'view':
            case 'create':
            case 'update':
            case 'delete':
            case 'approve':
            case 'cancel':
            case 'any':
                $for = $controller.'|'.$tab;
                break;
        }

        return true;
        //return Auth::user()->can($ability, $for);
    }

    protected function checkAbility($ability, $for = '')
    {
        if (!$this->hasAbility($ability, $for)) {
            abort(403);
        }
    }
}