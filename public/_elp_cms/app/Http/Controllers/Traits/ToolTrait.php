<?php

namespace App\Http\Controllers\Traits;
use Request;

trait ToolTrait
{
    protected $titles = array();

    protected function setTitles($list, $edit, $createButton)
    {
        $this->titles = array($list, $edit, $createButton);
    }

    protected function getTitles($key = null)
    {
        $titles = array();
        $titles['index'] = isset($this->titles[0]) ? ucfirst($this->titles[0]) : null;
        $titles['edit'] = isset($this->titles[1]) ? ucfirst($this->titles[1]) : null;
        $titles['createButton'] = isset($this->titles[2]) ? $this->titles[2] : null;

        return $key ? $titles[$key] : (object)$titles;
    }

    protected function redirectToEdit($id)
    {
        $parameters['id'] = $id;

        if ($tabId = $this->getTabId()) {
            $parameters[$this->getTabIdQueryName()] = $tabId;
        }

        return redirect()->route($this->getRouteName().'.edit', $parameters);
    }

    protected function setFlashMessage($message)
    {
        switch ($message) {

            case 'created':
                $flashMessage = 'Rekord został dodany.';
                break;

            case 'updated':
                $flashMessage = 'Rekord został zapisany.';
                break;

            case 'deleted':
                $flashMessage = 'Rekord został usunięty.';
                break;

            case 'approved':
                $flashMessage = 'Rekord został zatwierdzony.';
                break;

            case 'canceled':
                $flashMessage = 'Rekord został anulowany.';
                break;

            default:
                $flashMessage = $message;
        }

        session()->flash('success', $flashMessage);
    }
}


