<?php

namespace App\Http\Controllers\Traits;

trait ExcelTrait
{
    //DO SKONCZENIA!!!!!
    protected function exportToExcel(array $parameters = null)
    {
        //id zestawienia
        $id = array_get($parameters, 'id', 1);

        //tymczasowo
        if ($this->getRouteName() == 'failures') {
            $failures = $this->repository
            ->pushAppliedFilters($this->getAppliedFilters())
            ->setOrderBy('failure_id')
            ->paginate(['limit'=>0]);

            return (new \App\Exports\FailureExport)->download($failures);
        }

        if ($this->getRouteName() == 'scanned-pallets-comparison') {
            $scannedPalletsComparison = $this->repository
            ->pushAppliedFilters($this->getAppliedFilters())
            ->setOrderBy($this->getAppliedOrderBy())
            ->paginate(['limit'=>0]);

            return (new \App\Exports\ScannedPalletComparisonExport)->download($scannedPalletsComparison);
        }

        if ($this->getRouteName() == 'packed-products') {
            $packedProducts = $this->repository
            ->pushAppliedFilters($this->getAppliedFilters())
            ->setOrderBy($this->getAppliedOrderBy())
            ->paginate(['limit'=>0]);

            return (new \App\Exports\PackedProductExport)->download($packedProducts);
        }

        if ($this->getRouteName() == 'inventories') {
            $inventories = $this->repository->allToXls();

            return (new \App\Exports\InventoryExport)->download($inventories);
        }        
    }
}