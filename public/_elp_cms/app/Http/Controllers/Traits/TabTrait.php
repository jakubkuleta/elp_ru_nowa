<?php

namespace App\Http\Controllers\Traits;
use Request;

trait TabTrait
{
    private $tabIdQueryName = '_tab_id';
    private $tabs = array();
    private $tabId = 1;

    /**
     * Dodaje zakladki
     */
    protected function addTab($name, $title = null, array $parameters = null)
    {
        $tabId = $this->tabId;

        $this->tabs[$tabId] = (object)[
            'tab_id' =>  $tabId,
            'current' => $tabId == $this->getTabId() ? true : false,
            'name' => $name,
            'title' => ($title) ? $title : 'Dane podstawowe',
            'relation' => array_get($parameters, 'relation'),
            'refreshPage' => array_get($parameters, 'refreshPage', true),
            'refreshTab' => array_get($parameters, 'refreshTab', false),
            'type' => array_get($parameters, 'refreshTab'),
            'lockApproved' => array_get($parameters, 'refreshTab', true),
            'checkPermission' => array_get($parameters, 'refreshTab', false),
            'url' => array_get($parameters, 'url'),
            'width' => array_get($parameters, 'width'),
            'belongsTo' => array_get($parameters, 'belongsTo'),
            'subForm' => array_get($parameters, 'subForm', false),
        ];

        //jesli zakladka jest w subform to ja ukrywam
        if ($this->tabs[$tabId]->subForm) {
            $this->tabs[$tabId]->belongsTo = 'x';
        }

        ++$this->tabId;
    }

    protected function getTabIdQueryName()
    {
        return $this->tabIdQueryName;
    }
 
    /**
     * Pobiera nr aktywnej zakładki.
     */
    protected function getTabId()
    {
        $tabId = (int) Request::input($this->getTabIdQueryName());

        return ($tabId) ? $tabId : 1;
    }

    protected function getTabs()
    {
        $this->loadTabs();

        return $this->tabs;
    }

    protected function hasTabs()
    {
        $this->loadTabs();

        return count($this->tabs) > 1 ? true : false;
    }

    /**
     * Pobiera parametr aktywnej zakladki.
     */
    protected function getTabParameter($key)
    {
        $this->loadTabs();

        return $this->tabs[$this->getTabId()]->{$key};
    }

    protected function setTabParameter($no, $key, $val)
    {
        $this->loadTabs();
        $this->tabs[$no]->{$key} = $val;
    }
    
    protected function getTabRelation()
    {
        return $this->getTabParameter('relation');
    }

    protected function getTabName()
    {
        return $this->getTabParameter('name');
    }

    /**
     * Zwrwaca nazwe zakladki (z "-") pod warunkiem, ze zdefiniowao co najmniej 2 zakladki
     */
    protected function getTabNameForView()
    {
        return $this->hasTabs() ? '-'.$this->getTabName() : '';
    }

    protected function isTabTable()
    {
        return ($this->getTabParameter('type') == 'table') ? true : false;
    }

    private function loadTabs()
    {
        if (!$this->tabs) {
            if (method_exists($this, 'tabs')) {
                $this->tabs();
            }

            //jesli nie ma zadnego tab, dodaje jeden tab
            if (!$this->tabs) {
                $this->addTab('main');
            }
        }
    }     

}