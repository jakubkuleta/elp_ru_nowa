<?php

namespace App\Http\Controllers;
use Auth;
use Config;

class HomeController extends BaseController
{
    public function index(array $parameters = null)
    {
        if ($homepageId = Auth::user()->homepage_id) {
            $homepage = array_get(Config::get('system.homepages'), $homepageId);
            return redirect()->route($homepage['route']);
        }

        return $this->render('home.index', []);
    }
}
