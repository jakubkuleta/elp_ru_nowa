<?php

namespace App\Http\Controllers;

use App\Repositories\ArticleRepository;

class ArticleController extends BaseController
{

    public function __construct(ArticleRepository $articleRepository)
    {
        $this->setRepository($articleRepository);
        $this->setRequestName('ArticleRequest');
        $this->setTitles('Artykuły', 'artykuł', 'artykuł');
    }

    public function index(array $parameters = null)
    {
        $this->pushFilter('text', ['name' => 'title', 'label'=>'tytuł']);
        return parent::index();
    }

    protected function viewData($view)
    {

        $data = [];
    //wstrzykuje zmienne do widoku przy użyciu wybranych metod (case self)
        switch ($view) {
            case self::VIEW_EDIT:
            case self::VIEW_CREATE:
            case self::VIEW_INDEX:
                $data = [
                ];
                break;
        }

        return $data;
    }
}
