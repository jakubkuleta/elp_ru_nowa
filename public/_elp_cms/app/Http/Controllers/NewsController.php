<?php

namespace App\Http\Controllers;

use App\Repositories\NewsRepository;

class NewsController extends BaseController
{

    public function __construct(NewsRepository $newsRepository)
    {
        $this->setRepository($newsRepository);
        $this->setRequestName('NewsRequest');
        $this->setTitles('Newsy', 'newsy', 'news');
    }

    public function index(array $parameters = null)
    {
        $this->pushFilter('text', ['name' => 'title', 'label'=>'tytuł']);
        return parent::index();
    }

    protected function viewData($view)
    {

        $data = [];
    //wstrzykuje zmienne do widoku przy użyciu wybranych metod (case self)
        switch ($view) {
            case self::VIEW_EDIT:
            case self::VIEW_CREATE:
            case self::VIEW_INDEX:
                $data = [
                    /*'news' => $this->newsRepository->pluck(),*/
                ];
                break;
        }

        return $data;
    }
}
