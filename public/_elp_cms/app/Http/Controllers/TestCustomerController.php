<?php

namespace App\Http\Controllers;

use App\Repositories\TestCustomerRepository;
use App\Http\Requests\TestCustomerRequest;

class TestCustomerController extends BaseController
{
    public function __construct(TestCustomerRepository $testCustomerRepository)
    {
        $this->setRepository($testCustomerRepository);
        $this->setRequestName('TestCustomerRequest');
        $this->setTitles('Klienci', 'klient', 'klienta');
    }

    public function index(array $parameters = null)
    {
        $this->pushFilter('text', ['name' => 'name', 'label'=>'nazwa']);
        $this->pushFilter('text', ['name' => 'street', 'label'=>'ulica']);
        return parent::index();
    }    
}