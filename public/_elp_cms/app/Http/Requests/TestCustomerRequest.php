<?php

namespace App\Http\Requests;

class TestCustomerRequest extends BaseRequest
{ 
    public function rules()
    {
        return [
            'name' => 'required',
            'street' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nazwa',
            'street' => 'ulica'
        ];
    }
}