<?php

namespace App\Http\Requests;

class ProductRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'name' => 'required',
            'symbol'  => 'required',
            'description' => 'required',

        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nazwa',
            'symbol' => 'symbol',
            'description' => 'opis',
        ];
    }
}
