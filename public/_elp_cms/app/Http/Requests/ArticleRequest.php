<?php

namespace App\Http\Requests;

class ArticleRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'title' => 'required',
            'content' => 'required',
            'hide' => 'required',
            'created_at' => 'required|date_format:Y-m-d',
            'publish_from' => 'required|date_format:Y-m-d',
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'tytuł',
            'content' => 'treść',
            'hide' => 'ukryj',
            'created_at' => 'utworzono',
            'publish_from' => 'opublikuj w dniu',
        ];
    }
}
