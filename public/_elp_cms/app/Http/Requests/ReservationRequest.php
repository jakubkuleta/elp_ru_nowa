<?php

namespace App\Http\Requests;

class ReservationRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'item_id' => 'required',
            'description' => 'required',
            'date_start'  => 'required|date_format:Y-m-d|after:yesterday',
            'hour_start' => 'required|before_or_equal:hour_end',
            'hour_end' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'item_id' => 'Przedmiot rezerwacji',
            'date_start' => 'Data start',
            'hour_start' => 'Godzina start',
            'hour_end' => 'Godzina koniec',
            'description' => 'Opis rezerwacji'
        ];
    }
}
