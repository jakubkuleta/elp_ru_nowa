<?php

namespace App\Http\Requests;

use Auth;

class HolidayRequestStoreRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'id_rodzaj_nieobecnosci' => 'required',
            'urlop_od'  => 'required|date_format:Y-m-d|after:yesterday|before_or_equal:urlop_do',
            'urlop_do' => 'required|date_format:Y-m-d',
            'telefon' => $this->has('telefon') ? 'required|integer|digits_between:9,9' : '',
            'zastepstwo' => $this->has('zastepstwo') ? 'required' : '',
        ];
    }

    public function attributes()
    {
        return [
            'id_rodzaj_nieobecnosci' => 'rodzaj urlopu',
            'urlop_od' => 'urlop od',
            'urlop_do' => 'urlop do',
            'zastepstwo' => 'zastępstwo',
        ];
    }

    public function messages()
    {
        return [
            'urlop_od.after' => 'Wartość w polu [:attribute] musi być datą późniejszą lub równą od dzisiaj.',
            'urlop_od.before_or_equal' => 'Wartość w polu [urlop do] musi być datą późniejszą lub równą od pola [urlop od].',
            'telefon.digits_between' => 'Pole [:attribute] musi zawierać 9 cyfr.',
        ];
    }
}