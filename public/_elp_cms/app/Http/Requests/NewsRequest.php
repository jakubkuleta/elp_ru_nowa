<?php

namespace App\Http\Requests;

class NewsRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'title' => 'required',
            'intro' => 'required',
            'hide' => 'required',
            'image' => 'required',
            'created_at' => 'required|date_format:Y-m-d',
            'publish_from' => 'required|date_format:Y-m-d',
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'tytuł',
            'intro' => 'nagłówek',
            'hide' => 'ukryj',
            'image' => 'ikona',
            'created_at' => 'utworzono',
            'publish_from' => 'opublikuj w dniu',
        ];
    }
}
