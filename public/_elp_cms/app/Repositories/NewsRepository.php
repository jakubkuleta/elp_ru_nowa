<?php

namespace App\Repositories;
use \Auth;


class NewsRepository extends BaseRepository
{

    protected $defaultOrderBy = 'news_id DESC';
    protected $searchableColumns = array(
        'like' => ['title']
    );

    protected function model()
    {
        return "App\\Models\News";
    }

}
