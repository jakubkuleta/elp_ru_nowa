<?php

namespace App\Repositories;

use Auth;

class HolidayRequestRepository extends BaseRepository
{    
    protected $defaultOrderBy = 'id DESC';
    protected $searchableColumns = array(
        'like' => ['pracownik'],
        '=' => ['id_rodzaj_nieobecnosci', 'id_status']
    );

    protected function model()
    {
        return "App\\Models\HolidayRequest";
    }

    public function paginate(array $parameters = null)
    {
        $this->pushFilter($this->globalFilter());

        $parameters['query'] = '
        SELECT *
        FROM
        (
            SELECT
            a.*,
            p.nazwa as pracownik,
            s.nazwa as przelozony,
            lr.nazwa as rodzaj_urlopu
            FROM dbo.prac_wnioski_urlopowe as a
            INNER JOIN dbo.prac_pracownicy as p ON a.id_prac = p.id_prac
            INNER JOIN dbo.prac_pracownicy as s ON a.id_prac_przelozony = s.id_prac
            INNER JOIN dbo.prac_l_rodzaje_nieobecnosci as lr ON a.id_rodzaj_nieobecnosci = lr.id_rodzaj_nieobecnosci
        )a
        ';

        return parent::paginate($parameters);
    }

    public function findOrFail($id, $columns = ['*'])
    {
        return $this->model->filter($this->globalFilter())->findOrFail($id, $columns);
    }

    protected function globalFilter()
    {
        $employeeId = Auth::user()->present()->employeeId();
        return '(id_prac = '.$employeeId.' OR id_prac_przelozony = '.$employeeId.')';
    }
}