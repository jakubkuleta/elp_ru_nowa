<?php

namespace App\Repositories;
use \Auth;
use \Exception;
use DB;

class ReservationRepository extends BaseRepository
{
    protected $defaultOrderBy = 'reservation_id DESC';
    protected $searchableColumns = array(
        'like' => ['description'],
    );


    protected function model()
    {
        return "App\\Models\Reservation";
    }

    public function getCurrunt($itemId, $addDays)
    {
        return $this->model->whereRaw('item_id = '.$itemId.' AND CONVERT(DATE, date_start) = CONVERT(DATE, DATEADD(day, '. $addDays .', GETDATE()))')->get();
    }

    public function create(array $attributes)
    {
        $date = $attributes['date_start'];
        $attributes['date_start'] = $date.' '.$attributes['hour_start'];
        $attributes['date_end'] = $date.' '.$attributes['hour_end'];

        if (DB::selectOne("SELECT dbo.rsk_if_date_taken('".$attributes['date_start']."', '".$attributes['date_end']."', ".$attributes['item_id'].") date_taken")->date_taken) {
            throw new Exception('Data się nie zgadza!');
        }

        return parent::create(array_merge($attributes, ['user_id' => Auth::id()]));
    }
}
