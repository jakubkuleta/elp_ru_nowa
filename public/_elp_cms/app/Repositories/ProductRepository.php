<?php

namespace App\Repositories;
use \Auth;


class ProductRepository extends BaseRepository
{
    protected $defaultOrderBy = 'product_id DESC';
    protected $searchableColumns = array(
        '=' => ['symbol', 'category_id'],
        'like' => ['name']
    );

    protected function model()
    {
        return "App\\Models\Product";
    }
}
