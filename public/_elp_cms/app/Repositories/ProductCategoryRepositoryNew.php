<?php

namespace App\Repositories;

use DB;

class ProductCategoryRepositoryNew extends BaseRepository
{
    protected function model()
    {
        return "App\\Models\ProductCategoryNew";
    }

    public function pluckCategoriesForProduct($product_id)
    {
        return $this->model->where('product_id', $product_id)
                           ->pluck('category_id');
    }
}
