<?php

namespace App\Repositories;

class ReservationItemRepository extends BaseRepository
{
    protected function model()
    {
        return "App\\Models\ReservationItem";
    }

    public function pluck()
    {
        return $this->model->pluck('name', 'item_id');
    }
}
