<?php

namespace App\Repositories;
use \Auth;


class ArticleRepository extends BaseRepository
{

    protected $defaultOrderBy = 'article_id DESC';
    protected $searchableColumns = array(
        'like' => ['title']
    );

    protected function model()
    {
        return "App\\Models\Article";
    }

}
