<?php

namespace App\Repositories;

class TestCustomerRepository extends BaseRepository
{    
    protected $defaultOrderBy = 'customer_id DESC';
    protected $searchableColumns = array(
        'like' => ['name', 'street'],
    );

    protected function model()
    {
        return "App\\Models\TestCustomer";
    }
}