<?php

namespace App\Repositories;

use Illuminate\Container\Container as Application;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Pagination\LengthAwarePaginator;
use Config;
use DB;

use App\Repositories\Traits\FilterTrait;
use App\Repositories\Traits\PaginatorTrait;

class BaseRepository
{
    use FilterTrait;
    use PaginatorTrait;

    protected $app;
    protected $model;

    protected $defaultOrderBy;
    protected $primaryOrderBy;
    protected $orderBy;

    public function __construct()
    {
        $this->app = new Application;
        $this->makeModel();
    }

    protected function model()
    {
        return null;
    }

    public function modelInstance()
    {
        return $this->model;
    }

    public function makeModel()
    {
        if ($this->model()) {

            $model = $this->app->make($this->model());
            if (!$model instanceof Model) {
                throw new \Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
            }

            $this->model = $model;

        }
    }

    public function get(array $parameters = null)
    {
        $columns =  ['*'];
        return $this->model->get($columns);
    }

    public function paginate(array $parameters = null)
    {
        $columns =  ['*'];
        $limit = 50;
        $page = LengthAwarePaginator::resolveCurrentPage();
 
        if (array_get($parameters, 'limit') === 0) {
            $limit = 10000;
            $page = 1;
        }

        if ($query =  array_get($parameters, 'query')) {

            $queryForPaginator =  array_get($parameters, 'queryForPaginator', $query); //dla przypadku podania "prostszego SQL"

            if ($filters = $this->getFilters()) {
                $query.= ' WHERE '.$filters['sql'];
                $queryForPaginator.= ' WHERE '.$filters['sql'];
            }

            $items = DB::select($this->getSelectExpressionForPaginator($query, $this->getOrderBy(), $limit, $page), array_get($filters, 'bindings', []));
            $total = DB::selectOne('SELECT COUNT(*) as total FROM ('.$queryForPaginator.')a', array_get($filters, 'bindings', []))->total;
            $results = new LengthAwarePaginator($items, $total, $limit, $page, ['path' => LengthAwarePaginator::resolveCurrentPath()]);

        } else {

            $results = $this->model->filter($this->getFilters())->orderByRaw($this->getOrderBy())->paginate($limit, $columns, 'page', $page);
        }

        $this->resetFilters();

        return $results;
    }

    public function findOrFail($id, $columns = ['*'])
    {
        return $this->model->findOrFail($id, $columns);
    }

    public function find($id, $columns = ['*'])
    {
        return $this->model->find($id, $columns);
    }    

    public function findWhereIn($field, array $values, $columns = ['*'])
    {
        return $this->model->whereIn($field, $values)->get($columns);
    }

    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    public function update(array $attributes, $id)
    {
        $model = $this->findOrFail($id);
        $model->fill($attributes);
        return $model->save();
    }

    public function delete($id)
    {
        $model = $this->findOrFail($id);
        $model->delete();
    }

    public function pluck()
    {
        $args = func_get_args();
        
        $column = $args[0];
        $key = $args[1];
        $order = isset($args[2]) ? $args[2] : $column;
        $parameters = isset($args[3]) ? $args[3] : null;

        return $this->model->orderByRaw($order)->pluck($column, $key);
    }

    public function getDefaultOrderBy()
    {
        return $this->defaultOrderBy;
    }

    public function setOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;
        return $this;
    }

    protected function getOrderBy()
    {
        $defaultOrderBy = $this->defaultOrderBy ? $this->defaultOrderBy : $this->model->getPrimaryKey();
        $orderBy = $this->orderBy ? $this->orderBy : $defaultOrderBy; 

        //dodaje sortowanie "unikatowe" np. ORDER BY name, user_id;
        $primaryOrderBy = $this->primaryOrderBy ? $this->primaryOrderBy : $this->model->getPrimaryKey();
        if (!in_array(trim(str_replace(['DESC', 'ASC'], '', $primaryOrderBy)), explode(' ', $orderBy))) {
            $orderBy.= ', '.$primaryOrderBy;
        }

        return $orderBy;
    }

    protected function getDriver()
    {
        return Config::get('database.connections.'.Config::get('database.default').'.driver');
    }    
}
