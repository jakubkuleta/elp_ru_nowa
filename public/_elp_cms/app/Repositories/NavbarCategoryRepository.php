<?php

namespace App\Repositories;

class NavbarCategoryRepository extends BaseRepository
{
    protected function model()
    {
        return "App\\Models\NavbarCategory";
    }

    public function pluckCategories()
    {
        return $this->model->where('depth_level', '<>', 0)
                           ->where('route_name', 'products.index')
                           ->pluck('name', 'category_id');
    }
}
