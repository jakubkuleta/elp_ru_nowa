<?php

namespace App\Models;

class ProductCategory extends BaseModel
{
    protected $table = 'pr_categories';
    protected $primaryKey = 'category_id';
}
