<?php

namespace App\Models;

class Product extends BaseModel
{
    protected $table = 'pr_products';
    protected $primaryKey = 'product_id';

    protected $fillable = [
        'name',
        'description',
        'symbol',
        'sort',
        'hide',
        'img_path',
        'img_filename',
        'metatitle',
        'metadescription'
    ];

    public function category()
    {
        return $this->belongsTo('App\Models\ProductCategory', 'category_id', 'category_id');
    }

    public function categories()
    {
        return $this->hasMany('App\Models\ProductCategoryNew', 'product_id', 'product_id');
    }
}
