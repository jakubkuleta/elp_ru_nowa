<?php

namespace App\Models;

class SalesRepresentative extends BaseModel
{
    protected $table = 'uz_handlowcy';
    protected $primaryKey = 'id_uz';
}