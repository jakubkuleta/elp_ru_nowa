<?php

namespace App\Models;

class News extends BaseModel
{
    protected $table = 'ot_news';
    protected $primaryKey = 'news_id';

    protected $fillable = [
        'title',
        'intro',
        'hide',
        'content',
        'created_at',
        'image',
        'publish_from',
        'metadescription'
    ];

    public function getCreatedAtAttribute($value)
    {
        return $this->attributes['created_at'] = substr($value, 0, 10);
    }

    public function getPublishFromAttribute($value)
    {
        return $this->attributes['publish_from'] = substr($value, 0, 10);
    }

}
