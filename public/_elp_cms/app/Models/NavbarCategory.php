<?php

namespace App\Models;

class NavbarCategory extends BaseModel
{
    protected $table = 'navbar_categories';
    protected $primaryKey = 'category_id';
}
