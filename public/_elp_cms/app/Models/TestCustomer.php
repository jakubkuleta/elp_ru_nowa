<?php

namespace App\Models;

class TestCustomer extends BaseModel
{
    protected $table = 'test_customers';
    protected $primaryKey = 'customer_id'; 

    protected $fillable = [
        'name',
        'street'
    ];
}