<?php


namespace App\Models;

class Reservation extends BaseModel
{
    protected $table = 'rsk_reservation';
    protected $primaryKey = 'reservation_id';

    protected $fillable = [
        'date_start',
        'date_end',
        'user_id',
        'item_id',
        'description'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id_uz');
    }

    public function item()
    {
        return $this->belongsTo('App\Models\ReservationItem', 'item_id', 'item_id');
    }

}
