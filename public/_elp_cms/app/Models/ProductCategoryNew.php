<?php

namespace App\Models;

class ProductCategoryNew extends BaseModel
{
    protected $table = 'navbar_categories_products';
    protected $primaryKey = 'id';

    protected $fillable = [
            'product_id',
            'category_id'];
}
