<?php

namespace App\Models;

class Article extends BaseModel
{
    protected $table = 'ot_articles';
    protected $primaryKey = 'article_id';

    protected $fillable = [
        'title',
        'href',
        'hide',
        'content',
        'created_at',
        'publish_from',
    ];

    public function getCreatedAtAttribute($value)
    {
        return $this->attributes['created_at'] = substr($value, 0, 10);
    }

    public function getPublishFromAttribute($value)
    {
        return $this->attributes['publish_from'] = substr($value, 0, 10);
    }

}
