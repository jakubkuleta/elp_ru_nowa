<?php

namespace App\Models;

class ReservationItem extends BaseModel
{
    protected $table = 'rsk_reservation_item';
    protected $primaryKey = 'item_id';
}
