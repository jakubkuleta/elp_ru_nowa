<?php

namespace App\Models;

use Laracasts\Presenter\PresentableTrait;

class User extends BaseModel
{
    use PresentableTrait;

    protected $table = 'uz_uzytkownicy_www';
    protected $primaryKey = 'id_uz'; 

    protected $presenter = 'App\Presenters\UserPresenter';
    
    public function jobTitle()
    {
        return $this->belongsTo('App\Models\UserJobTitle', 'id_stanowisko', 'id_stanowisko');
    }
}