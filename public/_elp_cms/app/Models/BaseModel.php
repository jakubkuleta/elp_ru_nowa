<?php

namespace App\Models;

use \Auth;

class BaseModel extends \Eloquent
{
    public $timestamps = false;

    public static function getUserId()
    {
        return Auth::id();
    }

    public function scopeFilter($query, $sql = null, array $bindings = null)
    {
        if (is_array($sql)) {
            $bindings = array_get($sql, 'bindings');
            $sql = array_get($sql, 'sql');
        }

        if ($sql) {
            $query->whereRaw($sql, (array) $bindings);
        }

        return $query;
    }

    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }
}
