<?php

namespace App\Helpers;

use Request;

class TableHelper
{
    private static $_tempalate; //dla uprawnien
    public static $appliedOrderBy = array();

    private $tempalate;
    private $tableTempalates = array(
        'list' => array(
            'open' => '<table class="adm-table adm-table-hlr adm-table-alc[class]"[style]>',
        ),
        'edit' => array(
            'open' => '<table class="adm-table adm-table-100 adm-table-rwd[class]"[style]>',
            'td1Class' => 'wh120',
            'td2Class' => 'wh750',
        ),
        'edit-list' => array(
            'open' => '<table class="adm-table adm-table-hlr adm-table-alc[class]"[style]>',
            'editButton' => true,
            'deleteButton' => true,
            'addButton' => true,
        ),
    );

    private $data; //dane tabeli
    private $primaryKey;
    private $item; //dane wiersza

    private $caption = array();
    private $rawCaption;

    private $tr = array(); //tablica wierszy (dla edit)
    private $trClass = array(); //tablica class wierszy
    private $td1Class = array(); //tablica class komorki1
    private $td2Class = array(); //tablica class komorki2
    private $td1Style = array();
    private $td2Style = array();
    private $th = array(); //tablica naglowkow
    private $thSortable = array(); //tablica naglowkow do sortowania
    private $isThSortable = false; //czy zdefiniowano co najmniej jedna kolumne do sortowania?

    private $tf = array(); //tablica stopek
    private $td = array(); //tablica komorek
    private $formField = array();
    private $flag = false;  //przy pierwszym tr ustawiam na true, zeby nie dodawac do $tdNew

    private $rowId;
    private $rowNo = 0;
    private $column;

    private $additionalLinkParam;

    private $closeTable = true;
    private $canSave = false;
    private $canDelete = false;
    private $canAdd = false;

    private $controllerAction;

    private $tab = 0;
    private $subForm; //"podformularz" w formularzu glownym (wysle przez POST tylko pola w podanej klasie)

    //private $tableWidth = 0;

    public function __construct($tempalate, array $param = null)
    {
        $this->tempalate = $tempalate;
        self::$_tempalate = $tempalate;

        $this->controllerAction = ''; //ZMIANA: Utils::getControllerAction();

        if (isset($param['additionalLinkParam'])) {
            $this->additionalLinkParam = $param['additionalLinkParam'];
        }

        if (isset($param['td1Class'])) {
            $this->setConf('td1Class', $param['td1Class']);
        }
        if (isset($param['td2Class'])) {
            $this->setConf('td2Class', $param['td2Class']);
        }
        if (isset($param['open'])) {
            $this->setConf('open', $param['open']);
        }
        if (isset($param['tableStyle'])) {
            $this->setConf('tableStyle', $param['tableStyle']);
        }
        if (isset($param['tableClass'])) {
            $this->setConf('tableClass', $param['tableClass']);
        }
        if (isset($param['frozen'])) {
            $this->setConf('frozen', $param['frozen']);
        }

        if (isset($param['hideTh'])) {
            $this->setConf('hideTh', $param['hideTh']);
        }

        $this->setConf('thHeightClass', 'he40');
        if (isset($param['thHeightClass'])) {
            $this->setConf('thHeightClass', $param['thHeightClass']);
        }

        if (isset($param['thEdit'])) {
            $this->setConf('thEdit', $param['thEdit']);
        }

        if (isset($param['addButton'])) {
            $this->setConf('addButton', $param['addButton']);
        }

        if (isset($param['readOnly'])) {
            $this->setConf('editButton', false);
            $this->setConf('deleteButton', false);
            $this->setConf('addButton', false);
        }

        if (isset($param['closeTable'])) {
            $this->closeTable = $param['closeTable'];
        }

        //subform + ustawienei tab
        //(musi byc przed ustawieniem uprawnien)
        if (isset($param['subForm'])) {
            $this->subForm = $param['subForm'];
            $this->setTabByName($this->subForm);
        }

        $this->canSave = $this->hasAbility('update');
        $this->canDelete = $this->hasAbility('destroy');
        $this->canAdd = $this->hasAbility('store');

        //nadpisanie uprawnien
        if (isset($param['canAdd'])) {
            $this->canAdd = $param['canAdd'];
        }

        if (isset($param['canDelete'])) {
            $this->canDelete = $param['canDelete'];
        }

        if (isset($param['canEdit'])) {
            $this->canSave = $param['canEdit'];
        }
    }

    public function setConf($key, $val)
    {
        $this->tableTempalates[$this->tempalate][$key] = $val;
    }

    private function getConf($key)
    {
        return isset($this->tableTempalates[$this->tempalate][$key]) ? $this->tableTempalates[$this->tempalate][$key] : null;
    }

    public static function open($template = null)
    {
        return '<table class="adm-table adm-table-hlr adm-table-alc">';
    }

    public static function close(array $attributes = null)
    {
        return '</table>';
    }

    private function open2(array $attributes = null)
    {
        $template = $this->getConf('open');

        $style = $this->getConf('tableStyle');
        if ($style) {
            $style = ' style="'.$style.'"';
        }

        $class = $this->getConf('tableClass');
        if ($class) {
            $class = ' '.$class;
        }

        $template = str_replace('[style]', $style, $template);
        $template = str_replace('[class]', $class, $template);

        return $template;
    }

    private function close2(array $attributes = null)
    {
        return ($this->closeTable) ? '</table>' : '';
    }

    public function render()
    {
        $html = '';

        switch ($this->tempalate) {
            case 'edit-list':
                $html .= $this->open2();
                $html .= $this->rawCaption;
                $html .= $this->tr('caption');

                if (!$this->getConf('hideTh')) {
                    $html .= $this->tr('th');
                }

                $html .= $this->tr('main');
                $html .= $this->tr('new');
                $html .= $this->tr('own');
                $html .= $this->close2();
                break;

            case 'edit':
                $html .= $this->open2();
                $html .= $this->tr();
                $html .= $this->close2();
                break;

            case 'list-caption': //sam naglowek (bez table) - wykorzystywane w list.phtml
                $html .= $this->tr('caption');
                $html .= $this->tr('th');
                if ($this->isThSortable) {
                    $html .= $this->tr('thSortable');
                }
                break;

            case 'list':
                $html .= $this->open2();
                $html .= $this->tr('caption');
                $html .= $this->tr('th');
                $html .= $this->close2();
                break;

            case 'list-summary':
                $html .= $this->tr('tf');
                $html .= $this->tr('own');
                break;

        }

        if ($this->getConf('frozen')) {
            //jesli ma uprawnienia do edycji lub usuwania lub dodawania - to przyciski sa fixed
            $class = (self::hasAbility('update') || self::hasAbility('destroy') || self::hasAbility('store')) ? ' btn-fixed' : '';
            $html = '<div class="table-frozen '.$class.'"><div class="table-frozen-div">'.$html;

            //jesli tabeli nie zamykam to zamkniecie div musze zrobic "recznie"
            if ($this->closeTable) {
                $html .= '</div></div>';
            }

            $html .= "<script>
			$(document).ready(function()
			{
				var trHeight;
				var isFF = $.browser.mozilla;
				
				var inputWidth;
				var inputPaddingLeft;
				var inputPaddingRight;
			
				/*dostosowanie wysokosci guzikow frozen*/
				$('.table-frozen-div table > tbody  > tr').each(function() { /*petla po wierszach tabeli*/
					trHeight = $(this).find('td:first-child').height(); /*odczytuje wysokosc pierwszej komorki*/
					if (!isFF) trHeight = trHeight + 1;
			
					if (isFF) {
						$(this).find('.btn, .caption-btn, .summary-btn').addClass('mt'); /*dodaje klase mr (margin-top dla FF)*/
					}
					
					/* ustawia wyokosc td edit, del, add - na wysokosc biezacego wiesza*/
					$(this).find('.btn, .caption-btn, .summary-btn').css('height', trHeight);
					
					/*centruje w pionie guziki*/
					$(this).find('.btn  a').css('marginTop', (trHeight / 2)-7); /*14/2 = 7 - wysokosc guzika*/
				});
				
				/*petla po inputach text - skracam je, zeby nie rozciagaly kolumn (poniewz bez uprawnien dodawania (bez pol na dole) tabela bez skracania jest wezsza)*/
				$('.table-frozen-div table input[type=text]').each(function() {
					inputWidth = $(this).css('width');
					inputPaddingLeft = $(this).css('padding-left');
					inputPaddingRight = $(this).css('padding-right');
					$(this).css('width', parseInt(inputWidth, 10) - parseInt(inputPaddingLeft, 10) - parseInt(inputPaddingRight, 10) - 2);
					
				});				
			});
			</script>";
        }

        return $html;
    }

    public function setTab($tab)
    {
        $this->tab = $tab;
    }

    /**
     * Ustawia $tab na postawie przekazanej nazwy
     */
    public function setTabByName($name)
    {
        $tabs = Utils::getData('tabs');
        foreach ($tabs as $tab => $item) {
            if ($item['name'] == $name) {
                $this->setTab($tab);
                break;
            }
        }
    }

    /**
     * Ustawia dane dla calej tabeli.
     */
    public function setData($data, $primaryKey)
    {
        $this->data = $data;
        $this->primaryKey = $primaryKey;
    }

    private function getItemId()
    {
        if ($this->item) {
            $id = $this->item->{$this->primaryKey};
            if (!$id) {
                throw new \Exception('Brak wartosci pola ID');
            }

            return $id;
        } else {
            return false;
        }
    }

    /**
     * Ustawia dane dla wiersza (tr)
     * Dodakowo ustawiam id rekordu w edycji.
     */
    public function setItem($item)
    {
        $this->item = $item;
        $this->rowId = Utils::getData('rowId');
    }

    /**
     * Dodaje naglowki tabeli (szablon: edit-list).
     */
    public function addTh($class, $title, $required = false, $colspan = '', array $param = null)
    {
        $value = '';
        $style = '';

        //przekazanie stylu zamiast klasy
        if (substr($class, 0, 5) == 'width') {
            $style = ' style = "'.$class.'"';
            $class = '';
        }

        //pierwszy naglowek
        if (!count($this->th)) {
            $class =  $this->getConf('thHeightClass').' '.$class;
        }

        $required = ($required) ? '<span class="cRed">* </span> ' : '';
        $colspan = ($colspan) ? ' colspan="'.$colspan.'"' : '';

        //$this->calculateTableWidth($class);
        //komorka zwykla
        $class = trim('caption '.$class);
        $this->th[] = '<th class="'.$class.'"'.$colspan.$style.'>'.$required.$title.'</th>';

        //komorka do sortowania
        if (isset($param['orderBy'])) {
            $orderBy = $param['orderBy'];
            $orderByGet = self::$appliedOrderBy;

            if ($orderBy) {
                $sortClass1 = '';
                if ($orderByGet == $orderBy) {
                    $sortClass1 = ' current';
                }
                $sortClass2 = '';
                if ($orderByGet == $orderBy.' DESC') {
                    $sortClass2 = ' current';
                }
                $value = '<a class="sort-asc'.$sortClass1.'" href="'.Request::fullUrlWithQuery(array('orderBy' => $orderBy, 'page' => null)).'"></a><a class="sort-desc'.$sortClass2.'" href="'.Request::fullUrlWithQuery(array('orderBy' => $orderBy.' DESC', 'page' => null)).'"></a>';
                $this->isThSortable = true; //rejestruje, ze jest komorka sortowania
            }
        }

        $this->thSortable[] = '<td class="caption sort"'.$colspan.'>'.$value.'</td>';
    }

    /**
     * Dodaje naglowki "sortujace" tabeli (szablon: edit-list).
     */
    public function addThSortable($class, $title, $orderBy = null, array $param = null)
    {
        //colspan i required przekazuje przez parametry
        $colspan = ($param['colspan']) ? $param['colspan'] : '';

        $this->addTh($class, $title, false, $colspan, array('orderBy' => $orderBy));
    }

    /**
     * Dodaje komorki tabeli (szablon: edit-list, list).
     */
    public function addTd($class, $filed, array $param = null)
    {
        $this->td[] = array(
            'class' => $class, /*klasa dla kazdego TR*/
            'editClass' => isset($param['editClass']) ? $param['editClass'] : null, /*klasa komorki dla rekordu EDIT/ADD - ale tylko jak jest pole*/
            'addClass' => isset($param['addClass']) ? $param['addClass'] : null, /*klasa komorki dla nowego rekodu (niezaleznie czy jest pole czy nie)*/
            'filed' => $filed, /*nie wiem???*/
            'selectColumn' => isset($param['selectColumn']) ? $param['selectColumn'] : null, /*to ustawia sie dla komorki, ktora edytowana jest przez pole SELECT (wyswietla wartosc z pola tabeli, a nie z ARR selecta - zapobiega pustej komorce, jesli na liscie ARR select nie ma wartosci zapisanej w tabeli)*/
            'fnValue' => isset($param['fnValue']) ? $param['fnValue'] : null, /*przetwarza wartosc komorki i pola*/
            'fnTdValue' => isset($param['fnTdValue']) ? $param['fnTdValue'] : null, /*to samo co wyzej ale tylko dla wartosci komorki*/
            'funcLink' => isset($param['funcLink']) ? $param['funcLink'] : null, /*to jest do poprawienia*/
            'funcContent' => isset($param['funcContent']) ? $param['funcContent'] : null, /*przetwarza wartosc komorki, ale wykorzystuje caly $item*/
            'fnItem' => isset($param['fnItem']) ? $param['fnItem'] : null,
            'funcCellClass' => isset($param['funcCellClass']) ? $param['funcCellClass'] : null,
        );
    }

    /**
     * Dodaje komorki tabeli (szablon: list-summary).
     */
    public function addTf($value, $colspan = '', array $parameters = null)
    {
        $colspan = ($colspan) ? ' colspan="'.$colspan.'"' : '';

        $sumCell = false;
        $showSummary = (int) Input::get('summary');

        //suma z linkiem pokaz / ukryj
        if ($value == 'sum') {
            $sumCell = true;

            $show = ($showSummary) ? 0 : 1;
            $text = ($showSummary) ? 'ukryj podsumowanie' : 'pokaż podsumowanie';
            $url = Request::fullUrlWithQuery(['summary'=>$show]);

            $value = '<a href="'.$url.'" style="padding-right: 10px">'.$text.'</a>';
            $value .= '<strong>Suma:</strong>';
        }

        //jesli podsumowanie ukryte
        if ((string) $value != '' && !$sumCell && !$showSummary) {
            $value = '-';
        }

        $class = array_get($parameters, 'class');
        if ($class) {
            $class = ' '.$class;
        }
        $this->tf[] = '<td class="alr'.$class.'"'.$colspan.'>'.$value.'</td>';
    }

    /**
     * Dodaje wiersze tabeli (szablon: edit).
     */
    public function addTr($td1, $td2 = null, array $param = null)
    {
        $class = '';
        $saveButton = '';
        $deleteButton = '';

        if (is_array($td1)) {
            $paramSave = isset($param['save']) ? $param['save'] : null;
            $paramDelete = isset($param['delete']) ? $param['delete'] : null;

            //jesli drugi parametr jest arr
            if (is_array($td2)) {
                $param = $td2;
            }

            if (in_array('save', $td1)) {
                $saveButton = $this->saveButton2($paramSave);
            }

            if (in_array('delete', $td1)) {
                $deleteButton = $this->deleteButton($paramDelete);
            }

            $td1 = '<td colspan="2" class="bg1 alr">'.$deleteButton.$saveButton.'</td>';
            $td2 = null;
        } else {
            if ($td1 == 'summary') {
                $class = 'summary';
                $td1 = $td2; //przepisanie z powrotem do td1
                $td2 = null;
            }

            if (isset($param['trClass'])) {
                $class = $param['trClass'];
            }
        }

        $this->tr[] = array($td1, $td2);
        $this->trClass[] = $class;
        $this->td1Class[] = isset($param['td1Class']) ? $param['td1Class'] : '';
        $this->td2Class[] = isset($param['td2Class']) ? $param['td2Class'] : '';
        $this->td1Style[] = isset($param['td1Style']) ? $param['td1Style'] : '';
        $this->td2Style[] = isset($param['td2Style']) ? $param['td2Style'] : '';
    }

    public function addTrColspan($colspan, $class, $content, array $param = null)
    {
        $td = '<td colspan="'.$colspan.'" class="'.$class.'">'.$content.'</td>';
        $this->tr[] = array($td, null);

        if ($param['trClass']) {
            $class = $param['trClass'];
        }
        $this->trClass[] = $class;
        $this->td1Class[] = null;
        $this->td2Class[] = null;
        $this->td1Style[] = null;
        $this->td2Style[] = null;
    }

    /**
     * Przyciski dla widoku "type=table" (ale widok jest jako "edit")
     * przeroboc, ze jak bedzie $rowId === null to bedzie to dla "type<>table"
     */
     public function addTrWithButtons($id, $rowId)
     {
         $value = ($rowId) ? 'Zapisz zmiany' : 'Dodaj';

         $deleteButton =  $id && $rowId ? $this->deleteButton(array('query'=>'?_tab='.self::getCurrentTab().'&_row_id='.$rowId)) : '';

         $ability = $rowId ? 'update' : 'store';
         $hasAbility = $this->hasAbility($ability, false);
         $saveButton = $hasAbility ? $this->saveButton(array('value'=>$value)) : AbilityHelper::cannotMessage($ability);

         $this->addTrColspan(2, 'bg-summary alr', $deleteButton.$saveButton);
     }

    /**
     * Dodaje naglowek (szablon: edit-list).
     */
    public function addCaption($class, $content)
    {
        $this->caption[] = '<td colspan="100%" class="bg1 '.$class.'" style="padding:5px">'.$content.'</td>';
    }

    /**
     * Dodaje tr z podanym kodem HTML (szablon: edit-list).
     */
    public function addRawCaption($html)
    {
        $this->rawCaption = $html;
    }

    /**
     * Dodaje stopke tabeli.
     */
    public static function addFooter($class, $content)
    {
        return '<td colspan="100%" class="bg1 '.$class.'">'.$content.'</td>';
    }

    /**
     * Dodaje pole (szablon: edit-list).
     */
    public function addText($name, array $attributes = null, $maxlength = null, array $param = null)
    {
        $column = $this->getColumnNo((array) $param);

        $this->formField[$column] = array(
        'type' => 'text',
        'name' => $name,
        'attributes' => $attributes,
        'maxlength' => ($maxlength) ? $maxlength : 50,
        'defualtValue' => isset($param['defualtValue']) ? $param['defualtValue'] : null,
        'fnContentAfter' => isset($param['fnContentAfter']) ? $param['fnContentAfter'] : null,
        'fnContentBefore' => isset($param['fnContentBefore']) ? $param['fnContentBefore'] : null,
        'funcLock' => isset($param['funcLock']) ? $param['funcLock'] : null,
        );
    }

    /**
     * Dodaje pole (szablon: edit-list).
     */
    public function addTextarea($name, array $attributes = null, array $param = null)
    {
        $column = $this->getColumnNo((array) $param);

        $this->formField[$column] = array(
            'type' => 'textarea',
            'name' => $name,
            'attributes' => $attributes,
            'fnContentAfter' => isset($param['fnContentAfter']) ? $param['fnContentAfter'] : null,
            'fnContentBefore' => isset($param['fnContentBefore']) ? $param['fnContentBefore'] : null,
            'funcLock' => isset($param['funcLock']) ? $param['funcLock'] : null,
        );
    }

    /**
     * Dodaje pole (szablon: edit-list).
     */
    public function addSelect($name, array $options = null, array $attributes = null, array $param = null)
    {
        $column = $this->getColumnNo((array) $param);

        if (Arr::isStd($options)) {
            $options = Arr::std2Arr($options);
        } else {
            $options = Arr::addEmpty($options);
        }

        $this->formField[$column] = array(
            'type' => 'select',
            'name' => $name,
            'attributes' => $attributes,
            'options' => $options,
            'funcOptions' => isset($param['funcOptions']) ? $param['funcOptions'] : null,
            'funcLock' => isset($param['funcLock']) ? $param['funcLock'] : null,
            'defualtValue' => isset($param['defualtValue']) ? $param['defualtValue'] : null,
            'fnContentAfter' => isset($param['fnContentAfter']) ? $param['fnContentAfter'] : null,
            'fnContentBefore' => isset($param['fnContentBefore']) ? $param['fnContentBefore'] : null,
        );
    }

    /**
     * Dodaje pole (szablon: edit-list).
     */
    public function addCheckbox($name, $value = null, array $attributes = null, array $param = null)
    {
        $column = $this->getColumnNo((array) $param);

        $this->formField[$column] = array(
            'type' => 'checkbox',
            'name' => $name,
            'attributes' => $attributes,
            'checkboxValue' => $value,
            'funcLock' => isset($param['funcLock']) ? $param['funcLock'] : null,
        );
    }

    /**
     * Zwrca pole tekstowe, select, .... (szablon: edit-list).
     */
    private function input($originalValue, $value, $newRecord)
    {
        $data = $this->formField[$this->column];
        $name = $this->getInputName($data['name']);
        $attributes = isset($data['attributes']) ? $data['attributes'] : null;
        $maxlength = isset($data['maxlength']) ? $data['maxlength'] : null;
        $options = isset($data['options']) ? $data['options'] : null;
        $defualtValue = isset($data['defualtValue']) ? $data['defualtValue'] : null;
        $checkboxValue = isset($data['checkboxValue']) ? $data['checkboxValue'] : null;

        //wartosc domyslna
        if ($newRecord) {

            //licznik rekordow
            if ($defualtValue == 'row-no') {
                $defualtValue = $this->rowNo + 1;
            }

            $value = $defualtValue;
        }

        //subform - dodanie dodatkowej klasy dla js
        if ($this->subForm) {
            $attributes['class'] = $attributes['class'].' subform';
        }

        //blokada pola
        if (isset($data['funcLock']) && $data['funcLock']) {
            $func = $data['funcLock'];
            $lock = $func($this->item);

            //jesli blokada
            if ($lock) {
                switch ($data['type']) {
                    case 'text':
                    case 'checkbox':
                        $attributes['class'] = $attributes['class'].' readonly';
                        break;

                    case 'select': //select
                        $data['type'] = 'hidden_select';
                        break;
                }
            }
        }

        //content przed polem
        $contentBefore = '';
        if (isset($data['fnContentBefore']) && $data['fnContentBefore']) {
            $func = $data['fnContentBefore'];
            $contentBefore = $func($this->item);
        }

        //content po polu
        $contentAfter = '';
        if (isset($data['fnContentAfter']) && $data['fnContentAfter']) {
            $func = $data['fnContentAfter'];
            $contentAfter = $func($this->item);
        }

        switch ($data['type']) {
            case 'text':
                return $contentBefore.Form::text($name,  $value, array_merge($attributes, array('maxlength' => $maxlength))).$contentAfter;
                break;

            case 'textarea':
                return $contentBefore.Form::textarea($name, $value, $attributes).$contentAfter;
                break;

            case 'select':
                $options = $this->funcOptions($data, $options, $value, $this->item);

                return $contentBefore.Form::select($name, $options, $value, $attributes).$contentAfter;
                break;

            case 'checkbox':
                return Form::checkboxWithHidden($name, $checkboxValue, (bool)$originalValue, $attributes);
                break;

            case 'hidden_select':
                $attributes['class'] .= ' readonly'; //dodaje klase readonly

                if (is_array($options[$value])) {
                    $inputValue = $options[$value]['name'];
                } else {
                    $inputValue = $options[$value];
                }

                return Form::hidden($name, $value).''.Form::text(null, $inputValue, $attributes, 100);
                break;
        }
    }

    /**
     * Dodaje przedrostek do nazwy pola, jesli ustawiono subForm
     */
    private function getInputName($name)
    {
        if ($this->subForm) {
            $name = '_'.$this->subForm.'-'.$name;
        }

        return $name;
    }

    /**
     * Zwrca wiersze tabeli (zalezne od typu tabeli).
     */
    public function tr($param = null)
    {
        switch ($this->tempalate) {
            case 'list':
            case 'edit-list':
            case 'list-caption':
            case 'list-summary':

                return $this->getTrTempalateEditList($param);
                break;

            case 'edit':
                return $this->getTrTempalateEdit();
                break;
        }
    }

    /**
     * Zwrca wiersze tabeli (szablon: edit).
     */
    private function getTrTempalateEdit()
    {
        $html = '';

        $class = $this->getConf('td2Class');
        $style = '';

        //petla po tablicy wierszy
        foreach ($this->tr as $key => $tr) {
            $trClass = ($this->trClass[$key]) ? ' class="'.$this->trClass[$key].'"' : '';
            $html .= '<tr'.$trClass.'>';

            //zdefiniowane klase komorki 2
            $tdClass = $class;
            if (isset($this->td2Class[$key]) && $this->td2Class[$key]) {
                $tdClass = $tdClass.' '.$this->td2Class[$key];
            }

            //zdefiniowane style komorki 2
            $tdStyle = $style;
            if (isset($this->td2Style[$key]) && $this->td2Style[$key]) {
                $tdStyle = $tdStyle.' '.$this->td2Style[$key];
            }
            $tdStyle = trim($tdStyle);
            if ($tdStyle) {
                $tdStyle = ' style="'.$tdStyle.'"';
            }

            //potela po komorkach w danym elemencie wiersza
            foreach ($tr as $td) {

                //jesli jest zdefiniwoana komorka i nie zaczyna sie od <td
                if ($td !== null && substr($td, 0, 3) != '<td') {
                    //echo $class;
                    $td = '<td class="'.$tdClass.'"'.$tdStyle.'>'.$td.'</td>';
                }

                $html .= $td;
            }
            $html .= '</tr>';
        }

        return $html;
    }

    /**
     * Zwrca wiersze tabeli (szablon: edit-list, list).
     */
    private function getTrTempalateEditList($element)
    {
        $html = '';

        switch ($element) {
            case 'caption':
                if (count($this->caption)) {
                    $html = '<tr>'.implode('', $this->caption).'</tr>';
                }
                break;

            case 'th':
                $this->th[] = $this->thEdit(); //to jest naglowek dla guzikow edit, save
                $html = '<tr class="tr-caption">'.implode('', $this->th).'</tr>';
                break;

            case 'thSortable':
                $html = '<tr class="tr-caption">'.implode('', $this->thSortable).'</tr>';
                break;

            case 'tf':
                $html = '<tr class="summary">'.implode('', $this->tf).'</tr>';
                break;

            case 'main':
                //petla po danych
                if ($this->data) {
                    foreach ($this->data as $item) {
                        ++$this->rowNo;
                        $this->setItem($item);
                        $html .= $this->getTrTempalateEditList2();
                    }
                }
                break;

            //nowy wierszy tylko jak inny nie jest w edycji	i ma uprawnienia
            case 'new':
                if (!$this->rowId && $this->canAdd && $this->getConf('addButton')) {
                    $this->setItem(null);
                    $html .= $this->getTrTempalateEditList2(true);
                }
                break;

            //wlasny jak w szablon: edit
            case 'own':
                    $html .= $this->getTrTempalateEdit();
                break;
        }

        return $html;
    }

    /**
     * Pomocnicza dla getTrTempalateEditList.
     */
    private function getTrTempalateEditList2($newRecord = false)
    {
        $trClass = '';
        $tdContent = '';

        //jesli rekord w edycji lub nowy rekord
        if (($this->getItemId() > 0 && $this->rowId == $this->getItemId()) || $newRecord) {
            $trClass = (!$newRecord) ? 'bg-edit' : 'bg-add';
            $trClass = ' class="'.$trClass.'"';
        }

        $html = '<tr'.$trClass.'>';

        //petla po komorkach
        foreach ($this->td as $column => $td) {
            if (!$td['filed']) {
                $td['filed'] = '.';
            } //nie podano nazwy pola w $table->addTd

            $hasfnItem = false; //czy okreslono fnItem

            $this->column = $column;

            //jesli rekord w edycji, lub nowy rekord i jest zdefiniowane pole
            if (($this->rowId == $this->getItemId() || $newRecord) && isset($this->formField[$this->column])) {
                $class = $td['editClass'];

                if (!$newRecord) {
                    $originalValue = ($this->item) ? $this->item->{$td['filed']} : null;
                } else {
                    $originalValue = null;
                }

                //funkcja formatujaca wartosc (tylko dla nienowego wiersza)
                if ($newRecord) {
                    $displayedValue = $originalValue;
                } else {
                    $displayedValue = $this->fnValue($td, $originalValue);
                }

                $tdContent = $this->input($originalValue, $displayedValue, $newRecord);
            } else {
                $class = $td['class'];

                //domyslnie - nazwa pola wspolna dla listy i edycji
                if ($this->item) {
                    $tdContent = $this->item->{$td['filed']};
                }

                //jesli pole edycji jest selectem (pobieram wartosc z Arr)
                if (isset($this->formField[$column]) && $this->formField[$column]['type'] == 'select') {
                    $arrValue = isset($this->formField[$column]['options'][$this->item->{$td['filed']}]) ? $this->formField[$column]['options'][$this->item->{$td['filed']}] : null;

                    //jesli miniselect
                    if (is_array($arrValue)) {
                        $arrValue = $arrValue['name'];
                    }
                    $tdContent = $arrValue;
                }

                //jesli inne pole w liscie (niz w edycji)
                if ($td['selectColumn']) {
                    $tdContent = $this->item->{$td['selectColumn']};
                }

                //funkcja formatujaca cala zawartosc komorki (operuje na $item)
                if ($this->item) {
                    $hasfnItem = $td['fnItem'] ? true : false;
                    $tdContent = $this->fnItem($td, $tdContent, $this->item);
                }

                //funkcja linka
                if ($this->item) {
                    $tdContent = $this->funcLink($td, $tdContent, $this->item);
                }

                //funkcja formatujaca wartosc
                //dziala tylko jak nie ma funkcji fnItem (zeby mozna bylo laczyc fnItem dla <td> i fnValue dla <input>)
                if (!$hasfnItem) {
                    $tdContent = $this->fnValue($td, $tdContent);
                }

                //funkcja formatujaca wartosc ale tylko w komorce
                $tdContent = $this->fnTdValue($td, $tdContent);

                //przypadek jesli nie ma pola w nowym rekordzie, zeby nie kopiowal wartosci poprzedniego rekordu
                if ($newRecord) {
                    $tdContent = null;
                }
            }

            //klasa komorki add
            if ($newRecord && $td['addClass']) {
                $class = $td['addClass'];
            }

            $tdClass = ($class) ? ' class="'.$class.'"' : '';
            $html .= '<td'.$tdClass.'>'.$tdContent.'</td>';
        }

        if (!$newRecord) {
            $html .= $this->tdEdit();
        } else {
            $html .= $this->tdAdd();
        }

        $html .= '</tr>';

        return $html;
    }

    /**
     * Funckaja dla wartosci komorki i pola
     * Zwraca wartosc przerobiona przez funkcje (wykorzysuje tylko $value, wiec jedno przekazane pole).
     */
    private function fnValue($td, $value)
    {
        if ($td['fnValue']) {
            $func = $td['fnValue'];

            return $func($value);
        }

        return $value;
    }

    /**
     * Funckaja dla wartosci TYLKO komorki.
     */
    private function fnTdValue($td, $value)
    {
        if ($td['fnTdValue']) {
            $func = $td['fnTdValue'];

            return $func($value);
        }

        return $value;
    }

    /**
     * Funckaja dla wartosci komorki
     * Zwraca wartosc przerobiona przez funkcje (wykorzustje caly $item, wiec moze poskladac w jeden ciag kilka pol).
     */
    private function funcContent($td, $value, $item)
    {
        if ($td['funcContent']) {
            $func = $td['funcContent'];

            if ($func == 'row-no') {
                return $this->rowNo;
            }

            return $func($item);
        }

        return $value;
    }

    /**
     * Alias funkcji wyzej.
     */
    private function fnItem($td, $value, $item)
    {
        if ($td['fnItem']) {
            $func = $td['fnItem'];

            if ($func == 'row-no') {
                return $this->rowNo;
            }

            return $func($item);
        }

        return $value;
    }

    /**
     * Funckaja dla tablicy selecta
     * Zwraca tablice przerobiona przez funkcje.
     */
    private function funcOptions($dataField, $arr, $value, $item)
    {
        if ($dataField['funcOptions']) {
            $func = $dataField['funcOptions'];
            //print_r($func($arr, $value));
            return $func($arr, $value, $item);
        }

        return $arr;
    }

    /**
     * Funckaja dla komorek - linkow (ta funkcja cala jest zla !!! - bez sensu jest tu wstawianie zaleznosci z Link).
     */
    private function funcLink($td, $value, $item)
    {
        if ($td['funcLink']) {
            $func = $td['funcLink'];

            switch ($func) {
                case 'productOld':
                    return Link::productOld($item->product_id, $item->product_code);
                    break;

                case 'product':
                    return Link::editPr($item->type_id, $item->product_id, array('title' => $item->product_code));
                    break;

                //to jest wyjatek bo w Analiza zapotrzebowania jest w jednym widoku produkt i material
                case 'material':
                    return ($item->material_id) ? Link::editPr($item->material_type_id, $item->material_id, array('title' => $item->material_code)) : '';
                    break;

                case 'customer':
                    return Link::customer($item->customer_id, $item->customer_code);
                    break;
            }
        }

        return $value;
    }

    /**
     * Naglowek tabeli (szablon: edit-list).
     */
    private function thEdit()
    {
        if (!$this->getConf('editButton') && !$this->getConf('deleteButton')) {
            return;
        }
        if ($this->getConf('thEdit') === false) {
            return;
        }

        //$title = (_View::$var['sReturnData']) ? 'wybierz'  : 'szczegóły';
        //return '<td class="caption wh55">'.$title.'</td>';

        //$this->calculateTableWidth('wh20');
        //$this->calculateTableWidth('wh20');

        return '<td class="caption caption-btn" colspan="2"></td>';
    }

    /**
     * Nazwa pola wysyłanego w data- dla przycisku "save" i "add".
     */
    private static function getDataRowIdName()
    {
        if (self::getTemplateFile() == 'list-editable') {
            return '_id'; //edycja tabeli glownej (wiec przekazuje _id, dla zgodnosc z "postSave")
        } else {
            return '_row_id'; //edycja tabeli podrzednej
        }
    }

    /**
     * Guziki Edit i Delete (szablon: edit-list).
     */
    private function tdEdit()
    {
        if (!$this->getConf('editButton') && !$this->getConf('deleteButton')) {
            return;
        }

        $primaryId = self::getPrimaryId();
        $currentTab = $this->getCurrentTab();
        $linkParam = self::getLinkParam();

        if (self::getTemplateFile() == 'list-editable') {
            $action = 'getIndex';

            //edycja tabeli (lista)
            $linkEdit = Utils::urlToAction($action, array('_erow_id' => $this->getItemId()));
            $linkCancel = Utils::urlToAction($action);
            $linkSave = Utils::urlToAction($action); //"action" jest w action formularza (list-edit.blade.php)
            $linkDelete = Utils::urlToDelete($this->getItemId(), array('_form' => 2)); //row_id (a nie: rowId) - dla zgodnosci nazwy zmiennej z save
        } else {

            //edycja tabeli w zakladce (domyslna)
            //dla EDIT i CANCEL jest pole ""_erow_id""
            $linkQuery = array('_tab' => $currentTab, '_erow_id' => $this->getItemId());

            //dodakowe parametry (przekazuje z bazy wymienione pola do linka np: product_id)
            //dodale literke l_, zeby nazwa nie kolidowala z innymi zmiennymi GET
            $linkQueryReset = array();
            if (isset($this->additionalLinkParam['edit'])) {
                foreach ($this->additionalLinkParam['edit'] as $field) {
                    $linkQuery['l_'.$field] = $this->item->$field;
                    $linkQueryReset['l_'.$field] = null; //aby zresetowac te zmienne w 	$linkCancel i $linkSave
                }
            }

            if (isset($this->additionalLinkParam['all'])) {
                foreach ($this->additionalLinkParam['all'] as $key => $val) {
                    $linkQuery[$key] = $val;
                    //$linkQueryReset[$key] = null; //aby zresetowac te zmienne w 	$linkCancel i $linkSave
                }
            }

            $linkEdit = Utils::urlToEdit($primaryId, array_merge($linkParam, $linkQuery));
            $linkCancel = Utils::currentUrl(array_merge(array('_erow_id' => null), $linkQueryReset));
            $linkSave = Utils::currentUrl(array_merge(array('_erow_id' => null), $linkQueryReset));

            //$linkQuery osobny dla $linkDelete
            $linkQuery = array_merge((array) $this->additionalLinkParam['all'], array('_tab' => $currentTab, '_row_id' => $this->getItemId())); //row_id (a nie: rowId) - dla zgodnosci nazwy zmiennej z save
            $linkDelete = Utils::urlToDelete($primaryId, array_merge($linkParam, $linkQuery));
        }


        $linkEdit = '<a class="ld-ajax" href="'.$linkEdit.'"></a>';
        $linkSave = '<a class="ld-ajax-submit"'.$this->getSubFromJsData().' data-'.self::getDataRowIdName().'="'.$this->rowId.'" href="'.$linkSave.'"></a>';
        $linkDelete = '<a class="ld-ajax" href="'.$linkDelete.'" data-ld_confirm="Czy na pewno usunąć rekord ?"></a>';

        //uprawnienia
        if (!$this->canSave) {
            $linkEdit = '-';
            $linkSave = '-';
        }

        if (!$this->canDelete) {
            $linkDelete = '-';
        }

        //row_id (data) - z podkresleniem bo JS podczas wysylania POST zamienia rowId na rowid
        if ($this->rowId == $this->getItemId()) {
            return '<td class="adm-btn save bg-edit">'.$linkSave.'</td><td class="adm-btn cancel bg-edit"><a class="ld-ajax" href="'.$linkCancel.'"></a></td>';
        } else {
            return '<td class="adm-btn edit">'.$linkEdit.'</td><td class="adm-btn delete">'.$linkDelete.'</td>';
        }
    }

    /**
     * Guziki Add (szablon: edit-list).
     */
    private function tdAdd()
    {
        //$linkParam = _View::$var['td']['linkParam'];
        $linkSave = Utils::currentUrl(array('_row_id' => null));

        return '<td class="adm-btn add bg-add" colspan="2" style="width: 47px;"><a class="ld-ajax-submit"'.$this->getSubFromJsData().' data-'.self::getDataRowIdName().'="0" href="'.$linkSave.'"></a></td>';
    }

    /**
     * Zwraca kolejny nr klucza tablicy pol (szablon: edit-list)
     * Poprzez param (w addInput) mozna "przeskoczyc" kolumne, aby nie zawierala ona pola.
     */
    private function getColumnNo(array $param = null)
    {
        if (!count($this->formField)) {
            $column = 0;
        } else {
            $column = max(array_keys($this->formField)) + 1;
        }

        if (isset($param['column'])) {
            $column = $param['column'];
        }

        //sprawdza czy nie podmienia kolumny
        if (key_exists($column, $this->formField)) {
            die('given key exists: '.$column);
        }

        return $column;
    }

    /**
     * Zwraca komorke tabeli z etykieta (wyrownana do prawej)  (szablon: edit).
     */
    public function label($label, $required = false, array $param = null)
    {
        $class = $this->getConf('td1Class');
        $style = '';
        $required = ($required) ? '<span class="c-red">* </span> ' : '';

        //dodanie :, jesl go nie ma w etykiecie
        if ($label) {
            if (strpos($label, ':') === false) {
                $label = $label.':';
            }
        }

        //klasa dodakowa
        if (isset($param['class'])) {
            $class .= $class.' '.$param['class'];
        }

        //styl
        if (isset($param['style'])) {
            $style = ' style="'.$param['style']."'";
        }

        return '<td class="adm-label '.$class.'"'.$style.'>'.$required.''.$label.'</td>';
    }

    /**
     * Zwraca [data-] dla linku "save" i "add" dla "subForm"
     */
    private function getSubFromJsData()
    {
        $data = '';
        if ($this->subForm) {
            $data = ' data-_sub_form="'.$this->subForm.'"';
            $data.= ' data-_tab="'. $this->getCurrentTab().'"';
            $data.= ' data-_id="'. self::getPrimaryId().'"';
        }

        return $data;
    }

    /*STARE*/
    public static function openTr(array $attributes = null)
    {
        return '<tr>';
    }

    public static function closeTr(array $attributes = null)
    {
        return '</tr>';
    }

    public static function tdLabel($label, $required = false, array $param = null)
    {
        if ($param['class']) {
            $class = ' '.$param['class'];
        }

        $required = ($required) ? '<span class="cRed">* </span> ' : '';

        $html = '';

        if ($param['begin']) {
            $html .= '<tr>';
        }

        //dla przypdaku pustej etykiety - nie dodaje :
        if ($label) {
            $label .= ':';
        }

        $html = '<td class="label'.$class.'">'.$required.''.$label.'</td>';

        if ($param['end']) {
            $html .= '</tr>';
        }

        return $html;
    }

    public static function tdOpen($item, $param = null)
    {
        $primaryKey = _View::$var['td']['primaryKey'];
        $linkParam = _View::$var['td']['linkParam'];

        //if(!$item->$primaryKey) {

        //}

        $href = Link::edit($item->$primaryKey, $linkParam);

        if (!is_array($param)) {
            $fieldName = $param;
        } else {
            $fieldName = $param['name'];
            //to jest uzywane?
            $href = call_user_func(array('Link',  'edit'.ucfirst($param['prefix'])), $item->$param['id']);
        }

        //if (_View::$var['sReturnData']) {
        //	$edit = '<a href="" class="returnData" data-name="'.$primaryKey.'" data-id="'.$item->$primaryKey.'">wybierz</a>';
        //} else {
            $edit = '<a class="fancy" href="'.$href.'"'.Link::fancyWidth($linkParam).'>'.$item->$fieldName.'</a>';
        //}

        return $edit;
    }

    public static function getPrimaryId()
    {
        return Utils::getData('currentId');
    }

    public static function getLinkParam()
    {
        return array(); //_View::$var['td']['linkParam'];
    }

    public function getCurrentTab()
    {
        $tab = $this->tab ? $this->tab : Utils::getData('currentTab');
        //echo $tab;
        return $tab;
    }

    public function getCurrentTabName()
    {
        $tabs = Utils::getData('tabs');
        $currentTab = $this->getCurrentTab();
       // echo $this->getCurrentTab().'---'.$this->tab.'<br>';
        return $tabs && $currentTab ? $tabs[$currentTab]['name'] : '';
    }

    public function getAbilityControllerName()
    {
        return Utils::getData('abilityControllerName');
    }

    public static function getTemplateFile()
    {
        return Utils::getData('template');
        //return 'editTable'; // _View::$var['table']['templateFile'];  nie wiem po co to
    }

    /**
     * Sprawdza uprawnienia do danej akcji
     * Jesli formularz jest zablokowany (po zatwierdzeniu rekordu) - zwraca false dla wszystkich akcji (edit, save, delete).
     * Jesli ma uprawnienia do "run" calego Controllera - zwraca true dla wszystkich akcji (edit, save, delete).
     */
    public function hasAbility($ability, $checkFormLock = true)
    {
        if ($checkFormLock) { //ZMIANA: && Form::isLocked()
            return false;
        }

        $controller = $this->getAbilityControllerName();
        $tab = $this->getCurrentTabName();

        if (\Auth::user()->can('run', $controller)) {
            return true;
        }

        return \Auth::user()->can($ability, $controller.'|'.$tab);
    }

    /**
     * Dla statycznego wywolania (np. z widoku) lub z metody saveButton2.
     */
    public static function saveButton(array $param = null)
    {
        $value = (self::getPrimaryId()) ? 'Zapisz zmiany' : 'Dodaj';
        $class = '';

        if (isset($param['value'])) {
            $value = $param['value'];
        }
        if (isset($param['class'])) {
            $class = ' '.$param['class'];
        }

        return Form::button($value, array('class' => 'inp sub ld-ajax-submit'.$class));
    }

    /**
     * Przycisk "dodaj" (nowy rekord) / "zapisz zmiany" (istniejacy rekord)
     * Tutuaj jest wyjatek.
     */
    private function saveButton2(array $param = null)
    {
        //zapis lub dodawanie
        $ability = (self::getPrimaryId()) ? 'update' : 'store';

        $hasAbility = $this->hasAbility($ability, false);

        //jesli ma uprawmnienia lub guzik nie jest blokowany (posiada parametr "no-lock" (ustawiony w $table->addTr)
        if ($hasAbility) { // || $param['no-lock'] === true - tego juz nie uzywam
            $saveButton = self::saveButton((array) $param);
        } else {
            $saveButton = AbilityHelper::cannotMessage($ability);
        }

        return $saveButton;
    }

    public function deleteButton(array $param = null)
    {
        $deleteButton = '';
        $value = 'Usuń';
        $alert = 'Czy na pewno usunąć rekord ?';
        $url = Utils::urlToDelete(self::getPrimaryId()).array_get($param, 'query');

        if (isset($param['value'])) {
            $value = $param['value'];
        }
        if (isset($param['alert'])) {
            $alert = ' '.$param['alert'];
        }
        if (isset($param['url'])) {
            $url = ' '.$param['url'];
        }

        //przycisk usun - jesli jest ID rekordu
        if (self::getPrimaryId()) {
            $deleteButton = Form::button($value, array('class' => 'inp sub fl-l record-delete-input'));
            $deleteButton .= '<a href="'.$url.'" class="ld-ajax record-delete-a hide" data-ld_confirm="'.$alert.'">x</a>';

            //if (!$this->canDelete) {
                //$deleteButton = '';
            //}

            //TAKIE SAME ZASADY JAK W "saveButton2"
            if (! $this->hasAbility('destroy', false)) {
                $deleteButton = '';
            }
        }

        return $deleteButton;
    }

    /*
     * Oblicza szerokosc tabeli na podstawie klass naglowkow
     * Zle oblicza bo pola select/inoput w wierszu dodawania dodakowo rozpychaja tabele
     */
    /*private function calculateTableWidth($class)
    {
        if (substr($class, 0, 2) == 'wh') {
            $columnWidth = str_replace('wh', '', $class);
            $this->tableWidth = (int)$this->tableWidth + (int)$columnWidth + 8; //3+3 (padding) + 2 (border)
        }
    }*/
}