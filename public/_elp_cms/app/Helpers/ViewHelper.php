<?php

namespace App\Helpers;

class ViewHelper
{
    private static $routeName = false;

    public static function setRouteName($name)
    {
        self::$routeName = $name;
    }

    public static function menuLi($menu)
    {
        $menuTree = [
            'scanned-pallets' => [
                'scanned-pallets',
                'scanned-pallets-comparison', 
            ],

            'packed-products' => [
                'packed-products',
            ],

            'inventories' => [
                'inventories',
            ],

            'configuration' => [
                'users', 
                'user-li-areas',
                'failure-li-areas',
                'failure-li-testers',
                'failure-li-call-types',
                'failure-li-break-reasons',
                'holidays',
            ],
        ];
        
       
        $showLi = false;
        $active = false;
        $toggle = false;

        $submenuIcon = '';
        $submenuUl = '';
        $submenuLi = '';
        $group = '';

        if (isset($menu['submenus'])) {
            foreach ($menu['submenus'] as $submenu) {
                $submenuLi.= self::menuLi($submenu);
            }

            //there is at least one Li
            if ($submenuLi) {
                $showLi = true;
                $submenuUl = ' <ul class="dropdown-menu">'.$submenuLi.'</ul>';
                $submenuIcon = ' <span class="caret"></span>';
            }

            $toggle = true;

        } else {
            $showLi = true;
        }

        $html = '';
        if ($showLi && self::hasPermission(array_get($menu, 'permission', '*'))) {

            if (self::$routeName) {
                if (self::$routeName == array_get($menu, 'route_name')) {
                    $active = true;
                }

                //if (in_array(self::$routeName, array_get($menuTree, array_get($menu, 'name')))) {
                //    $active = true;
                //}
            }

            $target = array_get($menu, 'target');
            $html ='<li'.($active ? ' class="active"' : '').'><a'.($toggle ? ' class="dropdown-toggle" data-toggle="dropdown"' : '').' href="'.array_get($menu, 'url', '#').'" target="'.$target.'"><i class="fa '.array_get($menu, 'icon', 'fa-circle-o').'"></i> '.$menu['title'].$submenuIcon.'</a>'. $submenuUl.'</li>';
        }

        return $html;
    }

    public static function hasPermission($permission)
    {
        if ($permission == '*') {
            return true;
        }

        return \Auth::user()->can($permission) ?  true : false;
    }
}