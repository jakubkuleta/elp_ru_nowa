<?php

namespace App\Helpers;

class FormatHelper
{
    public static function date($date)
    {
        return $date ? date('Y-m-d', strtotime($date)) : '';
    }

    public static function dateTime($date, $returnIfEmpty = '')
    {
        return $date ? date('Y-m-d H:i:s', strtotime($date)) : $returnIfEmpty;
    }

    public static function dateTimeWithoutSeconds($date)
    {
        return $date ? date('Y-m-d H:i', strtotime($date)) : '';
    }

    public static function timeWithoutSeconds($date)
    {
        return $date ? date('H:i', strtotime($date)) : '';
    }

    public static function number($number, $decimalPlaces = null, $showEmpty = false)
    {
        return self::_number($number, $decimalPlaces, $showEmpty);
    }

    public static function number2($number, $showEmpty = false)
    {
        return self::_number($number, 2, $showEmpty);
    }

    private static function _number($number, $decimalPlaces = null, $showEmpty = false)
    {
        if ((string) $number != '') {
            $number = self::_convertToNumeric($number, false);
        }

        //jesli nie zdefiniowano ilosc miejsc - wyswietlam tyle ile jest)
        if ($decimalPlaces === null) {
            $decimalPlaces = self::_countDecimals($number);
        }

        //jesli bedzie false to dla ciagu pustego zwroci 0
        if ($showEmpty) {
            if ((string) $number == '') {
                return '';
            }
        }

        return number_format((float) $number, $decimalPlaces, ',', ' ');
    }

    private static function _countDecimals($number)
    {
        $number = str_replace(',', '.', $number);
        $number = floatval($number);

        for ($decimals = 0; $number != round($number, $decimals); ++$decimals);

        return $decimals;
    }

    private static function _convertToNumeric($val, $nullIfZero = false)
    {
        $val = str_replace(',', '.', trim($val));
        $val = str_replace(' ', '', $val);

        //konweruje do float tylko w przypadku jak jest numeric
        if ($val != '' && self::_isNumeric($val)) {
            $val = (float) $val;
        }

        if ($nullIfZero) {
            return ($val == 0) ? null : $val;
        } else {
            return $val;
        }
    }

    private static function _isNumeric($val)
    {
        if ((string) $val == '') {
            return false;
        }

        $val = str_replace(',', '.', $val);
        $val = str_replace(' ', '', $val);

        return preg_match('/^-?[0-9]{1,12}(?:\.[0-9]{1,4})?$/', $val);
    }    
}
