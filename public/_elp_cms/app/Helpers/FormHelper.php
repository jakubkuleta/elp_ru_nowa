<?php

namespace App\Helpers;

use Form;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class FormHelper
{
    private static $formHorizontal = false;
    private static $labelDivClass = '';
    private static $inputDivClass = '';
    private static $checkboxDivClass = '';

    public static function setLayoutToHorizontal()
    {
        //self::$labelDivClass = 'col-sm-2 control-label';
        //self::$inputDivClass = 'col-sm-10';
        //self::$checkboxDivClass = 'col-sm-10 col-sm-offset-2';
    }

    public static function label($label, $name)
    {
        $isRequired = substr($label, -1) == '*' ? true : false;
        $em = '';

        if ($isRequired) {
            $label = substr($label, 0, -1);
            $em = ' <em class="req">*</em>';
        }

        return '<label class="'.self::$labelDivClass.'" for="id-'.$name.'">'.$label.':'. $em.'</label>';
    }

    public static function labelAndText($label, $name, array $paremeters = null)
    {
        return '<div class="form-group">'.self::label($label, $name).'<div class="'.self::$inputDivClass.'">'.self::text($name, $paremeters).'</div></div>';
    }

    public static function labelAndPassword($label, $name, array $paremeters = null)
    {
        return '<div class="form-group">'.self::label($label, $name).'<div class="'.self::$inputDivClass.'">'.self::password($name, $paremeters).'</div></div>';
    }

    public static function text($name, array $paremeters = null)
    {
        $value = array_get($paremeters, 'value', null);
        $class = array_get($paremeters, 'class', '');

        $paremeters = array_except((array)$paremeters, ['class', 'id']);
        return Form::text($name, $value, array_merge($paremeters, array('class'=>trim('form-control '. $class), 'autocomplete'=>'off', 'id'=>'id-'.$name)));
    }

    public static function password($name, array $paremeters = null)
    {
        $value = null;
        $class = array_get($paremeters, 'class', '');
        $maxlength = array_get($paremeters, 'maxlength', '');

        return Form::password($name, array('class'=>trim('form-control '. $class), 'maxlength' => $maxlength, 'autocomplete'=>'off', 'id'=>'id-'.$name));
    }

    public static function labelAndTextarea($label, $name, array $paremeters = null)
    {
        $value = null;
        $class = '';

        $paremeters = array_except((array)$paremeters, ['class', 'id']);
        return '<div class="form-group">'.self::label($label, $name).'<div class="'.self::$inputDivClass.'">'.Form::textarea($name, $value, array_merge($paremeters, array('class'=>trim('form-control '. $class), 'id'=>'id-'.$name))).'</div></div>';
    }

    public static function labelAndSelect($label, $name, $options, $item = null, array $paremeters = null)
    {
        $value = $item ? Arr::get($item, $name) :  null;
        $class = Arr::get($paremeters, 'class', '');

        if (!in_array('multiple', (array) $paremeters)) {
            $options = self::prependEmptyOption($options);
        }

        if ($selectedItems = Arr::get($paremeters, 'selectedItems'))  {
            $value = self::multipleValue($selectedItems);
        }

        $paremeters = Arr::except((array)$paremeters, ['class', 'id', 'selectedItems']);

        return '<div class="form-group">'.self::label($label, $name).'<div class="'.self::$inputDivClass.'">'.Form::select($name, $options, $value, array_merge($paremeters, array('class' => trim('form-control '.$class), 'id'=>'id-'.$name))).'</div></div>';
    }

    public static function labelAndCheckboxWithHidden($label, $name, $item = null)
    {
        return '<div class="form-group"><div class="'.self::$checkboxDivClass.'"><div class="checkbox"><label>'.self::checkboxWithHidden($name, $item).' '.$label.'</label></div></div></div>';
    }

    public static function checkboxWithHidden($name, $item = null)
    {
        $value = $item ? array_get($item, $name) :  null;
        return Form::hidden($name, 0).Form::checkbox($name, 1, $value);
    }

    public static function optionsYesNo($key = null)
    {
        return [
            0 => 'nie',
            1 => 'tak',
        ];
    }

    public static function prependEmptyOption($arr)
    {
        if ($arr instanceof Collection) {
            $arr = $arr->toArray();
        }

        return array('' => '--') + $arr;
    }

    public static function tree($array, $level = -1)
    {
        $level++;

        $return = '';
        $indent = str_repeat('&nbsp; &nbsp;', $level);

        if (!is_array($array)) {
            $return.= $array;
        } else {
            foreach ($array as $key => $val) {
                if (!is_array($val)) {
                    $return.= $indent.$key.': '.$val."\n";
                } else {
                    $return.= $indent.$key."\n";
                    $return.= ArrayHelper::tree($val, $level);
                }
            }
        }

        return $return;
    }

    public static function multipleValue($selectedItems)
    {
        return count(request()->old()) ? null : $selectedItems;
    }
}
