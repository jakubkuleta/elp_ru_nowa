<?php

namespace App\Helpers;

use Carbon\Carbon;
use DB;

class DateHelper
{
    public static function businessDaysBetween($from, $to)
    {
        $from = Carbon::parse($from);
        $to = Carbon::parse($to);

        return DB::selectOne("SELECT dbo.fn_dni_robocze(DATEFROMPARTS($from->year, $from->month, $from->day), DATEFROMPARTS($to->year, $to->month, $to->day), 0) as business_days")->business_days;
    }

    public static function months()
    {
        return [
            1 => 'styczeń',
            2 => 'luty',
            3 => 'marzec',
            4 => 'kwiecień',
            5 => 'maj',
            6 => 'czerwiec',
            7 => 'lipiec',
            8 => 'sierpień',
            9 => 'wrzesień',
            10 => 'październik',
            11 => 'listopad',
            12 => 'grudzień'
        ];
    }

    public static function shortWeekDays()
    {
        return [
            1 => 'Pn',
            2 => 'Wt',
            3 => 'Śr',
            4 => 'Cz',
            5 => 'Pt',
            6 => 'So',
            7 => 'N',
        ];
    }

    public static function years($from, $to)
    {
        $years = [];

        for ($i = $from; $i <= $to; $i++) {
            $years[$i] = $i;
        }

        return $years;
    }
}