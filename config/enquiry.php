<?php

return [
    'send_to' => [
        'taisaludwikowska@euro-locks.pl',
        'piotrgulski@euro-locks.pl',
        'katerynapavlenko@euro-locks.pl',
        'aleksanderdulski@euro-locks.pl',
        'jkuleta@euro-locks.pl',
        'sekretariat@euro-locks.pl',
        'anatolykarpov@euro-locks.com.ru',
        'krzysztofratajczak@euro-locks.pl'
        ],
    'send_to_local' => [
        'k.ratajczak@gmail.com',
        'krzysztofratajczak@euro-locks.pl'
        ],
    'subject' => 'Запрос с интернет-сайта',
];
