<?php

return [

    'company_address' => [
        'name' => 'OOO Евро-Локс',
        'street' => 'ул. Павла 29',
        'city' => '41-708 Руда Слёнска',
        'country' => 'ПОЛЬША',
        'phone' => '+48 32 344-78-70 ÷ 89',
        'phone_url' => '+48323447870',
        'fax' => '+48 32 344-78-74',
        'email' => 'sekretariat@euro-locks.pl',
    ],

    'domain' => 'http://www.euro-locks.com.ru',

    'downloads' => [
        'katalog-euro-locks-avtomobilnaya-promyshlennost' => [
                'name' => 'Каталог EURO-LOCKS Автомобильная промышленность',
                'size' => '26.1 MB',
                'pages' => 49,
                'version' => 4,
        ],
        'katalog-euro-locks-obshchiy_2020' => [
                'name' => 'Каталог EURO-LOCKS Общий',
                'size' => '40.4 MB',
        ],
        'katalog-euro-locks-energeticheskaya-promyshlennost-i-stroitelstvo' => [
                'name' => 'Каталог EURO-LOCKS Энергетическая промышленность и строительство',
                'size' => '33.3 MB',
        ],
        'katalog-euro-locks-energeticheskaya-promyshlennost-i-stroitelstvo-mini' => [
                'name' => 'Каталог EURO-LOCKS Энергетическая промышленность и строительство Mini',
                'size' => '6.4 MB',
        ],
        'zapirajushhie-sistemy-dlia-medicinskoi-mebeli' => [
            'name' => 'Запирающие системы для медицинской мебели',
            'size' => '2.0 MB',
        ],
        'zamki-dlja-sanitarno-gigienicheskogo-oborudovanija' => [
            'name' => 'Замки для санитарно-гигиенического оборудования',
            'size' => '1.9 MB',
        ],
    ],
];
